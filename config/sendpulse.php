<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | API Keys
    |--------------------------------------------------------------------------
    |
    | You can find your API keys via the SendPulse dashboard at https://login.sendpulse.com/settings/#api
    |
    */

    'apiUserId' => '55153a655be4a781b062f48d8aafb416',

    'apiSecret' => 'e23cbb490807d250324a3a0261724691',

    /*
    |--------------------------------------------------------------------------
    | Application Settings
    |--------------------------------------------------------------------------
    |
    */

    // Where to save the generated SendPulse API bearer token
    'tokenStorage' => 'session', //session, memcache or file
);