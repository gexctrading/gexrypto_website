<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();
Route::group(['namespace'=>'frontend'], function(){

    Route::any('/test3', 'PagesController@test3');
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('roadmap', 'PagesController@roadmap')->name('roadmap');
	Route::get('mission', 'PagesController@mission')->name('mission');
	Route::get('team', 'PagesController@team')->name('team');
    Route::get('teams', 'PagesController@teams');
	Route::get('presale', 'PagesController@presale')->name('presale');
    /*Route::get('presales', 'PagesController@presale')->name('presales');*/
    Route::get('public-token-sale', 'PagesController@presale')->name('public_sale');
	Route::get('faq', 'PagesController@faq')->name('faq');
    Route::any('contact', 'PagesController@contact')->name('contact');
    Route::any('steps', 'PagesController@steps')->name('steps');
    Route::any('private-invester', 'PagesController@private_invester');
    Route::any('step_form', 'PagesController@stepsSubmit')->name('step_form');
    Route::any('step_new_form', 'PagesController@stepsnewSubmit')->name('step_new_form');
    Route::any('invester_step_form', 'PagesController@investerStepsSubmit');
    Route::any('terms', 'PagesController@terms')->name('terms');
    Route::any('contact', 'PagesController@contact')->name('contact');
    Route::any('subscribe', 'PagesController@subscribe')->name('contact');
    Route::get('career', 'PagesController@career')->name('career');

	Route::post('send-contact-us-mail', 'HomeController@sendContactUsMail')->name('send_contact_us_mail');

    Route::any('/save_token/{token}','PagesController@saveToken');
    Route::any('/show_notification','PagesController@show_notification');
    Route::any('/remove_token','PagesController@remove_token');

    Route::any('/blog','PagesController@blog');
    Route::any('/blog-detail/{id}','PagesController@blog_detail');
    Route::any('/blog-comment','PagesController@blog_comment');
    Route::any('/terms-conditions','PagesController@terms');
    Route::any('/terms-sales','PagesController@terms');
    Route::any('/exchange','PagesController@exchange');
    Route::any('/about','PagesController@about');

    Route::get('/join-now','PagesController@test');
    Route::get('/individual','PagesController@test');
    Route::get('/private-investors','PagesController@private_invester');
    Route::get('/airdrop','PagesController@airdrop');

    Route::post('/testpost','PagesController@testpost')->name('testpost');

    Route::any('/getCarrerByAjax', 'HomeController@getCarrerByAjax');
    Route::any('/applyJob/{id?}', 'HomeController@applyJob');
    Route::any('/apply_linkedin', 'HomeController@applylinkedin');
    Route::any('/edit_linkedin', 'HomeController@editlinkedin');
    Route::any('/send-resume', 'HomeController@sendResume');
     Route::any('/saveLinkedInData', 'HomeController@saveLinkedInData');

     
});

Route::group(array('prefix' => 'administrator'), function()
{
	Route::any('/','AdminController@login');
	Route::any('/dashboard','AdminController@dashboard');
	Route::any('/logout','AdminController@logout');
    Route::any('/profile','AdminController@profile');
    Route::any('/profile_information','AdminController@profile_information');
    Route::any('/admin_image','AdminController@admin_image');
    Route::any('/checkAdminPassword','AdminController@checkAdminPassword');
    Route::any('/change_pass','AdminController@change_pass');
    /*cms*/
    Route::any('/home_page','backend\CmsController@homePage');
    Route::any('/team_page','backend\CmsController@teamPage');
    Route::any('/faq_page','backend\CmsController@faqPage');
    Route::any('/mission_page','backend\CmsController@missionPage');
    Route::any('/terms_page','backend\CmsController@termsPage');
    Route::any('/whitepaper_page','backend\CmsController@whitepaperPage');
    Route::any('/roadmap_page','backend\CmsController@roadmapPage');
    Route::any('/header_menu','backend\CmsController@header');
    Route::any('/footer_menu','backend\CmsController@footer');
    Route::any('/faq','backend\CmsController@faq');
    Route::get('/add/faq', function () {
	    return view('backend.cms.faq_form');
	});
    Route::any('edit/faq/{id}','backend\CmsController@edit_faq');
    Route::any('/change_status/{id}/{status}','backend\CmsController@change_status');
    Route::any('/contact','backend\CmsController@contact');
    /*team*/
    Route::any('team/categories','backend\TeamController@categories');
    Route::any('add/team-category','backend\TeamController@categories');
    Route::get('/add/team-category', function () {
	    return view('backend.team.category_form');
	});
    Route::any('edit/team-category/{id}','backend\TeamController@edit_categories');
    Route::any('del/team-category/{id}','backend\TeamController@del_categories');

    Route::any('team/members','backend\TeamController@members');
    Route::any('add/member','backend\TeamController@addMember');
	Route::any('edit/team-member/{id}','backend\TeamController@edit_member');
    Route::any('del/team-member/{id}','backend\TeamController@del_member');
    Route::any('/change_status_team_category/{id}/{status}','backend\TeamController@change_status_team_category');
    Route::any('/change_status_team_member/{id}/{status}','backend\TeamController@change_status_team_member');
    /*blog*/

    Route::any('blog/posts','backend\BlogController@posts');
    Route::any('edit/post/{id}','backend\BlogController@editPost');
    Route::get('/add/blog', function () {
        return view('backend.blog.post_form');
    });
    Route::any('/change_status_blog_post/{id}/{status}','backend\BlogController@change_status_blog_post');
     Route::any('/comment_status/{id}/{status}','backend\BlogController@comment_status');
    Route::any('/view/post/{id}','backend\BlogController@view_blog');
    Route::any('/view/comment/{id}','backend\BlogController@view_comment');



    Route::any('reply','backend\BlogController@reply');

    /*contact*/
    Route::any('contact-list','AdminController@contactList');
    Route::any('view-contact/{id}','AdminController@viewContact');
    /*subscriber*/
    Route::any('subscribers','AdminController@subscribers');
    Route::any('del/subscriber/{id}','AdminController@deleteSubscriber');
    /*newsletter*/

    /*subscriber*/
    Route::any('newsletters','AdminController@newsletters');
    Route::any('del/newsletter/{id}','AdminController@delNewsletter');
    Route::any('send/newsletter','AdminController@sendNewsletter');
    Route::any('send/newsletter/{id}','AdminController@send_newsletter');
     Route::get('/add/newsletter', function () {
        return view('backend.newsletter_form');
    });

    Route::any('private-investers','AdminController@private_investers');
    Route::any('join-list','AdminController@join_list');
    Route::any('delete/invester/{id}','AdminController@deleteInvester');
    Route::any('delete/join/{id}','AdminController@deleteJoin');

    
   /* Start Career */

    Route::any('/career', 'AdminController@empCareer');
    Route::any('/empcareer', 'AdminController@careerSection1');
    Route::any('/editempsec1', 'AdminController@editCareerSec1');
    Route::any('/deletesect1', 'AdminController@deleteCareerSec1');

    Route::any('/empcareer2', 'AdminController@careerSection2');
    Route::any('/editempsec2', 'AdminController@editCareerSec2');
    Route::any('/deletesect2', 'AdminController@deleteCareerSec2');

    Route::any('/empcareer3', 'AdminController@careerSection3');
    Route::any('/editempsec3', 'AdminController@editCareerSec3');
    Route::any('/deletesect3', 'AdminController@deleteCareerSec3');

    Route::any('/empcareer4', 'AdminController@careerSection4');
    Route::any('/editempsec4', 'AdminController@editCareerSec4');
    Route::any('/deletesect4', 'AdminController@deleteCareerSec4');

    /* End Career */

    /*Applicant*/

    Route::any('/applicant', 'AdminController@applicantDetail');
    Route::any('/deleteApplicant', 'AdminController@deleteRecord');
    
    /*End Applicant*/


});

Route::any('/{page?}',function(){
  return View::make('errors.404');
})->where('page','.*');