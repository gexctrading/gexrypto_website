<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
   <meta name="description" content="Admin, Dashboard, Bootstrap" />
   <link rel="shortcut icon" sizes="196x196" href="{{ url('/public/img/logo.png') }}">
   <title>@yield('title')</title>
   <link rel="icon" type="image/png" href="https://gexcrypto.io/public/frontend/images/favicon.png" />
   
   <link rel="stylesheet" href="{{ url('/public/libs/bower/font-awesome/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ url('/public/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css') }}">
   <link rel="stylesheet" href="{{ url('/public/libs/bower/animate.css/animate.min.css') }}">
   <link rel="stylesheet" href="{{ url('/public/libs/bower/fullcalendar/dist/fullcalendar.min.css') }}">
   <link rel="stylesheet" href="{{ url('/public/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css') }}">
   <link rel="stylesheet" href="{{ url('/public/assets/css/bootstrap.css') }}">
   <link rel="stylesheet" href="{{ url('/public/assets/css/core.css') }}">
   <link rel="stylesheet" href="{{ url('/public/assets/css/app.css') }}">
   <link rel="stylesheet" href="{{ url('/public/css/custom_style.css') }}">
   <!-- endbuild -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
   <script src="{{ url('/public/libs/bower/breakpoints.js/dist/breakpoints.min.js') }}"></script>
   <script>
      Breakpoints();
   </script>

   @yield('css')
</head>

<body class="menubar-left menubar-unfold menubar-light theme-primary">
<!--============= start main area -->

<!-- APP NAVBAR ==========-->
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top primary">
  
  <!-- navbar header -->
  <div class="navbar-header">
    <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-box"><span class="hamburger-inner"></span></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-more"></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-search"></span>
    </button>

    <a href="{{url('/administrator/dashboard')}}" class="navbar-brand">
      <span class="brand-icon"><i class="fa fa-gg"></i></span>
      <span class="brand-name">Gexcrypto</span>
    </a>
  </div><!-- .navbar-header -->
  

</nav>
<!--========== END app navbar -->


<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
  <div class="app-user">
    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md avatar-circle">
          <a href="javascript:void(0)"><img class="img-responsive" src="/public/admin/{{ Auth::user()->image }}" alt="avatar"/></a>
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <h5><a href="javascript:void(0)" class="username">{{ Auth::user()->name }}</a></h5>
          <ul>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <small>Admin</small>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu animated flipInY">
                <li>
                  <a class="text-color" href="{{ url('/administrator/dashboard') }}">
                    <span class="m-r-xs"><i class="fa fa-home"></i></span>
                    <span>Dashboard</span>
                  </a>
                </li>
                <li>
                  <a class="text-color" href="{{ url('/administrator/profile') }}">
                    <span class="m-r-xs"><i class="fa fa-user"></i></span>
                    <span>Profile</span>
                  </a>
                </li>
                <li role="separator" class="divider"></li>
                <li>
                  <a class="text-color" href="{{ url('/administrator/logout') }}">
                    <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                    <span>Logout</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- .media-body -->
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
        <li class="has-submenu">
          <a href="{{ url('/administrator/dashboard') }}">
            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">Dashboard</span>
          </a>
        </li>
        
       
        
      
        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text">CMS Pages</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            <li><a href="{{ url('/administrator/home_page') }}"><span class="menu-text">Home Page</span></a></li>
            <li><a href="{{ url('/administrator/team_page') }}"><span class="menu-text">Team Page</span></a></li>
            <li><a href="{{ url('/administrator/faq_page') }}"><span class="menu-text">FAQ</span></a></li>
            <li><a href="{{ url('/administrator/mission_page') }}"><span class="menu-text">Mission</span></a></li>
             <li><a href="{{ url('/administrator/terms_page') }}"><span class="menu-text">Terms & Conditions</span></a></li>
            <li><a href="{{ url('/administrator/whitepaper_page') }}"><span class="menu-text">WhitePaper</span></a></li> 
            <li><a href="{{ url('/administrator/roadmap_page') }}"><span class="menu-text">Roadmap</span></a></li>
            <li><a href="{{ url('/administrator/contact') }}"><span class="menu-text">Contact</span></a></li>
            <li><a href="{{ url('/administrator/header_menu') }}"><span class="menu-text">Header Menu</span></a></li>
            <li><a href="{{ url('/administrator/footer_menu') }}"><span class="menu-text">Footer Menu</span></a></li>
            
          </ul>
        </li>

        <li class="has-submenu">
          <a href="{{ url('/administrator/career') }}">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text">Career</span>
          </a>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text">FAQs</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            
            <li><a href="{{ url('/administrator/faq') }}"><span class="menu-text">FAQs List</span></a></li>
            <li><a href="{{ url('/administrator/add/faq') }}"><span class="menu-text">Add New</span></a></li>
            
          </ul>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text">Team</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            
            <li><a href="{{ url('/administrator/team/categories') }}"><span class="menu-text">Categories</span></a></li>
            <!--  <li><a href="{{ url('/administrator/add/team-category') }}"><span class="menu-text">Add Category</span></a></li>  -->
              <li><a href="{{ url('/administrator/team/members') }}"><span class="menu-text">Team Members</span></a></li>
             <li><a href="{{ url('/administrator/add/member') }}"><span class="menu-text">Add Team Member</span></a></li>
            
          </ul>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text">Blog</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
          <ul class="submenu">
            
            <!-- <li><a href="{{ url('/administrator/blog/categories') }}"><span class="menu-text">Categroies</span></a></li> -->
            <li><a href="{{ url('/administrator/blog/posts') }}"><span class="menu-text">Blog Post</span></a></li>
            <li><a href="{{ url('/administrator/add/blog') }}"><span class="menu-text">Add Blog Post</span></a></li>
             
            
          </ul>
        </li>

        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text">Contact Us</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
            <ul class="submenu">            
                <li><a href="{{ url('/administrator/contact-list') }}"><span class="menu-text">List</span></a></li>            
            </ul>
        </li>
  
       <!--  <li class="has-submenu">
         <a href="{{ url('/administrator/private-investers') }}">
           <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
           <span class="menu-text">Private Contributors List</span>
         </a>
       </li> -->
        <li class="has-submenu">
          <a href="{{ url('/administrator/join-list') }}">
            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">Join Now List</span>
          </a>
        </li>

        <li class="has-submenu">
          <a href="{{ url('/administrator/applicant') }}">
            <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">Job Applicant Details</span>
          </a>
        </li>


         <li class="has-submenu">
          <a href="{{url('/administrator/subscribers')}}">
             <i class="menu-icon zmdi zmdi-view-dashboard zmdi-hc-lg"></i>
            <span class="menu-text">Subscribers</span>            
          </a>          
        </li>
        <li class="has-submenu">
          <a href="javascript:void(0)" class="submenu-toggle">
            <i class="menu-icon zmdi zmdi-apps zmdi-hc-lg"></i>
            <span class="menu-text">Newsletter</span>
            <i class="menu-caret zmdi zmdi-hc-sm zmdi-chevron-right"></i>
          </a>
            <ul class="submenu">            
                <li><a href="{{ url('/administrator/newsletters') }}"><span class="menu-text">List</span></a></li>
                <li><a href="{{ url('/administrator/add/newsletter') }}"><span class="menu-text">Send new Newsletter</span></a></li>            
            </ul>
        </li>


       

     </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->


<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

@yield('content')

   <!-- APP CUSTOMIZER -->
   <div id="app-customizer" class="app-customizer">
      <a href="javascript:void(0)" 
         class="app-customizer-toggle theme-color" 
         data-toggle="class" 
         data-class="open"
         data-active="false"
         data-target="#app-customizer">
         <i class="fa fa-gear"></i>
      </a>
      <div class="customizer-tabs">
         <!-- tabs list -->
         <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#menubar-customizer" aria-controls="menubar-customizer" role="tab" data-toggle="tab">Menubar</a></li>
            <li role="presentation"><a href="#navbar-customizer" aria-controls="navbar-customizer" role="tab" data-toggle="tab">Navbar</a></li>
         </ul><!-- .nav-tabs -->

         <div class="tab-content">
            <div role="tabpanel" class="tab-pane in active fade" id="menubar-customizer">
               <div class="hidden-menubar-top hidden-float">
                  <div class="m-b-0">
                     <label for="menubar-fold-switch">Fold Menubar</label>
                     <div class="pull-right">
                        <input id="menubar-fold-switch" type="checkbox" data-switchery data-size="small" />
                     </div>
                  </div>
                  <hr class="m-h-md">
               </div>
               <div class="radio radio-default m-b-md">
                  <input type="radio" id="menubar-light-theme" name="menubar-theme" data-toggle="menubar-theme" data-theme="light">
                  <label for="menubar-light-theme">Light</label>
               </div>

               <div class="radio radio-inverse m-b-md">
                  <input type="radio" id="menubar-dark-theme" name="menubar-theme" data-toggle="menubar-theme" data-theme="dark">
                  <label for="menubar-dark-theme">Dark</label>
               </div>
            </div><!-- .tab-pane -->
            <div role="tabpanel" class="tab-pane fade" id="navbar-customizer">
               <!-- This Section is populated Automatically By javascript -->
            </div><!-- .tab-pane -->
         </div>
      </div><!-- .customizer-taps -->
      <hr class="m-0">
      <div class="customizer-reset">
         <button id="customizer-reset-btn" class="btn btn-block btn-outline btn-primary">Reset</button>
      </div>
   </div><!-- #app-customizer -->
   
   <!-- SIDE PANEL -->
   <div id="side-panel" class="side-panel">
      <div class="panel-header">
         <h4 class="panel-title">Friends</h4>
      </div>
      <div class="scrollable-container">
         <div class="media-group">
            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/221.jpg" alt="">
                        <i class="status status-online"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">John Doe</h5>
                     <small class="media-meta">active now</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/205.jpg" alt="">
                        <i class="status status-online"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">John Doe</h5>
                     <small class="media-meta">active now</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/206.jpg" alt="">
                        <i class="status status-online"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Adam Kiti</h5>
                     <small class="media-meta">active now</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/207.jpg" alt="">
                        <i class="status status-away"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Jane Doe</h5>
                     <small class="media-meta">away</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/208.jpg" alt="">
                        <i class="status status-away"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Sara Adams</h5>
                     <small class="media-meta">away</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/209.jpg" alt="">
                        <i class="status status-away"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Smith Doe</h5>
                     <small class="media-meta">away</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/219.jpg" alt="">
                        <i class="status status-away"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Dana Dyab</h5>
                     <small class="media-meta">away</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/210.jpg" alt="">
                        <i class="status status-offline"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Jeffry Way</h5>
                     <small class="media-meta">2 hours ago</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/211.jpg" alt="">
                        <i class="status status-offline"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Jane Doe</h5>
                     <small class="media-meta">5 hours ago</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/212.jpg" alt="">
                        <i class="status status-offline"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Adam Khoury</h5>
                     <small class="media-meta">22 minutes ago</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/207.jpg" alt="">
                        <i class="status status-offline"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Sara Smith</h5>
                     <small class="media-meta">2 days ago</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->

            <a href="javascript:void(0)" class="media-group-item">
               <div class="media">
                  <div class="media-left">
                     <div class="avatar avatar-xs avatar-circle">
                        <img src="/public/assets/images/211.jpg" alt="">
                        <i class="status status-offline"></i>
                     </div>
                  </div>
                  <div class="media-body">
                     <h5 class="media-heading">Donia Dyab</h5>
                     <small class="media-meta">3 days ago</small>
                  </div>
               </div>
            </a><!-- .media-group-item -->
         </div>
      </div><!-- .scrollable-container -->
   </div><!-- /#side-panel -->

   <!-- build:js /public/assets/js/core.min.js -->
   <script src="{{ url('/public/libs/bower/jquery/dist/jquery.js') }}"></script>
   <script src="{{ url('/public/libs/bower/jquery-ui/jquery-ui.min.js') }}"></script>
   <script src="{{ url('/public/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js') }}"></script>
   <script src="{{ url('/public/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js') }}"></script>
   <script src="{{ url('/public/libs/bower/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
   <script src="{{ url('/public/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js') }}"></script>
   <script src="{{ url('/public/libs/bower/PACE/pace.min.js') }}"></script>
   <!-- endbuild -->

   <script src="{{ url('/public/assets/js/library.js') }}"></script>
   <script src="{{ url('/public/assets/js/plugins.js') }}"></script>
   <script src="{{ url('/public/assets/js/app.js') }}"></script>
   <!-- endbuild -->
   <script src="{{ url('/public/libs/bower/moment/moment.js') }}"></script>
   <script src="{{ url('/public/libs/bower/fullcalendar/dist/fullcalendar.min.js') }}"></script>
   <script src="{{ url('/public/assets/js/fullcalendar.js') }}"></script>

   @yield('script')
</body>
</html>