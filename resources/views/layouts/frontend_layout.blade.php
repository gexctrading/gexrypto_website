<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title>@yield('title')</title>
      <link rel="icon" type="image/png" href="/public/img/favicon.png">
      <link href="/public/css/bootstrap.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="/public/css/style.css">
      <link rel="stylesheet" type="text/css" href="/public/css/font-awesome.min.css">
      <link rel="stylesheet" href="/public/css/owl.carousel.min.css">
       <link rel="stylesheet" type="text/css" href="/public/css/custom_style.css">
       <style type="text/css">
         .goog-te-banner-frame.skiptranslate {
            display: none !important;
         } 
         body
         {
            position: unset !important;
         }

         .translate_outr {
            display: inline-block;
            vertical-align: middle;
            padding: 15px 0;
         }
         .translate_outr .skiptranslate.goog-te-gadget > span{
            display: none;
         }
         div.goog-te-gadget {
            font-size: 0;
         }
         .translate_outr select.goog-te-combo{
            background-color: transparent;
            border: 0 none;
            color: #000000;
            font-size: 17px;
            appearance: none;
            -webkit-appearance: none;
            -moz-appearance: none; padding-right: 10px;
         }
         #google_translate_element{position: relative;background:#ffffff;padding: 5px 15px; box-shadow: 0 0 3px 1px #cccccc;}
         #google_translate_element:after{ content: "\f0d7"; position: absolute; right:15px; top:5px; font-family:'FontAwesome'; font-size: 21px;}
         select{ outline:0px !important; border:0px !important; box-shadow: none !important;}

       </style>
      @yield('css')
      <!--script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      <script type="text/javascript">
         function googleTranslateElementInit(){
           new google.translate.TranslateElement({includedLanguages: 'en,da'}, 'google_translate_element');

        } 
      </script-->
   </head>
   <body class="homepage">
      <div class="top-search-bar">
         <div class="input-group">
           <span class="input-group-btn">
             <button class="btn btn-default" type="button">
               <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
            </button>
         </span>
         <input type="text" name="" placeholder="Search" class="form-control">
         <a href="javascript:void(0)" class="cl-close-bar"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
      </div>
   </div>
      <div class="top-bar white-bl">
         <div class="container container-sm">
            <div class="row">
               <div class="col-md-12 text-right">
                  <ul>
<!--                      <li class="search-click"><a href="javascript:void(0)"><i class="fa fa-search" aria-hidden="true"></i></a></li>
 -->                     @if(Auth::check() == 1)
 						@if(Auth::user()->user_type != 2)
                     <li class="dropdown login-view-list">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}@if(!empty(Auth::user()->image))<img src="/public/img/users/{{ Auth::user()->image }}" class="user-top-im">@else <img src="/public/img/circle-user.png" class="user-top-im"> @endif </span></a>
                        @if(Auth::user()->user_type == 0)
                        <ul class="dropdown-menu" role="menu">
                           <li><a href="{{ url('/designer/dashboard') }}">Dashboard</a></li>
                           <li><a href="{{ url('/products') }}">Upload Designs</a></li>
                           <li><a href="{{ url('/designer/favourites') }}">Favourites</a></li>
                           <li><a href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                        @elseif(Auth::user()->user_type == 1)
                         <ul class="dropdown-menu" role="menu">
                           <li><a href="{{ url('/printer/dashboard') }}">Dashboard</a></li>
                           <li><a href="{{ url('/printer/products') }}">Add Product in List</a></li>
                           <li><a href="{{ url('/printer/favourites') }}">Favourites</a></li>
                           <li><a href="{{ url('/logout') }}">Logout</a></li>
                         </ul>
                        @elseif(Auth::user()->user_type == 3)
                         <ul class="dropdown-menu" role="menu">
                           <li><a href="{{ url('/reseller/dashboard') }}">Dashboard</a></li>
                           <li><a href="{{ url('/reseller/products') }}">Add Product in List</a></li>
                           <li><a href="{{ url('/reseller/favourites') }}">Favourites</a></li>
                           <li><a href="{{ url('/logout') }}">Logout</a></li>
                         </ul>
                        @endif
                     </li>
                     @else
                     <li class="dropdown login-view-list">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }}</span></a>
                        <ul class="dropdown-menu" role="menu">
                           <li><a href="{{ url('/admin/dashboard') }}">Dashboard</a></li>
                           <li><a href="{{ url('/logout') }}">Logout</a></li>
                        </ul>
                     </li>
                     @endif
                     @else
                     <li><a href="{{ url('/login') }}">Login</a></li>
                     <li><a href="{{ url('/register') }}">Sign Up</a></li>
                     @endif
                     @if(Request::path() == 'products' || Request::path() == 'printer/products' || Request::path() == 'reseller/products')
                   <li>
                        <div class="imageicon"  data-toggle="modal" data-target="#myModal">
                         <div class="c-navTrigger__text">Filter</div>
                         <img src="/public/img/menu.png" class="menupngicon">
                      </div>
                   </li>
                   @endif
                  </ul>
               </div>
            </div>
         </div>
      </div>
      <div class="nav-menu">
         <div class="container container-sm">
            <div class="row">
               <div class="col-md-12">
                  <nav class="navbar navbar-default">
                     <!-- Brand and toggle get grouped for better mobile display -->
                     <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{ url('/') }}">
                           <img src="/public/img/logo.png">
                        </a>
                     </div>
                     <!-- Collect the nav links, forms, and other content for toggling -->
                     <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul class="nav navbar-nav">
                        <li><a href="{{ url('/about-us') }}">About Us</a></li>
                        @if(Auth::check() == 1)
                           @if(Auth::user()->user_type == 1)
                              <li><a href="{{ url('/printer/products') }}">Products</a></li>
                           @elseif(Auth::user()->user_type == 3)
                              <li><a href="{{ url('/reseller/products') }}">Products</a></li>
                           @else
                              <li><a href="{{ url('/products') }}">Products</a></li>
                           @endif
                        @else
                         <li><a href="{{ url('/products') }}">Products</a></li>
                        @endif
                        <li><a href="{{ url('/resellers') }}">Resellers</a></li>
                        <li><a href="{{ url('/designers') }}">Designers</a></li>
                        <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>
                      <!--   <li class="dropdown translate_outr">
                            <div id="google_translate_element"></div>
                        </li> -->
                     </div>
                     <!-- /.navbar-collapse -->
                  </nav>
                  <!-- /.navbar -->
               </div>
            </div>
         </div>
      </div>


     
      @yield('content')

       <div class="footer">
         <div class="container container-sm">
            <div class="row">
               <div class="col-md-5 col-sm-5 col-xs-12">
                  <div class="footer-1">
                     <img src="/public/img/footer-logo.png">
                     <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                  </div>
               </div>
               <div class="col-md-2 col-sm-2 col-xs-6">
                  <div class="footer-nav">
                     <h3>More</h3>
                     <ul>
                        <li><a href="">Designers</a></li>
                        <li><a href="">Resellers</a></li>
                        <li><a href="">Products</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-sm-2 col-xs-6">
                  <div class="footer-nav">
                     <h3>More</h3>
                     <ul>
                        <li><a href="">Designers</a></li>
                        <li><a href="">Resellers</a></li>
                        <li><a href="">Products</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-md-2 col-sm-2 col-xs-6">
                  <div class="footer-nav">
                     <h3>On Social Media</h3>
                     <ul>
                        <li><a href="">Twitter</a></li>
                        <li><a href="">Facebook</a></li>
                        <li><a href="">Instagram</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-offset-2 col-md-8 col-sm-12 col-xs-12 text-center copyright-section">
                  <p>All rights reserved, Copyright 2017</p>
               </div>
            </div>
         </div>
      </div>
      <script src="/public/js/jquery.js"></script>
      <script src="/public/js/bootstrap.min.js"></script>
      <script src="/public/js/owl.carousel.js"></script>
      <script>
         $("li.search-click").click(function(e){
            $(".top-search-bar").slideToggle();
         });
          $("a.cl-close-bar").click(function(e){
            $(".top-search-bar").slideToggle();
         });
          $(".toggle-btn").click(function(e){
            $("span.icon-bars").toggleClass('open');
         });
      
 
      function googleTranslateElementInit(){
          new google.translate.TranslateElement('google_translate_element');
      }
</script>


<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
      @yield('script')
   </body>
</html>