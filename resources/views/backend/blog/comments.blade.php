  @extends('layouts.backend_layout')
@section('title')
     Blog Post | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.cust {
  width: 30%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}

</style>
<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
.btn.btn-info.collapsed {
  float: right;
  margin: 9px;
}
.collapse.in {
  padding: 40px;
}
</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
		<div class="col-md-12">

        </div>

        <div class="col-md-12">
        <div class="widget">
      

        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo" style="float:right;margin:10px;"> See Blog Detail</button>
          <div id="demo" class="collapse">
            <h4>{{$blog['title']}} - By {{$blog['author']}}</h4>
            <p class="blog_cont">{!! $blog['description'] !!}</p>
          </div>
          <header class="widget-header">
            <h4 class="widget-title">Comment List</h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div class="table-responsive">
              <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Sr/No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Message</th>
                    <th>Status</th>   
                    <th>Action</th>                 
                  </tr>
                </thead>
             
                <tbody>
                <?php $i=1; ?>
                @foreach($comments as $key=>$post)
                  <tr>
                    <td>{{++$key}}</td>
                    <td>{{ $post['name']}}</td>
                    <td>{{ $post['email']}}</td>
                    <td>{{ substr($post['message'],0,50). '..'}}</td>
                    
                    <td> 

                        <label class="switch">
                            <input type="checkbox" @if($post['status']==1) {{'checked'}} @endif name="status" id="{{$post['id']}}" class="status">
                            <span class="slider round"></span>
                        </label>
                    </td>
                    <td>
                    <a href="{{url('administrator/view/comment/'.base64_encode(convert_uuencode($post['id'])))}}" class="view-comment" id="{{$post['id']}}"><i class="fa fa-eye btn btn-primary" aria-hidden="true"></i> <span class="text-muted"></span></a>
                    </td>
                 
                  </tr>
                @endforeach
                
                 
               
                </tbody>
              </table>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div>
            

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comment</h4>
      </div>
      <div class="modal-body">
        <p class="apd_msg"></p>
        <hr>
        <form id="replyForm" action="{{url('administrator/reply')}}">
            <div class="form-group">
                <label>Reply</label>
                <input type="hidden" value="" name="comment_id" class="comment_id">
                <textarea   placeholder="Message" class="form-control" rows="1" name="message" required></textarea>
            </div>
          <button type="submit" class="btn btn-primary btn-md">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@section('script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
 $('#default-datatable').DataTable( {
        "order": [[ 3, "desc" ]]
    } );

</script>
 <script>
      $('.status').change(function(){
        var mode= $(this).prop('checked');
        //alert(mode);
        var id=$(this).attr('id');       
        $.ajax({
          type:'GET',
          dataType:'JSON',
          url:'{{ url('administrator/comment_status') }}/'+id+'/'+mode,
          //data:'mode='+mode,
          success:function(data)
          {
            if(data.status=='success'){
                if(data.sts==1){
                    alert('Approved Comment');
                }else{
                    alert('Rejected Comment');
                }
            }
          }
        });
      });
      $('.view-comment').click(function(e){
        e.preventDefault();
        var href= $(this).attr('href');
        var cmt_id=$(this).attr('id');
        $('.comment_id').val(cmt_id);
        //alert(href);
        $.ajax({
          type:'GET',
          dataType:'html',
          url:href,
          //data:'mode='+mode,
          success:function(data)
          {
            
               $('.apd_msg').html(data);
               $('#myModal').modal('show');
            
          }
        });
      });
     // $('#replyForm').validate();
    </script>


@endsection