@extends('layouts.backend_layout')
@section('title')
     Blog Post  | Admin Panel
@stop
@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<style type="text/css">
 .form-group.cust {
  width: 30%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
.form-group > img {
  margin-top: 15px;
  text-align: center;
  width: 250px;
}
</style>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				<div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-stream1" aria-controls="stream" role="tab" data-toggle="tab">Blog Post </a></li>

					</ul><!-- .nav-tabs -->

					<!-- Tab panes -->

					<div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade" id="profile-stream1">
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="faqpage" action="{{ url('/administrator/blog/posts') }}" enctype="multipart/form-data">
                  <input type="hidden" value="@if(isset($data)) {{$data['id']}} @endif" name="id">
                   <div class="form-group">
                      <label>Title</label>
                      <input type="text"   placeholder="Title" class="form-control" name="title" required value="@if(isset($data)) {{$data['title']}} @endif">
                    </div>
                     <div class="form-group">
                      <label>Category</label>
                      <select class="form-control" name="category" required>
                            <option value="">Choose Category</option>
                          <option value="1" @if(isset($data))  @if($data['category']==1)  selected @endif @endif>Gexcrypto Blog</option>
                          <option value="2" @if(isset($data))  @if($data['category']==2)  selected @endif @endif>Latest Blog</option>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Posted Date</label>
                      <input type="text"  id="datepicker" placeholder="Posted date" class="form-control" name="posted_date" required value="@if(isset($data)) {{$data['posted_date']}} @endif">
                    </div>


                    <div class="form-group">
                      <label>Author</label>
                      <input type="text"   placeholder="Author" class="form-control" name="author" required value="@if(isset($data)) {{$data['author']}} @endif">
                    </div>
                    <div class="form-group">
                      <label>Reference Link</label>
                      <input type="url"   placeholder="Url" class="form-control" name="link"  value="@if(isset($data)) {{$data['link']}} @endif">
                    </div>
                   

                     <div class="form-group">
                      <label>Description</label>
                      <textarea type="text"   placeholder="Description" class="form-control" name="description" required >@if(isset($data)) {{$data['description']}} @endif</textarea>
                    </div>
                   
                    <div class="form-group">
                      <label>Blog Image</label>
                     <input type='file' onchange="readURL(this);" class="form-control" name="image" accept="image/*" >
                      <img id="blah" src="@if(isset($data)) @if($data['image']!='') {{asset('public/admin/blog/'.$data['image'])}} @endif @endif " alt="" />
                    </div>

                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->
            </div><!-- .tab-pane -->


            

					</div><!-- .tab-content -->
				</div><!-- #profile-components -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
@section('script')
<script>


</script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>
<script>
$('#faqpage').validate();
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
  $( function() {
    $( "#datepicker" ).datepicker({
       dateFormat: "yy-mm-dd"
    });
  } );


</script>
 <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
    //$(document).ready(function(){
      tinymce.init({
        selector: 'textarea',
        theme: "modern",
  skin: "lightgray",
  
  /* width and height of the editor */
  width: "100%",
  
  /* display statusbar */
  statubar: true,
  
  /* plugin */
  plugins: [
    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "save table contextmenu directionality emoticons template paste textcolor"
  ],

  /* toolbar */
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
  
  /* style */
  style_formats: [
    {title: "Headers", items: [
      {title: "Header 1", format: "h1"},
      {title: "Header 2", format: "h2"},
      {title: "Header 3", format: "h3"},
      {title: "Header 4", format: "h4"},
      {title: "Header 5", format: "h5"},
      {title: "Header 6", format: "h6"}
    ]},
    {title: "Inline", items: [
      {title: "Bold", icon: "bold", format: "bold"},
      {title: "Italic", icon: "italic", format: "italic"},
      {title: "Underline", icon: "underline", format: "underline"},
      {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
      {title: "Superscript", icon: "superscript", format: "superscript"},
      {title: "Subscript", icon: "subscript", format: "subscript"},
      {title: "Code", icon: "code", format: "code"}
    ]},
    {title: "Blocks", items: [
      {title: "Paragraph", format: "p"},
      {title: "Blockquote", format: "blockquote"},
      {title: "Div", format: "div"},
      {title: "Pre", format: "pre"}
    ]},
    {title: "Alignment", items: [
      {title: "Left", icon: "alignleft", format: "alignleft"},
      {title: "Center", icon: "aligncenter", format: "aligncenter"},
      {title: "Right", icon: "alignright", format: "alignright"},
      {title: "Justify", icon: "alignjustify", format: "alignjustify"}
    ]}
  ]
        
    });
   
    </script>

@endsection