@extends('layouts.backend_layout')
@section('title')
     Team member  | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.cust {
  width: 30%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
.form-group > img {
  margin-top: 15px;
  text-align: center;
  width: 250px;
}
</style>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				<div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-stream1" aria-controls="stream" role="tab" data-toggle="tab">Team Member </a></li>

					</ul><!-- .nav-tabs -->

					<!-- Tab panes -->

					<div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade" id="profile-stream1">
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="faqpage" action="{{ url('/administrator/team/members') }}" enctype="multipart/form-data">
                  <input type="hidden" value="@if(isset($data)) {{$data['id']}} @endif" name="id">
                   <div class="form-group">
                      <label>Name</label>
                      <input type="text"   placeholder="Title" class="form-control" name="name" required value="@if(isset($data)) {{$data['name']}} @endif">
                    </div>
                     <div class="form-group">
                      <label>Designation</label>
                      <input type="text"   placeholder="Designation" class="form-control" name="designation" required value="@if(isset($data)) {{$data['designation']}} @endif">
                    </div>
                    <div class="form-group">
                      <label>Linkedin Link</label>
                      <input type="url"   placeholder="Linkedin Link" class="form-control" name="linkedin"  value="@if(isset($data)) {{$data['linkedin']}} @endif">
                    </div>
                    <div class="form-group">
                      <label>Category</label>
                        <select class="form-control" required name="category">
                            <option value="">Choose One category</option>
                            @foreach($category as $cat)
                            <option value="{{$cat['id']}}" @if(isset($data)) @if($data['category']==$cat['id']) {{'selected'}} @endif @endif>{{$cat['title']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Wants to show on Home Page</label>
                        <select class="form-control" required name="show_on_home_page">
                            <option value="0" @if(isset($data)) @if($data['show_on_home_page']==0) {{'selected'}} @endif @endif>No</option>
                            <option value="1"  @if(isset($data)) @if($data['show_on_home_page']==1) {{'selected'}} @endif @endif>Yes</option>
                           
                        </select>
                    </div>
                    <div class="form-group">
                      <label>Profile Pic</label>
                     <input type='file' onchange="readURL(this);" class="form-control" name="image" accept="image/*" >
                      <img id="blah" src="@if(isset($data)) @if($data['image']!='') {{asset('public/admin/team/'.$data['image'])}} @endif @endif " alt="" />
                    </div>

                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->
            </div><!-- .tab-pane -->


            

					</div><!-- .tab-content -->
				</div><!-- #profile-components -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
@section('script')
<script>


</script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>
<script>
$('#faqpage').validate();
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
</script>

@endsection