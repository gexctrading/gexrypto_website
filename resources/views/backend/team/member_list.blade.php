  @extends('layouts.backend_layout')
@section('title')
     Team member List | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.cust {
  width: 30%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				

        <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Team Member List</h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div class="table-responsive">
              <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Sr/No.</th>                   
                 
                    <th>Name</th>  
                    <th>Designation</th>   
                    <th>Status</th>                 
                    <th>Action</th>                    
                  </tr>
                </thead>
             
                <tbody>
                <?php $i=1; ?>
                @foreach($list as $key=>$lst)
                  <tr>
                    <td>{{++$key}}</td>
                    <td>{{ $lst['name']}}</td>
                    <td>{{ $lst['designation']}}</td>
                     
                    <td>
                         <label class="switch">
                            <input type="checkbox" @if($lst['status']==1) {{'checked'}} @endif name="status" id="{{$lst['id']}}" class="status">
                            <span class="slider round"></span>
                        </label>

                    </td>
                    
                    <td><a href="{{url('administrator/edit/team-member/'.base64_encode(convert_uuencode($lst['id'])))}}"><i class="fa fa-edit btn btn-primary" aria-hidden="true"></i> <span class="text-muted"></span>
                    <a  class="delete" href="{{url('administrator/del/team-member/'.base64_encode(convert_uuencode($lst['id'])))}}"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i> <span class="text-muted"></span>
                    </a></td>
                 
                  </tr>
                @endforeach
                
                 
               
                </tbody>
              </table>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div>
            

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection

 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure to want to delete this category</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a class="btn btn-primary del_url" href="" >Yes, Delete</a>
        </div>
      </div>
      
    </div>
  </div>

@section('script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
 $('#default-datatable').DataTable( {
        "order": [[ 3, "desc" ]]
    } );

</script>
 <script>
     

      $('.delete').click(function(e){        
        e.preventDefault();          
        $('#myModal').modal('show');
         var url = $(this).attr('href');
         $('.del_url').attr('href',url);         

      });

    //$('.status').change(function(){
      $(document).on('change','.status',function(){
        var mode= $(this).prop('checked');
       // alert(mode);
        var id=$(this).attr('id');       
        $.ajax({
          type:'GET',
          dataType:'JSON',
          url:'{{ url('administrator/change_status_team_member') }}/'+id+'/'+mode,
          //data:'mode='+mode,
          success:function(data)
          {
            if(data.status=='success'){
                alert('Status Changed');
            }
          }
        });
      });
    </script>


@endsection