@extends('layouts.backend_layout')
@section('title')
    Job Applicants | Admin Panel
@stop
@section('content')


<main id="app-main" class="app-main">
  <div class="wrap">
    <section class="app-content">
        @if (session('custom_insert_success'))
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success ! </strong>
              <span>{{ Session::get('custom_insert_success') }}</span>
            </div>
        @endif
        @if (session('custom_edit_trust'))
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success ! </strong>
              <span>{{ Session::get('custom_edit_trust') }}</span>
            </div>
        @endif
        @if (session('custom_trust_error'))
             <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Success ! </strong>
                <span>{{ Session::get('custom_trust_error') }}</span>
              </div>  
        @endif
        @if (session('custom_info'))
            <div class = "alert alert-info alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
               <strong>Info ! </strong>
               <span>{{ Session::get('custom_info') }}</span>
            </div>   
        @endif

	   <div class = "panel-body">
  	      
	        <table class = "table table-striped">    
	            <thead>
	                <tr>
	                    <!-- <th><input type="checkbox" id="checkAll"/></th> -->
	                    <th>Full Name</th> 
                        <th>Position</th>  
	                    <th>Email</th>
	                    <th>Contact</th>
	                    <th>Location</th>    
  						<!-- <th>Current Employee</th> -->
  						<th>Experience</th>	
                        <th>Source</th>
  						<th>Resume</th>
  						<th>Apply Date</th>	
  						<th>Action</th>	
	                </tr>
	            </thead>
	            <tbody>
	                @foreach($record as $value)
	                    <tr>
	                        <td>{{ @($value->first_name) }} {{ @($value->last_name) }}</td>
	                        <td>
                                <?php //if(!empty($value->position)) {
                                    // if($value->position == "Linkedin")
                                    // {
                                    //     echo @($value->Careers->position);
                                    // }
                                    // else
                                    // {
                                    //     echo @($value->position);
                                    // }
                                    // } else
                                    // {
                                    //     echo @($value->Careers->position);
                                    // }

                                ?>
                                
                                {{ @($value->career->position) }}
                          </td>
	                        <td>{{ @($value->email) }}</td>
	                        <td>{{ @($value->contact) }}</td>
	                        <td>{{ @($value->location) }}</td>
	                        <!-- <td>{{ @($value->current) }}</td> -->
	                        <td>{{ @($value->experience) }}</td>
                          <td><?php if(!empty($value->position)) {  
                              if($value->position == "Linkedin")
                              {
                                echo "Linkedin";
                              }
                              else{
                                echo "Direct Applied";
                              } 
                            }
                            else {
                            echo "Direct Applied";
                            } ?></td>
	                        <td>
                            <?php if(!empty($value->resume)){ ?>
                          <a href="{{url('/public/assets/resume') }}/{{ @($value->resume) }} ">Download</a>
                            <?php } ?>
                          </td>
	                        <td>{{ @($value->date) }}</td>
	                        <td><span class="glyphicon glyphicon-trash" id=deleteButton onclick="openDeleteContent({{ @($value->id) }}); "></span>
                            
                          </td> 
	                    </tr>	                    
	                @endforeach
	            </tbody>
	        </table>
	        <div class = "modal fade" id = "deleteContent" tabindex = "-1" role = "dialog" 
           aria-labelledby = "myModalLabel" aria-hidden = "true">
            <div class = "modal-dialog">
                <div class = "modal-content">
                 
                    <div class = "modal-header">
                        <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                              &times;
                        </button>
                    
                        <h4 class = "modal-title" id = "myModalLabel">
                           Delete Record
                        </h4>
                    </div>
                 
                    <div class = "modal-body">
                        Are you sure you want to delete this record ?
                        <form action="{{url('/administrator/deleteApplicant')}}" method="POST" enctype="multipart/ form-data" role="form-horizontal">
                          {{csrf_field()}}
                            <div class="form-group">
                                <div class="col-md-6 delbutton">
                                    <input type="hidden" name="delete_content_id" id="delete_content_id">
                                    <input type="submit" class="btn btn-primary" value="Delete">
                                </div>
                            </div>
                        </form>  

                    </div>
                 
                    <div class = "modal-footer my-modal-footer">

                    </div>
                 
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          
        </div><!-- /.modal -->
        <!-- End Delete Content Modal -->
	    </div>      
    </section>
</div>
</main>
<style>
.mce-branding-powered-by {
    display: none;
}
ul.req_list {
  list-style: none;
}
ul.req_list li::before {
  content: "•"; 
  color: black; 
  font-size:22px;
}
ul.Highlyt_list {
  list-style: none;
}
ul.Highlyt_list li::before {
  content: "•"; 
  color: black; 
  font-size:22px;
}

  td{
    text-align: center;
  }
  th{
    text-align: center;
  }
  .my-modal-newcontent{
    height: 700px;
  }
  .modal-header.my-modal-header .close {
  margin-top: -20px;
  }
  .modal-footer.my-modal-footer{
  border: none;
  }
  .modal-content.my-modal-content{
  height: 350px;
  }
  .modal-content.my-service-modal-content{
    height: 550px;
  }
  .delbutton{
  margin-left:-15px;
  }
  .btn-success{
    padding:4px !important;
    font-size: 12px !important;
  }
  .btn-danger{
    padding:4px !important;
    font-size: 12px !important;
  }
  .glyphicon-edit{
    color: green;
    font-weight: 800;
    font-size: 16px;
    cursor: pointer;
  }
  .glyphicon-trash{
    color: red;
    font-weight: 800;
    font-size: 16px;
    cursor: pointer;
  }
  .image_class{
    margin-top: 6px;
  }
  .glyphicon-eye-open{
    font-weight: 800;
    font-size: 16px;
    cursor: pointer;
  }
</style>

<script type="text/javascript">
    function openDeleteContent(id){
    $('#deleteContent').modal('show');
    $('#delete_content_id').prop('value',id);

    }
</script>
@endsection
