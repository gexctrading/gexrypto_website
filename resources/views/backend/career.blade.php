@extends('layouts.backend_layout')
@section('title')
    Career | Admin Panel
@stop
@section('content')


<main id="app-main" class="app-main">
  <div class="wrap">
    <section class="app-content">
        @if (session('custom_insert_success'))
            <div class="alert alert-success" role="alert">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success ! </strong>
              <span>{{ Session::get('custom_insert_success') }}</span>
            </div>
        @endif
        @if (session('custom_info'))
            <div class = "alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                 <strong>Info ! </strong>
                 <span>{{ Session::get('custom_info') }}</span>
            </div>   
        @endif
        @if (session('custom_trust_error'))
             <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <strong>Success ! </strong>
                <span>{{ Session::get('custom_trust_error') }}</span>
              </div>  
        @endif

        <div class = "panel-group" id = "accordion">
            <div class = "panel panel-default">
              
                <div class = "panel-heading">
                    <h4 class = "panel-title">
                        <a data-toggle = "collapse" data-parent = "#accordion" href = "#collapseOne">
                           Section 1
                        </a>
                    </h4>
                </div>
              
                <div id = "collapseOne" class = "panel-collapse collapse">
                    <div class = "panel-body">
                        <form id="serviceHeadingForm" action="{{url('/administrator/empcareer')}}" class="form-horizontal"     role="form" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Title</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="title" placeholder="Title">
                                </div>
                                <label for="name" class="col-md-1 control-label">Description</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="description" placeholder="Description">
                                </div>
                                <div class="col-md-2 col-md-offset-1">
                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
           
                        </form>
                        <table class = "table table-striped">
                                 
                            <thead>
                                <tr>
                                    <th>S.No.</th>   
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                       
                            <tbody>
                                <?php $i =1; ?>
                                @foreach($section1 as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->title}}</td>
                                        <td>{{$value->description}}</td>
                                        <td><span class="glyphicon glyphicon-edit" id="editSection" onclick="openEditSection1({{$value->id}},this);" data-title="{{$value->title}}" data-description="{{$value->description}}" data-id="{{$value->id}}" data-toggle="modal"></span> <span class="glyphicon glyphicon-trash" id=deleteButton onclick="openDeleteSection1({{$value->id}}); "></span></td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach    
                            </tbody>
                        
                        </table>
                        <div class="modal fade" id="edit_mainheading" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content my-modal-content">
                                    <div class="modal-header my-modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
                                        <button type="button" class="close" data-dismiss="modal"        aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="editServiceHeadingForm" action="{{url('/administrator/editempsec1')}}" method="POST" enctype="multipart/form-data" role="form-horizontal">
                                            {{csrf_field()}}
                            
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-2 col-md-offset-2 control-label">Title</label>
                                                    <div class="col-md-6">
                                                         
                                                        <input type="text" class="form-control" name="edit_title" id="edit_title">                                                                             
                                                    </div>
                                                </div>
                                            </div> 
                           
                                            <div class="form-group">
                                                <div class="row"> 
                                                    <label for="name" class="col-md-2 col-md-offset-2 control-label">Description</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="edit_description" id="edit_description" class="form-control">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <input type="hidden" name="edit_secid1" id="edit_secid1">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </div>
                                        </form>
                           
                                    </div>
                                    <div class="modal-footer my-modal-footer">  
                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "modal fade" id = "deleteSection1" tabindex = "-1" role = "dialog" 
                        aria-labelledby = "myModalLabel" aria-hidden = "true">
           
                            <div class = "modal-dialog">
                                <div class = "modal-content">
                                 
                                    <div class = "modal-header">
                                        <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                              &times;
                                        </button>
                                    
                                        <h4 class = "modal-title" id = "myModalLabel">
                                           Delete Record
                                        </h4>
                                    </div>
                                 
                                    <div class = "modal-body">
                                        Are you sure you want to delete this record ?
                                        <form action="{{url('/administrator/deletesect1')}}" method="POST" enctype="multipart/ form-data" role="form-horizontal">
                                          {{csrf_field()}}
                                            <div class="form-group">
                                              <div class="col-md-6 delbutton">
                                                <input type="hidden" name="delete_secid" id="delete_secid">
                                                <input type="submit" class="btn btn-primary" value="Delete">
                                              </div>
                                            </div>
                                        </form>  

                                    </div>
                                 
                                    <div class = "modal-footer my-modal-footer">

                                    </div>
                                 
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          
                        </div><!-- /.modal -->
                    </div>
                </div>  
            </div>
           
            <div class = "panel panel-default">
                <div class = "panel-heading">
                    <h4 class = "panel-title">
                        <a data-toggle = "collapse" data-parent = "#accordion" href = "#collapseTwo">
                           Section 2
                        </a>
                    </h4>
                </div>

                <div id = "collapseTwo" class = "panel-collapse collapse">
                    <div class = "panel-body">
                        <form id="serviceHeadingForm2" action="{{url('/administrator/empcareer2')}}" class="form-horizontal"     role="form" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Title</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="title" placeholder="Title">
                                </div>
                                <label for="name" class="col-md-1 control-label">Description</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="description" placeholder="Description">
                                </div>
                                <div class="col-md-2 col-md-offset-1">
                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
           
                        </form>
                        <table class = "table table-striped">
                                 
                            <thead>
                                <tr>
                                    <th>S.No.</th>   
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                       
                            <tbody>
                                <?php $i =1; ?>
                                @foreach($section2 as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->title}}</td>
                                        <td>{{$value->description}}</td>
                                        <td><span class="glyphicon glyphicon-edit" id="editSection" onclick="openEditSection2({{$value->id}},this);" data-title="{{$value->title}}" data-description="{{$value->description}}" data-id="{{$value->id}}" data-toggle="modal"></span> <span class="glyphicon glyphicon-trash" id=deleteButton onclick="openDeleteSection2({{$value->id}}); "></span></td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach    
                            </tbody>
                        
                        </table>
                        <div class="modal fade" id="edit_mainheading2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content my-modal-content">
                                    <div class="modal-header my-modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
                                        <button type="button" class="close" data-dismiss="modal"        aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="editCareerSection2" action="{{url('/administrator/editempsec2')}}" method="POST" enctype="multipart/form-data" role="form-horizontal">
                                            {{csrf_field()}}
                            
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-2 col-md-offset-2 control-label">Title</label>
                                                    <div class="col-md-6">
                                                         
                                                        <input type="text" class="form-control" name="edit_title" id="edit_title2">                                                                             
                                                    </div>
                                                </div>
                                            </div> 
                           
                                            <div class="form-group">
                                                <div class="row"> 
                                                    <label for="name" class="col-md-2 col-md-offset-2 control-label">Description</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="edit_description" id="edit_description2" class="form-control">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <input type="hidden" name="edit_secid1" id="edit_secid2">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </div>
                                        </form>
                           
                                    </div>
                                    <div class="modal-footer my-modal-footer">  
                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "modal fade" id = "deleteSection2" tabindex = "-1" role = "dialog" 
                        aria-labelledby = "myModalLabel" aria-hidden = "true">
           
                            <div class = "modal-dialog">
                                <div class = "modal-content">
                                 
                                    <div class = "modal-header">
                                        <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                              &times;
                                        </button>
                                    
                                        <h4 class = "modal-title" id = "myModalLabel">
                                           Delete Record
                                        </h4>
                                    </div>
                                 
                                    <div class = "modal-body">
                                        Are you sure you want to delete this record ?
                                        <form action="{{url('/administrator/deletesect2')}}" method="POST" enctype="multipart/ form-data" role="form-horizontal">
                                          {{csrf_field()}}
                                            <div class="form-group">
                                              <div class="col-md-6 delbutton">
                                                <input type="hidden" name="delete_secid" id="delete_secid2">
                                                <input type="submit" class="btn btn-primary" value="Delete">
                                              </div>
                                            </div>
                                        </form>  

                                    </div>
                                 
                                    <div class = "modal-footer my-modal-footer">

                                    </div>
                                 
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          
                        </div><!-- /.modal -->
                    </div>
                </div> 
            </div>
           
            <div class = "panel panel-default">
                <div class = "panel-heading">
                    <h4 class = "panel-title">
                        <a data-toggle = "collapse" data-parent = "#accordion" href = "#collapseThree">
                       Categories
                        </a>
                    </h4>
                </div>
             
                <div id = "collapseThree" class = "panel-collapse collapse">
                    <div class = "panel-body">
                        <form id="serviceCareerForm3" action="{{url('/administrator/empcareer3')}}" class="form-horizontal"     role="form" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Paragraph 1</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="title" placeholder="Paragraph 1">
                                </div>
                                <label for="name" class="col-md-2 control-label">Paragpraph 2</label>
                                <div class="col-md-3">
                                    <input type="text" class="form-control" name="description" placeholder="Paragraph 2">
                                </div>
                                <div class="col-md-2">
                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
           
                        </form>
                        <table class = "table table-striped">
                                 
                            <thead>
                                <tr>
                                    <th>S.No.</th>   
                                    <th>Paragraph 1</th>
                                    <th>Paragraph 2</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                       
                            <tbody>
                                <?php $i =1; ?>
                                @foreach($section3 as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->title}}</td>
                                        <td>{{$value->description}}</td>
                                        <td><span class="glyphicon glyphicon-edit" id="editSection" onclick="openEditSection3({{$value->id}},this);" data-title="{{$value->title}}" data-description="{{$value->description}}" data-id="{{$value->id}}" data-toggle="modal"></span> <span class="glyphicon glyphicon-trash" id=deleteButton onclick="openDeleteSection3({{$value->id}}); "></span></td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach    
                            </tbody>
                        
                        </table>
                        <div class="modal fade" id="edit_mainheading3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content my-modal-content">
                                    <div class="modal-header my-modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
                                        <button type="button" class="close" data-dismiss="modal"        aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="editCareerSection3" action="{{url('/administrator/editempsec3')}}" method="POST" enctype="multipart/form-data" role="form-horizontal">
                                            {{csrf_field()}}
                            
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-3 col-md-offset-1 control-label">Paragraph 1</label>
                                                    <div class="col-md-6">
                                                         
                                                        <input type="text" class="form-control" name="edit_title" id="edit_title3">                                                                             
                                                    </div>
                                                </div>
                                            </div> 
                           
                                            <div class="form-group">
                                                <div class="row"> 
                                                    <label for="name" class="col-md-3 col-md-offset-1 control-label">Paragraph 2</label>
                                                    <div class="col-md-6">
                                                        <input type="text" name="edit_description" id="edit_description3" class="form-control">
                                                    </div>
                                                </div>
                                            </div>  
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-4">
                                                    <input type="hidden" name="edit_secid1" id="edit_secid3">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </div>
                                        </form>
                           
                                    </div>
                                    <div class="modal-footer my-modal-footer">  
                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "modal fade" id = "deleteSection3" tabindex = "-1" role = "dialog" 
                        aria-labelledby = "myModalLabel" aria-hidden = "true">
           
                            <div class = "modal-dialog">
                                <div class = "modal-content">
                                 
                                    <div class = "modal-header">
                                        <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                              &times;
                                        </button>
                                    
                                        <h4 class = "modal-title" id = "myModalLabel">
                                           Delete Record
                                        </h4>
                                    </div>
                                 
                                    <div class = "modal-body">
                                        Are you sure you want to delete this record ?
                                        <form action="{{url('/administrator/deletesect3')}}" method="POST" enctype="multipart/ form-data" role="form-horizontal">
                                          {{csrf_field()}}
                                            <div class="form-group">
                                              <div class="col-md-6 delbutton">
                                                <input type="hidden" name="delete_secid" id="delete_secid3">
                                                <input type="submit" class="btn btn-primary" value="Delete">
                                              </div>
                                            </div>
                                        </form>  

                                    </div>
                                 
                                    <div class = "modal-footer my-modal-footer">

                                    </div>
                                 
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          
                        </div><!-- /.modal -->
                    </div>
                </div>
            </div>
            <div class = "panel panel-default">
                <div class = "panel-heading">
                    <h4 class = "panel-title">
                        <a data-toggle = "collapse" data-parent = "#accordion" href = "#collapseFour">
                            Openings  
                        </a>
                    </h4>
                </div>
             
                <div id = "collapseFour" class = "panel-collapse collapse">
                    <div class = "panel-body">
                      <form id="serviceCareerForm4" action="{{url('/administrator/empcareer4')}}" class="form-horizontal"     role="form" method="POST" enctype="multipart/form-data">
                            {{csrf_field()}}

                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Vacancy</label>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" name="vacancy" placeholder="Vacancy">
                                </div>
                               
                                      <label for="name" class="col-md-2 control-label">Category</label>
                                <div class="col-md-3">
                                    <select name="category" class="form-control">
                                    <?php foreach($section3 as $sec){ ?>
                                        <option value="<?= $sec->description ?>"><?= $sec->description ?></option>    
                                    <?php } ?>
                                    </select>
                                </div>                               
                            </div>
    
                                <div class="form-group">
                             
                                <label for="name" class="col-md-2 control-label">Position</label>
                                <div class="col-md-3">
                                    <input type="text" value="" placeholder="Ex: Php Developer" name="position" class="form-control">
                                </div> 
                                    </div> 
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Description</label>
                                <div class="col-md-3">
                                    <textarea class="form-control" name="description" placeholder="Description"></textarea> 
                                </div>
                                <label for="name" class="col-md-2 control-label">Experience</label>
                                <div class="col-md-3">
                                    <input type="number" class="form-control" name="experience" placeholder="Experience">
                                </div>                                
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Job Responsibilities</label>
                                <div class="col-md-8">
                                    <textarea class="tinymce form-control" name="responsibility" placeholder="Job Responsibilities"></textarea>
                                </div>                                     
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Skills</label>
                                <div class="col-md-8">
                                    <textarea class="tinymce form-control" name="skills" placeholder="Skills"></textarea>
                                </div>                                     
                            </div>
                            <div class="form-group">
                                <label for="name" class="col-md-2 control-label">Location</label>
                                <div class="col-md-8">
                                    <textarea class="tinymce form-control" name="location" placeholder="Location"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-2">
                                    <input type="submit" class="btn btn-primary" value="Save">
                                </div>
                            </div>
           
                        </form>
                        <table class="table table-striped">
                                 
                            <thead>
                                <tr>
                                    <th>S.No.</th>   
                                    <th>Vacancy</th>
                                    <th>Position</th>
                                    <th>Description</th>
                                    <th>Experience</th>
                                    <th>Location</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                       
                            <tbody>
                                <?php $i =1; ?>
                                @foreach($section4 as $value)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>{{$value->vacancy}}</td>
                                        <td>{{$value->position}}</td>
                                        <td class="desc_data">{{substr($value->description,0,200).'...'}}</td>
                                        <td>{{$value->experience}}</td>
                                        <td>{{$value->location}}</td>
                                        <td><span class="glyphicon glyphicon-edit" id="editSection" onclick="openEditSection4({{$value->id}},this);" data-vacancy="{{$value->vacancy}}" data-position="{{$value->position}}" data-description="{{$value->description}}" data-experience="{{$value->experience}}" data-skills="{{$value->skills}}" data-location="{{$value->location}}" data-id="{{$value->id}}" data-responsibility="{{$value->responsibility}}" data-toggle="modal"></span> <span class="glyphicon glyphicon-trash" id=deleteButton onclick="openDeleteSection4({{$value->id}}); "></span></td>
                                    </tr>
                                    <?php $i++; ?>
                                @endforeach    
                            </tbody>
                        
                        </table>
                        <div class="modal fade" id="edit_mainheading4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content my-modal-content-sec4 ">
                                    <div class="modal-header my-modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Edit Record</h5>
                                        <button type="button" class="close" data-dismiss="modal"        aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form id="editCareerSection4" action="{{url('/administrator/editempsec4')}}" method="POST" enctype="multipart/form-data" role="form-horizontal">
                                            {{csrf_field()}}
                            
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-2 control-label">Vacancy</label>
                                                    <div class="col-md-10">
                                                         
                                                        <input type="number" class="form-control" name="edit_vacancy" id="edit_vacancy">                                                                             
                                                    </div>
                                                </div>
                                            </div> 
                           
                                            <div class="form-group">
                                                <div class="row"> 
                                                    <label for="name" class="col-md-2 control-label">Position</label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="edit_position" id="edit_position" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="row"> 
                                                    <label for="name" class="col-md-2  control-label">Description</label>
                                                    <div class="col-md-10">
                                                        <input type="text" name="edit_description4" id="edit_description4" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row"> 
                                                    <label for="name" class="col-md-2 control-label">Experience</label>
                                                    <div class="col-md-10">
                                                        <input type="number" name="edit_experience" id="edit_experience" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-2 control-label">Job Responsibility</label>
                                                    <div class="col-md-10">
                                                      <textarea class="tinymce" name="edit_responsibility" id="edit_responsibility"></textarea> 
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-2 control-label">Skills</label>
                                                    <div class="col-md-10">
                                                      <textarea class="tinymce" name="edit_skills" id="edit_skills"></textarea> 
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-2 control-label">Location</label>
                                                    <div class="col-md-10">
                                                      <textarea class="tinymce" name="edit_location" id="edit_location"></textarea> 
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                          <!--   <div class="form-group">
                                                <div class="row">
                                                    <label for="name" class="col-md-2 control-label">Location</label>
                                                    <div class="col-md-10">
                                                      <input type="text" class="form-control" name="edit_location" id="edit_location" /> 
                                                    </div>
                                                </div>
                                            </div>   -->
                                            <div class="form-group">
                                                <div class="col-md-6 col-md-offset-2">
                                                    <input type="hidden" name="edit_secid4" id="edit_secid4">
                                                    <input type="submit" class="btn btn-primary" value="Save">
                                                </div>
                                            </div>
                                        </form>
                           
                                    </div>
                                    <div class="modal-footer my-modal-footer">  
                            
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class = "modal fade" id = "deleteSection4" tabindex = "-1" role = "dialog" 
                        aria-labelledby = "myModalLabel" aria-hidden = "true">
           
                            <div class = "modal-dialog">
                                <div class = "modal-content">
                                 
                                    <div class = "modal-header">
                                        <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                                              &times;
                                        </button>
                                    
                                        <h4 class = "modal-title" id = "myModalLabel">
                                           Delete Record
                                        </h4>
                                    </div>
                                 
                                    <div class = "modal-body">
                                        Are you sure you want to delete this record ?
                                        <form action="{{url('/administrator/deletesect4')}}" method="POST" enctype="multipart/ form-data" role="form-horizontal">
                                          {{csrf_field()}}
                                            <div class="form-group">
                                              <div class="col-md-6 delbutton">
                                                <input type="hidden" name="delete_secid" id="delete_secid4">
                                                <input type="submit" class="btn btn-primary" value="Delete">
                                              </div>
                                            </div>
                                        </form>  

                                    </div>
                                 
                                    <div class = "modal-footer my-modal-footer">

                                    </div>
                                 
                                </div><!-- /.modal-content -->
                            </div><!-- /.modal-dialog -->
                          
                        </div><!-- /.modal -->  
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
</main>
<style type="text/css">
td.desc_data{
    text-align: left;
    width: 200px;
}
td{
    text-align: center;
  }
  th{
    text-align: center;
  }
  ul {
    list-style-type: disc !important;
}
  .modal-header.my-modal-header .close {
  margin-top: -20px;
  }
  .modal-footer.my-modal-footer{
  border: none;
  }
  .modal-content.my-modal-content{
  height: 350px;
  }
  .my-modal-content-sec4{
    height: 100%;
  }
  .modal-content.my-service-modal-content{
    height: 550px;
  }
  .delbutton{
  margin-left:-15px;
  }
  .btn-success{
    padding:4px !important;
    font-size: 12px !important;
  }
  .btn-danger{
    padding:4px !important;
    font-size: 12px !important;
  }
  .glyphicon-edit{
    color: green;
    font-weight: 800;
    font-size: 16px;
    cursor: pointer;
  }
  .glyphicon-trash{
    color: red;
    font-weight: 800;
    font-size: 16px;
    cursor: pointer;
  }
  .image_class{
    margin-top: 6px;
  }
</style>


<script type="text/javascript">
   function openEditSection1(id,sect){
    var secTitle = $(sect).data('title');
    var secDescription = $(sect).data('description');
    $('#edit_mainheading').modal('show');
    $('#edit_title').prop('value', secTitle);
    $('#edit_description').prop('value', secDescription);
    $('#edit_secid1').prop('value',id);
   }
   function openDeleteSection1(id){
    $('#deleteSection1').modal('show');
    $('#delete_secid').prop('value',id);
   }

   function openEditSection2(id,sect){
    var secTitle = $(sect).data('title');
    var secDescription = $(sect).data('description');
    $('#edit_mainheading2').modal('show');
    $('#edit_title2').prop('value', secTitle);
    $('#edit_description2').prop('value', secDescription);
    $('#edit_secid2').prop('value',id);
   }
   function openDeleteSection2(id){
    $('#deleteSection2').modal('show');
    $('#delete_secid2').prop('value',id);
   }

   function openEditSection3(id,sect){
    var secTitle = $(sect).data('title');
    var secDescription = $(sect).data('description');
    $('#edit_mainheading3').modal('show');
    $('#edit_title3').prop('value', secTitle);
    $('#edit_description3').prop('value', secDescription);
    $('#edit_secid3').prop('value',id);
   }
   function openDeleteSection3(id){
    $('#deleteSection3').modal('show');
    $('#delete_secid3').prop('value',id);
   }
   function openEditSection4(id,sect){
    var vacancy = $(sect).data('vacancy');
    var position = $(sect).data('position');
    var description = $(sect).data('description');
    var experience = $(sect).data('experience');
    var responsibility = $(sect).data('responsibility');
    var skills = $(sect).data('skills');
    var location = $(sect).data('location');
    $('#edit_mainheading4').modal('show');
    $(window).scrollTop(0);

    $('#edit_vacancy').prop('value', vacancy);
    $('#edit_position').prop('value', position);
    $('#edit_description4').prop('value', description);
    $('#edit_experience').prop('value', experience);
    $('textarea#edit_responsibility').val('value', responsibility);
    tinyMCE.get('edit_responsibility').setContent(responsibility);
    $('textarea#edit_skills').val('value', skills);
    tinyMCE.get('edit_location').setContent(location);
    $('textarea#edit_location').val('value', location);
    tinyMCE.get('edit_skills').setContent(skills);
    // $('#edit_location').prop('value', location);
    $('#edit_secid4').prop('value', id);

   } 
   function openDeleteSection4(id){
    $('#deleteSection4').modal('show');
    $('#delete_secid4').prop('value',id);
   }
</script>

<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
  tinymce.init({
  /* replace textarea having class .tinymce with tinymce editor */
  selector: "textarea.tinymce",
  
  /* theme of the editor */
  theme: "modern",
  skin: "lightgray",
  
  /* width and height of the editor */
  width: "100%",
  
  /* display statusbar */
  statubar: true,
  
  /* plugin */
  plugins: [
    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "save table contextmenu directionality emoticons template paste textcolor"
  ],

  /* toolbar */
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
  
  /* style */
  style_formats: [
    {title: "Headers", items: [
      {title: "Header 1", format: "h1"},
      {title: "Header 2", format: "h2"},
      {title: "Header 3", format: "h3"},
      {title: "Header 4", format: "h4"},
      {title: "Header 5", format: "h5"},
      {title: "Header 6", format: "h6"}
    ]},
    {title: "Inline", items: [
      {title: "Bold", icon: "bold", format: "bold"},
      {title: "Italic", icon: "italic", format: "italic"},
      {title: "Underline", icon: "underline", format: "underline"},
      {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
      {title: "Superscript", icon: "superscript", format: "superscript"},
      {title: "Subscript", icon: "subscript", format: "subscript"},
      {title: "Code", icon: "code", format: "code"}
    ]},
    {title: "Blocks", items: [
      {title: "Paragraph", format: "p"},
      {title: "Blockquote", format: "blockquote"},
      {title: "Div", format: "div"},
      {title: "Pre", format: "pre"}
    ]},
    {title: "Alignment", items: [
      {title: "Left", icon: "alignleft", format: "alignleft"},
      {title: "Center", icon: "aligncenter", format: "aligncenter"},
      {title: "Right", icon: "alignright", format: "alignright"},
      {title: "Justify", icon: "alignjustify", format: "alignjustify"}
    ]}
  ]
});
</script>
<script type="text/javascript">
  $(document).ready(function(){

        $('#serviceHeadingForm').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "title": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "description": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
            }
        })
    });

    $(document).ready(function(){

        $('#editServiceHeadingForm').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "edit_title": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "edit_description": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  }
            }
        })
    });
    $(document).ready(function(){

        $('#serviceHeadingForm2').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "title": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "description": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  }
            }
        })
    });
     $(document).ready(function(){

        $('#editCareerSection2').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "edit_title": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "edit_description": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  }
            }
        })
    });
     $(document).ready(function(){

        $('#serviceCareerForm3').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "title": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "description": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  }
            }
        })
    });


     $(document).ready(function(){

        $('#editCareerSection3').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "edit_title": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "edit_description": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  }
            }
        })
    });
     $(document).ready(function(){

        $('#serviceCareerForm4').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "vacancy": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "position": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  
                  "experience": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                   "location": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  }
            }
        })
    });

    $(document).ready(function(){

        $('#editCareerSection4').formValidation({
            framework: 'bootstrap',
            excluded: [':disabled'],
            message: 'This value is not valid',
            icon: 
            {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            err: 
            {
                container: 'popover'
            },
            fields:
            {
                "edit_vacancy": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  "edit_position": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                  
                  "edit_experience": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  },
                   "edit_location": 
                  {
                      validators: 
                      {
                          notEmpty: 
                          {
                              message: 'Field is required'
                          }
                      }
                  }
            }
        })
    }); 
    </script>
@endsection
