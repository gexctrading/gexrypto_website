  @extends('layouts.backend_layout')
@section('title')
     Newsletter | Admin Panel
@stop
@section('content')


</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
  <section class="app-content">
    <div class="row">
      <div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
        

        <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Newsletters</h4>
          </header><!-- .widget-header -->
           @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
        
          <hr class="widget-separator">
          <div class="widget-body">
            <div class="table-responsive">
              <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Sr/No.</th>
                    <th>Title</th>
                    <th>Description</th>                   
                    <th>Action</th>                    
                  </tr>
                </thead>
             
                <tbody>
                <?php $i=1; ?>
                @foreach($list as $key=>$lst)
                  <tr>
                    <td>{{$i}}</td>
                    <td>{{ substr($lst['title'],0,50)}}</td>
                    <td>{{ substr($lst['description'],0,50). '..'}}</td>
                   
                    <td> <a  class="delete" href="{{url('administrator/del/newsletter/'.base64_encode(convert_uuencode($lst['id'])))}}"><i class="fa fa-trash btn btn-danger" aria-hidden="true"></i> <span class="text-muted"></span>
                    </a>
                    <a  class="send" href="{{url('administrator/send/newsletter/'.base64_encode(convert_uuencode($lst['id'])))}}"><i class="fa fa-envelope-square  btn btn-success" aria-hidden="true"></i> <span class="text-muted"></span>
                    </a>
                    </td>
                 
                  </tr>
                  <?php $i++; ?>
                @endforeach
                </tbody>
              </table>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div>
            

    </div><!-- .row -->
  </section><!-- #dash-content -->
</div><!-- .row -->

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure to want to delete this Subscriber ?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <a class="btn btn-primary del_url" href="" >Yes, Delete</a>
        </div>
      </div>
      
    </div>
  </div>

</main>
<!--========== END app main -->
@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
 $('#default-datatable').DataTable( {
        "order": [[ 3, "desc" ]]
    } );
 $('.delete').click(function(e){        
        e.preventDefault();          
        $('#myModal').modal('show');
         var url = $(this).attr('href');
         $('.del_url').attr('href',url);         

      });
</script>



@endsection