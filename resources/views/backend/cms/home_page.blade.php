  @extends('layouts.backend_layout')
@section('title')
     CMS Home Page | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.cust {
  width: 30%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
</style>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				<div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-stream1" aria-controls="stream" role="tab" data-toggle="tab">Home Page</a></li>

					</ul><!-- .nav-tabs -->

					<!-- Tab panes -->

					<div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade" id="profile-stream1">
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="homepage" action="{{ url('/administrator/home_page') }}">
                   <div class="form-group">
                      <label>Title</label>
                      <textarea   placeholder="Title" class="form-control" rows="1" name="title" required>{{$content['title']}}</textarea>
                    </div>

                    <div class="form-group">
                      <label>Heading</label>
                      <textarea  placeholder="Heading" class="form-control" name="heading" required>{{$content['heading']}}</textarea>
                    </div>
                    <div class="form-group cust">
                        <label>Currencies</label>
                        <input type="number" class="form-control " value="{{$content['currencies']}}" name="currencies" required>
                    </div>
                    <div class="form-group cust">
                        <label>Assets</label>
                        <input type="number" class="form-control cust" value="{{$content['assets']}}" name="assets" required>
                    </div>
                    <div class="form-group cust">
                        <label>Markets</label>
                        <input type="number" class="form-control cust" value="{{$content['markets']}}" name="markets" required>
                    </div>
                    <div class="form-group">
                        <label>DLT Heading</label>
                        <input type="text" class="form-control " value="{{$content['dlt_heading']}}" name="dlt_heading" required>
                    </div>
                    <div class="form-group">
                      <label>DLT Description</label>
                      <textarea   placeholder="Description" class="form-control" name="dlt_description" required>{{$content['dlt_description']}}</textarea>
                    </div>
                     <div class="form-group">
                        <label>Experience (In No.)</label>
                        <input type="number" class="form-control" value="{{$content['exp_year']}}" name="exp_year" required>
                    </div>
                     <div class="form-group">
                      <label>Exp. Description</label>
                      <textarea "  placeholder="Description" class="form-control" name="exp_description" required>{{$content['exp_description']}}</textarea>
                    </div>
                     <div class="form-group">
                      <label>Why Global Exchange Platform?</label>
                      <textarea   placeholder="Global Exchange" class="form-control" name="global_exchange" required>{{$content['global_exchange']}}</textarea>
                    </div>

                    <div class="form-group">
                      <label>Team Approach</label>
                      <textarea   placeholder="Team Approach" class="form-control" name="team_approach" required>{{$content['team_approach']}}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Honest Delivery</label>
                      <textarea  placeholder="Honest Delivery" class="form-control" name="honest_del" required>{{$content['honest_del']}}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Active Management</label>
                      <textarea  placeholder="Active Management" class="form-control" name="active_management" required>{{$content['active_management']}}</textarea>
                    </div>

                    <div class="form-group">
                      <label>Global Exchange Resource</label>
                      <textarea name="global_exh_resource"  placeholder="Global Exchange" class="form-control" required>{{$content['global_exh_resource']}}</textarea>
                    </div>
                    <div class="form-group">
                      <label>Multi-Language Support </label>
                      <textarea name="multi_lang_support"  placeholder="Multi-Language Support" class="form-control" required>{{$content['multi_lang_support']}}</textarea>
                    </div>
                     <div class="form-group">
                      <label>Technical Team </label>
                      <textarea name="technical_team"  placeholder="Technical Team" class="form-control" required>{{$content['technical_team']}}</textarea>
                    </div>
                     <div class="form-group">
                      <label>Whitepaper </label>
                      <textarea name="whitepaper"  placeholder="Whitepaper" class="form-control" required>{{$content['whitepaper']}}</textarea>
                    </div>


                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->
            </div><!-- .tab-pane -->


            

					</div><!-- .tab-content -->
				</div><!-- #profile-components -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
@section('script')
<script>


</script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>
<script>
$('#homepage').validate();
</script>

@endsection