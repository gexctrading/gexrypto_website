  @extends('layouts.backend_layout')
@section('title')
     Contact List | Admin Panel
@stop
@section('content')


</style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				

        <div class="col-md-12">
        <div class="widget">
          <header class="widget-header">
            <h4 class="widget-title">Contact List</h4>
          </header><!-- .widget-header -->
          <hr class="widget-separator">
          <div class="widget-body">
            <div class="table-responsive">
              <table id="default-datatable" data-plugin="DataTable" class="table table-striped" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>Sr/No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Date</th>
                    <th>Action</th>                    
                  </tr>
                </thead>
             
                <tbody>
                <?php $i=1; ?>
                @foreach($list as $key=>$lst)
                  <tr>
                    <td>{{++$key}}</td>
                    <td>{{ substr($lst['name'],0,50)}}</td>
                    <td>{{ substr($lst['email'],0,50). '..'}}</td>
                    <td><?php  $date=date_create($lst['created_at']); 
                        echo $date=date_format($date,' j F Y');
                    ?></td>
                    <td><i class="fa fa-eye btn btn-primary view_msg" aria-hidden="true" rel="{{$lst['id']}}"></i> <span class="text-muted"></span></a></td>
                 
                  </tr>
                @endforeach
                
                 
               
                </tbody>
              </table>
            </div>
          </div><!-- .widget-body -->
        </div><!-- .widget -->
      </div>
            

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog cust">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Message</h4>
      </div>
      <div class="modal-body">
        <h5 class="apd_msg"></h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->





@endsection

@section('script')
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
 $('#default-datatable').DataTable( {
        "order": [[ 3, "desc" ]]
    } );

</script>
 <script>
      //$('.view_msg').click(function(){
        $(document).on('click','.view_msg',function(){
        
        var id=$(this).attr('rel');       
        $.ajax({
          type:'GET',
          dataType:'JSON',
          url:'{{ url('administrator/view-contact/') }}/'+id,
          //data:'mode='+mode,
          success:function(data)
          {
            if(data.status=='success'){
              
              $('.apd_msg').text(data.message);
              $('#myModal').modal('show');
            }
          }
        });
      });
    </script>


@endsection