	  @extends('layouts.backend_layout')
@section('title')
     Footer | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.custm {
  width: 40%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
.form-control.cutm {
  float: left;
  margin-right: 10px;
  width: 175px;
}
.btn.btn-primary.btn-md {
  margin-top: 40px;
}
</style>
<style type="text/css" href="{{asset('public/css/bootstrap.min.css')}}"></style>
<style type="text/css" href="{{asset('public/css/bootstrap-datetimepicker.css')}}"></style>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				<div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-stream1" aria-controls="stream" role="tab" data-toggle="tab">Footer </a></li>

					</ul><!-- .nav-tabs -->

					<!-- Tab panes -->

					<div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade" id="profile-stream1">
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="homepage" action="{{ url('/administrator/footer_menu') }}">

                   <div class="form-group">
                      <label>Content</label>
                      <textarea   placeholder="Title" class="form-control" rows="1" name="content" required>{{$content['content']}}</textarea>
                    </div>

                    <div class="form-group">
                      <label>Disclosure</label>
                      <textarea  placeholder="Heading" class="form-control" name="disclosure" required >{{$content['disclosure']}}</textarea>
                    </div>

                    <div class="form-group">
                   <label>Facebook Link</label>
                   <input type="url"  placeholder="Heading" class="form-control" name="facebook" required value="{{@$content['facebook']}}">
                 </div>
                 
                 <div class="form-group">
                   <label>Twitter link</label>
                   <input  type="url" placeholder="Heading" class="form-control" name="twitter" required value="{{@$content['twitter']}}">
                 </div>
                 
                 <div class="form-group">
                   <label>Linkedin Link</label>
                   <input type="url"  placeholder="Heading" class="form-control" name="linkedin" required value="{{@$content['linkedin']}}">
                 </div>
                 
                 <div class="form-group">
                   <label>You Tube Link</label>
                   <input  type="url" placeholder="" class="form-control" name="youtube" required value="{{@$content['youtube']}}">
                 </div>
                 
                 <div class="form-group">
                   <label>Bitcoin Link 1</label>
                   <input type="url"  placeholder="" class="form-control" name="bitcoin" required value="{{@$content['bitcoin']}}" >
                 </div> 
                 <div class="form-group">
                   <label>Bitcoin Link 2</label>
                   <input type="url"  placeholder="" class="form-control" name="bitcointalk" required value="{{@$content['bitcointalk']}}" >
                 </div>
                 <div class="form-group">
                   <label>Reddit</label>
                   <input type="url"  placeholder="" class="form-control" name="reddit" required value="{{@$content['reddit']}}" >
                 </div> 
                  <div class="form-group">
                   <label>Telegram</label>
                   <input type="url"  placeholder="" class="form-control" name="telegram" required value="{{@$content['telegram']}}" >
                 </div>  
                    
                    <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <button type="button" class="btn btn-primary add_stage">Add Footer Link</button>
                    </div>
                    <div class="col-sm-12 col-md-12 append_div">
                    <?php $links=json_decode($content['links'],true); //print_r($links); die;
                    ?>
                    @if(!empty($links))
	                    @foreach($links as $key=>$link)
	                        <div class="div_cont">
	                            <div class="form-group ">                               
	                                <input type="text" class="form-control cutm" value="{{$link['title']}}" name="link[{{$key}}][title]" required placeholder="Title">
	                                <input type="text" class="form-control cutm" value="{{$link['link']}}" name="link[{{$key}}][link]" required placeholder="Link">
                                  <input type="text" class="form-control cutm" value="{{$link['order']}}" name="link[{{$key}}][order]" required placeholder="Date">
	                            </div>
	                            <i class="fa fa-remove remove btn btn-danger" aria-hidden="true"></i>                           
	                        </div>
	                    @endforeach
                    @endif

                       
                    </div>
                    
                    </div>



                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->
            </div><!-- .tab-pane -->


            

					</div><!-- .tab-content -->
				</div><!-- #profile-components -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
@section('script')
<script>


</script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{asset('public/js/bootstrap-datetimepicker.js') }}"></script>
<script>
$('#homepage').validate();
var counter=41;
$('.add_stage').click(function(){

$('.append_div').append('<div class="div_cont"><div class="form-group "><input type="text" class="form-control cutm" value="" name="link['+counter+'][title]"required placeholder="Title"><input type="text" class="form-control cutm" value="" name="link['+counter+'][link]" required placeholder="Link"> <input type="text" class="form-control cutm" value="" name="link['+counter+'][order]" required placeholder="Order"></div><i class="fa fa-remove remove btn btn-danger" aria-hidden="true"></i></div>')
counter++;
});
$(document).on('click','.remove',function(){
    $(this).parent().remove();
});

</script>
 <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script type="text/javascript">
    //$(document).ready(function(){
      tinymce.init({
        selector: 'textarea',
        theme: "modern",
  skin: "lightgray",
  
  /* width and height of the editor */
  width: "100%",
  
  /* display statusbar */
  statubar: true,
  
  /* plugin */
  plugins: [
    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "save table contextmenu directionality emoticons template paste textcolor"
  ],

  /* toolbar */
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons",
  
  /* style */
  style_formats: [
    {title: "Headers", items: [
      {title: "Header 1", format: "h1"},
      {title: "Header 2", format: "h2"},
      {title: "Header 3", format: "h3"},
      {title: "Header 4", format: "h4"},
      {title: "Header 5", format: "h5"},
      {title: "Header 6", format: "h6"}
    ]},
    {title: "Inline", items: [
      {title: "Bold", icon: "bold", format: "bold"},
      {title: "Italic", icon: "italic", format: "italic"},
      {title: "Underline", icon: "underline", format: "underline"},
      {title: "Strikethrough", icon: "strikethrough", format: "strikethrough"},
      {title: "Superscript", icon: "superscript", format: "superscript"},
      {title: "Subscript", icon: "subscript", format: "subscript"},
      {title: "Code", icon: "code", format: "code"}
    ]},
    {title: "Blocks", items: [
      {title: "Paragraph", format: "p"},
      {title: "Blockquote", format: "blockquote"},
      {title: "Div", format: "div"},
      {title: "Pre", format: "pre"}
    ]},
    {title: "Alignment", items: [
      {title: "Left", icon: "alignleft", format: "alignleft"},
      {title: "Center", icon: "aligncenter", format: "aligncenter"},
      {title: "Right", icon: "alignright", format: "alignright"},
      {title: "Justify", icon: "alignjustify", format: "alignjustify"}
    ]}
  ]
        
    });
   
    </script>
@endsection