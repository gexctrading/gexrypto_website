@extends('layouts.backend_layout')
@section('title')
     Header Menus | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.custm {
  width: 40%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
.form-control.cutm {
  float: left;
  margin-right: 10px;
  width: 175px;
}
.btn.btn-primary.btn-md {
  margin-top: 40px;
}
.add_stg {
  width: 100%;
  float: :left;
}
.div_cont_sub {
  float: left;
  width: 100%;
  padding: 1px 5px;
}
.fa.fa-remove.remove.btn.btn-danger.submen {
  margin-top: -33px;
}
</style>

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
  <section class="app-content">
    <div class="row">
      <div class="col-md-10">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
        <div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
          <!-- tabs list -->
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-stream1" aria-controls="stream" role="tab" data-toggle="tab">Header Menus</a></li>

          </ul><!-- .nav-tabs -->

          <!-- Tab panes -->

      <div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade" id="profile-stream1">
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="homepage" action="{{ url('/administrator/header_menu') }}">

                  
                <div class="row">
                <div class="col-sm-12 col-md-12 add_stg">
                        <button type="button" class="btn btn-primary add_stage">Add New Menu</button>
                    </div>
                    <div class="col-sm-12 col-md-12 append_div">
                       <?php $menus=json_decode($content['description'],true); //print_r($stages); die;
                       ?>
                       @if(!empty($menus))
                           @foreach($menus as $key=>$menu)
                            <div class="div_cont">
                                <div class="form-group ">                               
                                    <input type="text" class="form-control cutm" value="{{$menu['title']}}" name="menu[{{$key}}][title]" required placeholder="Title">
                                    <input type="url" class="form-control cutm " value="{{$menu['link']}}" name="menu[{{$key}}][link]" required placeholder="Link">
                                    <input type="number" class="form-control cutm " value="{{$menu['order']}}" name="menu[{{$key}}][order]" required placeholder="Order" min="1">
                                </div>
                                <i class="fa fa-remove remove btn btn-danger" aria-hidden="true"></i>
                                <i class="fa fa-plus add_submenu btn btn-danger" aria-hidden="true" id="append_sub_div_{{$key}}" rel="{{$key}}"> Sub menu</i> 
                                 <div class="append_sub_div_{{$key}}" style="margin-left:5%; width:100%;float:left;padding:2% 2%;">
                                 @if(!empty($menu['submenu']))
                                    @foreach($menu['submenu'] as $subs=>$mnu):
                                        <div class="div_cont_sub">
                                            <div class="form-group">                               
                                              <input type="text" class="form-control cutm" value="{{$mnu['title']}}" name="mnu[{{$key}}][submenu][{{$subs}}][title]" required placeholder="Title">
                                              <input type="url" class="form-control cutm " value="{{$mnu['link']}}" name="mnu[{{$key}}][submenu][{{$subs}}][link]" required placeholder="Link">
                                              <input type="number" class="form-control cutm " value="{{$mnu['order']}}" name="mnu[{{$key}}][submenu][{{$subs}}][order]" required placeholder="Order" min="1">
                                            </div>
                                            <i class="fa fa-remove remove btn btn-danger submen" aria-hidden="true"></i>
                                        </div>
                                    @endforeach
                                @endif
                                </div>                        
                            </div>
                            @endforeach
                        @endif                       
                    </div>
                    
                </div>



                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->
            </div><!-- .tab-pane -->


            

          </div><!-- .tab-content -->
        </div><!-- #profile-components -->
      </div><!-- END column -->

    </div><!-- .row -->
  </section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
@section('script')
<script>


</script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>

<script>
$('#homepage').validate();
var counter=41;
var subcounter=141;
$('.add_stage').click(function(){

$('.append_div').append('<div class="div_cont"><div class="form-group "><input type="text" class="form-control cutm" value="" name="menu['+counter+'][title]"required placeholder="Title"><input type="url" class="form-control cutm" value="" name="menu['+counter+'][link]" required placeholder="Link"><input type="number" class="form-control cutm " name="menu['+counter+'][order]" required placeholder="Order" min="1"></div><i class="fa fa-remove remove btn btn-danger " aria-hidden="true"></i></div>')
counter++;
});
$(document).on('click','.remove',function(){
    $(this).parent().remove();
});
$(document).on('click','.add_submenu',function(){
  var id = $(this).attr('id');
  var ky = $(this).attr('rel');
    $('.'+id).append('<div class="div_cont_sub"><div class="form-group "><input type="text" class="form-control cutm" value="" name="menu['+ky+'][submenu]['+subcounter+'][title]"required placeholder="Title"><input type="url" class="form-control cutm" value="" name="menu['+ky+'][submenu]['+subcounter+'][link]" required placeholder="Link"><input type="number" class="form-control cutm " name="menu['+ky+'][submenu]['+subcounter+'][order]" required placeholder="Order" min="1"></div><i class="fa fa-remove remove btn btn-danger submen" aria-hidden="true"></i></div>')
    subcounter++;
});
</script>

@endsection