	  @extends('layouts.backend_layout')
@section('title')
     CMS Roadmap Page | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.custm {
  width: 40%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
.form-control.cutm {
  float: left;
  margin-right: 10px;
  width: 175px;
}
.btn.btn-primary.btn-md {
  margin-top: 40px;
}
</style>
<style type="text/css" href="{{asset('public/css/bootstrap.min.css')}}"></style>
<style type="text/css" href="{{asset('public/css/bootstrap-datetimepicker.css')}}"></style>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				<div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-stream1" aria-controls="stream" role="tab" data-toggle="tab">Roadmap Page</a></li>

					</ul><!-- .nav-tabs -->

					<!-- Tab panes -->

					<div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade" id="profile-stream1">
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="homepage" action="{{ url('/administrator/roadmap_page') }}">

                   <div class="form-group">
                      <label>Title</label>
                      <textarea   placeholder="Title" class="form-control" rows="1" name="title" required>{{$content['title']}}</textarea>
                    </div>

                    <div class="form-group">
                      <label>Heading</label>
                      <textarea  placeholder="Heading" class="form-control" name="heading" required>{{$content['heading']}}</textarea>
                    </div>
                    <div class="form-group ">
                        <label>Start Text</label>
                        <input type="text" class="form-control " value="{{$content['start_text']}}" name="start_text" required>
                    </div>
                    <div class="form-group ">
                        <label>End Text</label>
                        <input type="text" class="form-control cust" value="{{$content['end_text']}}" name="end_text" required>
                    </div>
                    <div class="row">
                    <div class="col-sm-8 col-md-8 append_div">
                    <?php $stages=json_decode($content['stage'],true); //print_r($stages); die;
                    ?>
                    @if(!empty($stages))
	                    @foreach($stages as $key=>$stage)
	                        <div class="div_cont">
	                            <div class="form-group ">                               
	                                <input type="text" class="form-control cutm" value="{{$stage['title']}}" name="stage[{{$key}}][title]" required placeholder="Title">
	                                <input type="text" class="form-control cutm datetimepicker" value="{{$stage['date']}}" name="stage[{{$key}}][date]" required placeholder="Date">
	                            </div>
	                            <i class="fa fa-remove remove btn btn-danger" aria-hidden="true"></i>                           
	                        </div>
	                    @endforeach
                    @endif

                       
                    </div>
                    <div class="col-sm-4 col-md-4">
                        <button type="button" class="btn btn-primary add_stage">Add Stage</button>
                    </div>
                    </div>



                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->
            </div><!-- .tab-pane -->


            

					</div><!-- .tab-content -->
				</div><!-- #profile-components -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
@section('script')
<script>


</script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>
<script type="text/javascript" src="{{asset('public/js/bootstrap-datetimepicker.js') }}"></script>
<script>
$('#homepage').validate();
var counter=41;
$('.add_stage').click(function(){

$('.append_div').append('<div class="div_cont"><div class="form-group "><input type="text" class="form-control cutm" value="" name="stage['+counter+'][title]"required placeholder="Title"><input type="text" class="form-control cutm" value="" name="stage['+counter+'][date]" required placeholder="Date"></div><i class="fa fa-remove remove btn btn-danger" aria-hidden="true"></i></div>')
counter++;
});
$(document).on('click','.remove',function(){
    $(this).parent().remove();
});
/*$(function () {
            $('.datetimepicker').datetimepicker({
                viewMode: 'years',
                format: 'MM/YYYY'
            });
        });*/
</script>

@endsection