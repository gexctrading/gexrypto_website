@extends('layouts.backend_layout')
@section('title')
     Send New Newsletter  | Admin Panel
@stop
@section('content')
<style type="text/css">
 .form-group.cust {
  width: 30%;
  float: left;
  margin-right: 22px;
}
label.error {
  color: red;
  font-size: 12px;
}
.form-group > img {
  margin-top: 15px;
  text-align: center;
  width: 250px;
}
</style>
<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				<div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#profile-stream1" aria-controls="stream" role="tab" data-toggle="tab"> Send New Newsletter </a></li>

					</ul><!-- .nav-tabs -->

					<!-- Tab panes -->

					<div class="tab-content">

            <div role="tabpanel" class="tab-pane in active fade" id="profile-stream1">
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="faqpage" action="{{ url('/administrator/send/newsletter') }}" enctype="multipart/form-data">
                  <input type="hidden" value="@if(isset($data)) {{$data['id']}} @endif" name="id">
                   <div class="form-group">
                      <label>Title</label>
                      <input type="text"   placeholder="Title" class="form-control title" name="title" required value="@if(isset($data)) {{$data['title']}} @endif">
                    </div>
                     <div class="form-group">
                      <label>Description</label>
                      <textarea type="text"   placeholder="Description" class="form-control descp" name="description" required >@if(isset($data)) {{$data['description']}} @endif</textarea>
                    </div>
                   
                   

                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md send">Send To Subscribers</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->
            </div><!-- .tab-pane -->


            

					</div><!-- .tab-content -->
				</div><!-- #profile-components -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->




@endsection
@section('script')
<script>


</script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>
<script>
$('#faqpage').validate();
function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah')
                        .attr('src', e.target.result)
                        .width(150)
                        .height(200);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

/*for web push notification */

function showNotification(title,content) {
  try {
     var options = {
      body: content
      //icon: theIcon
  }
    var notification = new Notification(title,options);
  } catch (err) {
    alert('Notification API not available: ' + err);
  }
}

function notifyMe(title,content) {
  if (!("Notification" in window)) {
    alert('Notification API not supported.');
    return;
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    showNotification(title,content);
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        showNotification(title,content);
      }
    });
  }
}

$('.send').click(function(){
  if($('.title').val()!='' || $('.descp').val()!=''){
//notifyMe($('.title').val(),$('.descp').val());
}
});

</script>

@endsection