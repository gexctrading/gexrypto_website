@extends('layouts.backend_layout')
@section('title')
     Profile | Admin Panel
@stop
@section('content')
<style>
label.error{
    color:red;
    font-size:12px;
}
</style>

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">

<div class="wrap">
	<section class="app-content">
		<div class="row">
			<div class="col-md-8">
       @if (session('custom_success'))
         <div class="alert alert-success" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Success ! </strong>
          <span>{{ Session::get('custom_success') }}</span>
        </div>
        @endif
        @if (session('custom_error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
          <strong>Error ! </strong>
          <span>{{ Session::get('custom_error') }}</span>
        </div>
        @endif
				<div id="profile-tabs" class="nav-tabs-horizontal white m-b-lg">
					<!-- tabs list -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#profile-stream" aria-controls="stream" role="tab" data-toggle="tab">Basic Detail</a></li>
						<li role="presentation"><a href="#profile-photos" aria-controls="photos" role="tab" data-toggle="tab">Profile Image</a></li>
						<li role="presentation"><a href="#profile-friends" aria-controls="friends" role="tab" data-toggle="tab">Change Password</a></li>
					</ul><!-- .nav-tabs -->

					<!-- Tab panes -->

					<div class="tab-content">
						<div role="tabpanel" class="tab-pane in active fade" id="profile-stream">
					
              <div class="widget">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="detailForm" action="{{ url('/administrator/profile_information') }}">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Name</label>
                      <input class="numberNotAllowed form-control" value="{{ Auth::user()->name }}" name="name" placeholder="Name" type="text">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Email</label>
                      <input class="form-control" name="email" value="{{ Auth::user()->email }}" placeholder="Email" type="text">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Phone No.</label>
                      <input class="isNumber form-control" value="{{ Auth::user()->phone }}" name="phone" placeholder="Phone No." type="text">
                    </div>
                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
              </div><!-- .widget -->

							
						</div><!-- .tab-pane -->

						<div role="tabpanel" id="profile-photos" class="tab-pane fade p-md">
							<header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" enctype="multipart/form-data" id="imageForm"  action="{{ url('/administrator/admin_image') }}">
                  @if(Auth::user()->image)
                    <div class="avatar avatar-xl avatar-circle">
                      <a href="javascript:void(0)">
                      <img  class="img-responsive" src="/public/admin/{{ Auth::user()->image }}" alt="avatar">
                      </a>
                    </div>
                  @endif
                    <div class="form-group">
                      <label for="exampleInputPassword1">Image</label>
                      <input type="file" id="ff2" name="image">
                    </div>
                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
						</div><!-- .tab-pane -->

			<div role="tabpanel" class="tab-pane fade p-md" id="profile-friends">
                <header class="widget-header">
                </header><!-- .widget-header -->
                <hr class="widget-separator">
                <div class="widget-body">
                  <form method="Post" id="passwordForm" action="{{ url('/administrator/change_pass') }}">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Old Password</label>
                      <input class="form-control"  name="old_password" placeholder="Old Password" type="password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">New Password</label>
                      <input class="form-control" name="password" id="password"  placeholder="New Password" type="password">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Confirm Password</label>
                      <input class="form-control"  name="password_confirmation" placeholder="Confirm Password" type="password">
                    </div>
                    {{ csrf_field()}}
                    <button type="submit" class="btn btn-primary btn-md">Submit</button>
                  </form>
                </div><!-- .widget-body -->
						</div><!-- .tab-pane -->
					</div><!-- .tab-content -->
				</div><!-- #profile-components -->
			</div><!-- END column -->

		</div><!-- .row -->
	</section><!-- #dash-content -->
</div><!-- .row -->

<!-- Likes/comments Modal -->

  <!-- APP FOOTER -->
  <!-- /#app-footer -->
</main>
<!--========== END app main -->
@endsection
@section('script')
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>

<script type="text/javascript">
  $(function() {
   
   $("#detailForm").validate({
    
        ignore: ".ignore",
        rules: {
            name: "required",
            phone: "required",
            address: "required",
            email: {
                required: true,
                email: true
            },
        },


        submitHandler: function(form) {
            form.submit();
        }
    });

    $("#imageForm").validate({
    
        ignore: ".ignore",
        rules: {
            image: "required"
        },


        submitHandler: function(form) {
            form.submit();
        }
    });



    $("#passwordForm").validate({

      ignore: ".ignore",
      rules: {
       old_password: {
         required: true,
         "remote":
         {
           url: "{{ url('/administrator/checkAdminPassword') }}",
           type: "post",
           data:
           {
             _token: function()
             {
               return $('#passwordForm :input[name="_token"]').val();
             }
           }
         }
       },
       password: "required",
       password_confirmation: {
        required: true,
        minlength: 5,
        equalTo: "#password"
      },
    },

    submitHandler: function(form) {
      form.submit();
    }

  });


   $('.isNumber').keypress(function (event) {
    return isNumber(event, this)
  });

    function isNumber(evt, element)
    {
     var charCode = (evt.which) ? evt.which : event.keyCode
     if (charCode > 31 && (charCode < 48 || charCode > 57))
      return false;
    return true;
  }



  });


$('.numberNotAllowed').keyup(function()
  {
    var yourInput = $(this).val();
    re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
    var isSplChar = re.test(yourInput);
    if(isSplChar)
    {
      var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
      $(this).val(no_spl_char);
    }
  });

$(document).ready(function(){

  $('#ff2').change(
  function () {
    var fileExtension = ['jpeg', 'jpg', 'png'];
    if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only '.jpeg','.jpg','.png' formats are allowed.");
            $(this).val("");
           return false; 
    }
    });

});
</script>
@endsection