@extends('frontend.layouts.pages')

@section('title', '404')

@section('content')

<style>
	.error_page{
		float: left;
		width: 100%;
		padding: 60px 15px;
	}
	.error_page span{
		font-size: 100px;
		color: #aaaaaa;
		font-style: italic;
		padding: 20px 0;
		font-weight: 800;
		display: block;
	}
	.error_page h1 {
    	color: #238396;
	}
	.error_page p {
    	color: #999;
    	font-size: 20px;
	}
</style>

<div class="container-fluid text-center error_page">
	<span>404!</span>
	<img src="{{asset('public/frontend/images/404-error.png')}}">
	<h1>Page not found</h1>
	<p>Sorry, we can't find the page you are looking for<br> Click <a href="/">here </a>to go back to home page</p>
</div>

@stop