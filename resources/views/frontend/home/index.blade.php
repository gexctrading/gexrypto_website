@extends('frontend.layouts.index')

@section('title', 'Home')

@section('content')
<style>
.err_div, .suc_msg, .err_msg{
	display: none;
}
#overlay{
	position: fixed;
	z-index: 99999;
	top: 0;
	left: 0;
	bottom: 0;
	right: 0;
	text-align: center;
	display: none;
	background: rgba(0,0,0,0.8);
	transition: 1s 0.4s;
	-webkit-transition: 1s 0.4s;
	-moz-transition: 1s 0.4s;
	-ms-transition: 1s 0.4s;
}
.load_img{
	position: absolute;
	top: 50%;
	transform: translate(-50%, -50%);
	left: 50%;
	width: 6%;
	z-index: 999;
}
.alert.alert-success.suc_msg {
	width: 50%;
}
.alert.alert-danger.err_msg {
	width: 50%;
}
.fa.fa-calendar {
	color: #fecd06;
}
.whiteppr_ryt{
	padding: 0px 0px;
}
.counterstart span.sale_type{
	font-size: 30px;
	width: unset;
	margin: unset;
	font-weight: normal;
}
.screen_banner{
	background: #1590bc;
	background: -moz-linear-gradient(top, #1590bc 0%, #1686ae 25%, #187597 50%, #1b6a89 75%, #1d5e7a 100%);
	background: -webkit-linear-gradient(top, #1590bc 0%,#1686ae 25%,#187597 50%,#1b6a89 75%,#1d5e7a 100%);
	background: linear-gradient(to bottom, #1590bc 0%,#1686ae 25%,#187597 50%,#1b6a89 75%,#1d5e7a 100%);
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#1590bc', endColorstr='#1d5e7a',GradientType=0 );
	padding: 60px 15px;
}
.screen_banner ul{
	width: 100%;
	text-align: center;
	display: block;
	list-style: none;
	padding-left: 0;
	margin-top: 60px;
}
.screen_banner ul li{
	display: inline-block;
	padding: 0 80px;
}
.screen_banner ul li a{
	color: #fff;
	font-size: 26px;
}
.modal.fade{
	margin-top: 100px;
}
.payment_banner{
	padding:0;
}
.payment_banner img{
	width: 100%;
}

</style>

<div id="overlay">
 	<img class="load_img" src="{{asset('public/frontend/images/loading.gif')}}" >
</div>

<!-- Banner -->
<div class="banner">
	<div class="container">
		<!-- <div class="pulse pulse1"></div>
		<div class="pulse pulse2"></div>
		<div class="pulse pulse3"></div>
		<div class="pulse pulse4"></div> -->
		<div class="banner_text">
			<h2>{{$data['title']}}</h2>			
			<p>{{$data['heading']}}</p>
			<h3 style="color:#F7C807;">
			  <a href="" class="typewrite" data-period="2000" style="color:#F7C807;">
			  	<!-- data-type='[ "Avail 30% extra coin before Private Pre-Sale Ends"]' -->
			  	<!-- data-type='[ "Earn 20% extra Gex coins before Private Pre-Sale Ends","Earn 10% extra Gex coins on Lottery on daily basis up to 25th November, 2017" ]' -->
			    <span class="wrap" style="color:#F7C807;"></span>
			  </a>
			</h3>
		</div>

		<div class="counterstart banner_counter">
			<!-- <span class="purchase">
				<img src="{{asset('public/frontend/images/purchase.png')}}">
			</span> -->
			<!-- <h2 class="counter_text"> HURRY!!! Private Pre Sale Ends in </h2> -->
			<h2 class="counter_text"> Public Token Sale Begins</h2>
			<!-- <span class="sale_type"></span>			 -->
			<div id="countdown">
				<span class="days"> <label></label> <b>Days</b></span>
				<span class="hours"><label></label> <b>Hours</b></span>
				<span class="minutes"><label></label> <b>Minutes</b></span>
				<span class="seconds"><label><b>Seconds</b></span>
			</div>	
		</div>
	</div>
		
	<span class="dot_img">
		<ul>
			<li class="dot_1">
				<span>
					<h3>{{$data['currencies']}}</h3>
					<p>Currencies</p>
				</span>
			</li>
			<li class="dot_2">
				<span>
					<h3>{{$data['assets']}}</h3>
					<p>Assets</p>
				</span>
			</li>
			<li class="dot_3">
				<span>
					<h3>{{$data['markets']}}</h3>
					<p>Markets</p>
				</span>
			</li>
		</ul>
		<img src="{{ asset('public/frontend/images/layer.png') }}">
	</span>
</div>

<div class="container-fluid about_crypto">
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12 left_about">
				<h3>{{$data['dlt_heading']}}</h3>
				<p>{{$data['dlt_description']}}</p>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 ryt_about">
				<div class="year">
					<span class="scale1"></span>
					<span class="scale2"></span>
					<h1>{{$data['exp_year']}}</h1>
				</div>
				<div class="year_cont">
					<h4>Years of Experience</h4>
					<p>{{$data['exp_description']}}</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="video_sec">
	<div class="container-sm container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 first_video">
				<!-- <iframe width="100%" height="350" src="https://www.youtube.com/embed/1bqxzErR0CI" frameborder="0" gesture="media" allowfullscreen></iframe> -->
				<iframe width="100%" src="https://www.youtube.com/embed/1bqxzErR0CI" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 video_rytsec">
				<h3>Let’s see how GexCrypto makes your investment better</h3>
				<ul>
					<li>Secure Environment </li>
					<li>Live Monitoring</li>
					<li>24*7 Live Support</li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12 video_rytsec">
				<p>GexCrypto's mission is to deliver the highest level of service possible to users and to demonstrate technological leadership that will allow Gex to become the leading Cryptocurrency trading platform. We at Gex believe in our prospective success and know what path needs to be taken to become the leading global trading platform.</p>
			</div>
			<div class="col-md-6 col-sm-6 col-xs-12 second_video">
				<!-- <iframe width="100%" height="350" src="https://www.youtube.com/embed/FcTz0O3YIF8" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe> -->
				<iframe width="100%" src="https://www.youtube.com/embed/FcTz0O3YIF8" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
			</div>
		</div>
	</div>
</div>

<div class="token-sale">
	<img src="{{ asset('public/frontend/images/Token sale-11.jpg') }}">
</div>

<div class="container-fluid payment_banner">
	<img src="{{ asset('public/frontend/images/payment_banner.png') }}">
</div>

<div class="container-fluid why_global">
	<div class="container">
		<h2>Why Global Exchange Platform?</h2>
		<p>{{$data['global_exchange']}}</p>
		<img src="{{ asset('public/frontend/images/graph.jpg') }}" class="img-responsive center-block" alt="Graph">
	</div>
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="team_point">
					<img src="{{ asset('public/frontend/images/team.png') }}" class="img-responsive" alt="Team">
					<h4>Team Approach</h4>
					<p>{{ $data['team_approach'] }}</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="team_point center_point">
					<img src="{{ asset('public/frontend/images/delivery.png') }}" class="img-responsive" alt="Delivery">
					<h4>Honest Delivery</h4>
					<p>{{$data['honest_del']}}</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="team_point">
					<img src="{{ asset('public/frontend/images/active.png') }}" class="img-responsive" alt="Active">
					<h4>Active Management</h4>
					<p>{{$data['active_management']}}</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid res-outr">
	<div class="sec-main-hdng">
		<h2>Under "Global Exchange's Superior Resources"</h2>
		<span>{!! $data['global_exh_resource'] !!}</span>
	</div>
	<div class="row second-row">
		<div class="col-md-6 col-sm-12 col-xs-12 single-res">
			<h4>Multi-Language Customer Support </h4>
			<p>{{$data['multi_lang_support']}}</p>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 single-res blue-box">
			<h4>Technical Team</h4>
			<p>{{$data['technical_team']}}</p>
		</div>
	</div>
</div>

<div class="graphics">
	<img src="{{ asset('public/frontend/images/11.jpg') }}" class="" alt="Graphic Charts">
</div>


<!-- <div class="home_counter_sec">
	<div class="container">
		<div class="counterstart">					
			<div id="countdown">
				<span class="days"> <label>0</label> <b>Days</b></span> 
				<span class="hours"><label>12</label> <b>Hours</b></span>
				<span class="minutes"><label>10</label> <b>Minutes</b></span> 
				<span class="seconds"><label>19<b>Seconds</b></span>
			</div>	
		</div>
	</div>
</div> -->

<div class="roadmap homeroadmap">
	<div class="container-fluid">
		<div class="row">
			<div class="projectroadmap">
				<h3> {{@$roadmap['title']}}</h3>
				<p>  {{@$roadmap['heading']}}</p>
				<div class="roadmapstartend">
					<span class="start"> {{@$roadmap['start_text']}}</span>
					<ul>
					<?php $stages=json_decode($roadmap['stage'],true); $i=1;?>
					@foreach($stages as $key=>$stage)
						<li class="@if($i%2==0) {{'scnd_lim'}} @else {{'ist_lim'}} @endif">
							<div class="contentroadmap contntrd{{$i}}">
								<span>{{$stage['title']}}</span>
								<label>{{$stage['date']}}</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
					<?php $i++; ?>
					@endforeach
					</ul>
					<span class="end">{{$roadmap['end_text']}}</span>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="storage_files container-fluid">
	<div class="container container-sm">
		<div class="row">
			<h3>A great amount of storage sits unused in data centers and hard drives around the world.</h3>
			<ul>
				<li>
					<span>
						<img src="{{ asset('public/frontend/images/file.jpg') }}" class="img-responsive center-block" alt="Files">
					</span>
					<h4>Earn Gexc for hosting files</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
				</li>
				<li>
					<span>
						<img src="{{ asset('public/frontend/images/file.jpg') }}" class="img-responsive center-block" alt="Files">
					</span>
					<h4>Exchange Gexc for USD, BTC, ETH and more</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
				</li>
				<li>
					<span>
						<img src="{{ asset('public/frontend/images/file.jpg') }}" class="img-responsive center-block" alt="Files">
					</span>
					<h4>Reliably store files at hyper­competitive prices</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
				</li>
			</ul>
		</div>
	</div>
</div> -->

<div class="ques_sec">
	<img src="{{ asset('public/frontend/images/world_map.jpg') }}" alt="World_Map">
	<div class="container container-sm">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<!-- <h3>Decentralised trust and reputation</h3>
		<p>Commerce and trust need each other, but current systems are complicated. We use smart contracts to simplify the relationship.</p>
		<div class="row">
			<div class="ques_left">
				<img src="{{ asset('public/frontend/images/map.png') }}" class="img-responsive" alt="Map">
			</div>
			<div class="ques_ryt">
				<ul>
					<li>
						<h4>What does it do?</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
					</li>
					<li>
						<h4>How does it work?</h4>
						<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
					</li>
					<li>
						<h4>Creating a decentralised commerce of trust</h4>
						<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
					</li>
				</ul>
			</div>
		</div> -->
	</div>
</div>

<div class="home-whitepaper">
	<div class="whiteppr_left">
		<h3>Whitepaper</h3>
		<p>{{$data['whitepaper']}}</p>
		<a href="{{ asset('public/frontend/files/WhitePaper-GexC-Trading-Platform.pdf') }}" target="_blank" class="btn">Read More</a>
	</div>
	<div class="whiteppr_ryt">
	<img src="{{ asset('public/frontend/images/coinnew.jpg') }}" class="img-responsive center-block" alt="Whitepaper">
	</div>
</div>
<!-- graphics -->
<div class="team-outr">
	<div class="container">
		<h1 class="sec-main-blue-heading">{{$data['about_team']}}</h1>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				@foreach($members as $member)
					<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 outr_mmb">
					<div class="team-member">
						<a href="{{url('/team')}}" class="partner-img-cont">
							<span>
								<img src="{{ asset('public/admin/team/'.$member['image']) }}">
							</span>
						</a>
						 <div class="member-info">
						 	<h1>{{$member['name']}}</h1>
						 	<span>{{$member['designation']}}</span>
						 	<div class="linkedin">
						 		<a href="{{$member['linkedin']}}" target="_blank">
						 			<span class="fa fa-linkedin" aria-hidden="true"></span>
						 		</a>
						 	</div>
						 </div>
					</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>

<div class="partner-outr container-fluid">
	<div class="container container-sm">
		<div class="row">
			<div class="alert alert-success suc_msg" role="alert" >
	                		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                			<span aria-hidden="true">×</span>
	                		</button>
	                		<span>Subscribed successfully!</span>
	            	</div>
		            <div class="alert alert-danger err_msg" role="alert" >
		                	<button type="button" class="close" data-dismiss="alert" aria-label="Close">
		                		<span aria-hidden="true">×</span>
		                	</button>
		                	<span>Please enter valid email!</span>
		            </div>
			<div class="col-lg-6 col-md-8 col-sm-10 col-xs-12">
				<h2>Newsletter</h2>
				<span>Subscribe to our newsletter for daily updates</span>
				<form id="subscribeForm">
					{{ csrf_field() }}
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
						<input type="text" class="form-control sname" placeholder="Enter your name" name="name" required>
					</div>
					<div class="form-group col-md-5 col-sm-4 col-xs-12">
						<input type="email" class="form-control semail" placeholder="Enter your email address" name="email" required>
					</div>
					<div class="form-group col-md-3 col-sm-4 col-xs-12">
						<button type="button" class="btn btn-primary subscribeFormsub">Subscribe</button>
					</div>
					<div class="form-group col-md-12 col-sm-12 col-xs-12 err_div">
						<span class="error">Please fill all fields.</span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="screen_banner">
	<img src="{{asset('public/frontend/images/screen_banner.png')}}" class="img-responsive center-block">
	<ul>
		<li>
			<a href="#Modal1" data-toggle="modal">Mobile</a>
		</li>
		<li>
			<a href="#Modal2" data-toggle="modal">Desktop</a>
		</li>
		<li>
			<a href="#Modal3" data-toggle="modal">Tablet</a>
		</li>
	</ul>
</div>

<div class="news-outr">
	<div class="container-fluid">
		
		<div class="news-left-cnt col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="blog_tabs">
				<ul class="nav nav-tabs">
					<li class="active">
						<a href="#Gexcrypto" data-toggle="tab">Gexcrypto Blog</a>
					</li>
					<li>
						<a href="#latest" data-toggle="tab">Latest Blog</a>
					</li>
				</ul>
			</div>
			<!-- <h1 class="col-md-10 col-sm-10 col-xs-12 sec-main-blue-heading">LATEST FROM OUR BLOG</h1> -->
			<!-- <a href="#" class="">View all</a> -->
			<p>Putting up with the finest of Pieces, Knowledge Resources, News Archives, and Hand-picked Stories regarding Bitcoin, Cryptocurrency, Blockchain and other tech world to keep you ahead of the technology curve. Making sure you get the latest of industry insights and forum updates from our latest blogs</p>
		</div>
		<div class="news-right-cnt col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="tab-content">
				<div class="tab-pane active in" id="Gexcrypto">
					<div class="carousel slide" id="myCarousel1">
						<div class="carousel-inner">
							@if(isset($crypto))
							@foreach($crypto as $ky=>$post)
							<div class="item @if($ky==0) {{'active'}} @endif">
								<div class="col-md-6 col-sm-6 col-xs-12 test_item">
									<div class="fff">
										<div class="thumbnail">
											<a href="#"><img src="{{ asset('public/admin/blog/'.$post['image']) }}" alt=""></a>
											<div class="thumb-overlay">
												&nbsp;
											</div>
										</div>
										<div class="caption">
											<h4>{{$post['title']}} </h4>
											<h5><i class="fa fa-calendar" aria-hidden="true"></i> <font style="font-size: 13px; font-weight: normal;"><?php $date=date_create($post['posted_date']); echo $date=date_format($date,'j F, Y') ?></font></h5>
											<h6>By {{$post['author']}}</h6>
											@if(strlen($post['description'])>115)
											<p>{!! substr($post['description'],0,115) !!}.. </p>
											@else
											<p>{!! $post['description'] !!} </p>
											@endif
											<a href="{{url('blog-detail/'.base64_encode(convert_uuencode($post['id'])))}}" class="read_more" style="color:white;">Read More</a>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							@endif
						</div>
					</div><!-- /#myCarousel -->
					<nav class="carousel_navs">
						<ul class="control-box pager">
							<li><a data-slide="prev" href="#myCarousel1" class="prev"><i class="glyphicon glyphicon-chevron-left"></i></a></li>
							<li><a data-slide="next" href="#myCarousel1" class="next"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
						</ul>
					</nav>
				</div>
				<div class="tab-pane" id="latest">
					<div class="carousel slide" id="myCarousel2">
						<div class="carousel-inner">
							@if(isset($lat_posts))
							@foreach($lat_posts as $ky=>$post)
							<div class="item @if($ky==0) {{'active'}} @endif">
								<div class="col-md-6 col-sm-6 col-xs-12 test_item">
									<div class="fff">
										<div class="thumbnail">
											<a href="#"><img src="{{ asset('public/admin/blog/'.$post['image']) }}" alt=""></a>
											<div class="thumb-overlay">
												&nbsp;
											</div>
										</div>
										<div class="caption">
											<h4>{{$post['title']}} </h4>
											<h5><i class="fa fa-calendar" aria-hidden="true"></i> <font style="font-size: 13px; font-weight: normal;"><?php $date=date_create($post['posted_date']); echo $date=date_format($date,'j F, Y') ?></font></h5>
											<h6>By {{$post['author']}}</h6>
											<p>{!! substr($post['description'],0,110) !!}.. </p>
											<a href="{{url('blog-detail/'.base64_encode(convert_uuencode($post['id'])))}}" class="read_more" style="color:white;">Read More</a>
										</div>
									</div>
								</div>
							</div>
							@endforeach
							@endif
						</div>
					</div><!-- /#myCarousel -->
					<nav class="carousel_navs">
						<ul class="control-box pager">
							<li><a data-slide="prev" href="#myCarousel2" class="prev"><i class="glyphicon glyphicon-chevron-left"></i></a></li>
							<li><a data-slide="next" href="#myCarousel2" class="next"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
						</ul>
					</nav>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>


<!-- Modal-1 -->
<div class="modal fade" id="Modal1" tabindex="-1" role="dialog" aria-labelledby="Modal-1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal-1">Mobile</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="{{asset('public/frontend/images/mobile_img.jpg')}}" class="img-responsive center-block">
			</div>
		</div>
	</div>
</div>

<!-- Modal-2 -->
<div class="modal fade" id="Modal2" tabindex="-1" role="dialog" aria-labelledby="Modal-2" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal-2">Desktop</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="{{asset('public/frontend/images/desktop_img.png')}}" class="img-responsive center-block">
			</div>
		</div>
	</div>
</div>

<!-- Modal-3 -->
<div class="modal fade" id="Modal3" tabindex="-1" role="dialog" aria-labelledby="Modal-3" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="Modal-3">Tablet</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<img src="{{asset('public/frontend/images/tab_img.png')}}" class="img-responsive center-block">
			</div>
		</div>
	</div>
</div>

@stop

@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
<script src="{{ asset('public/js/moment.js') }}"></script>
<script src="https://momentjs.com/downloads/moment-timezone-with-data-2012-2022.js"></script>

<!-- Counter script -->
<script>
/*
	var target_date = new Date('Nov, 12, 2017 00:31:00').getTime();
	console.log(target_date);


	var days, hours, minutes, seconds;


	var countdown = document.getElementById('countdown');


	setInterval(function () {

	   
	    var current_date = new Date().getTime();
	    var seconds_left = (target_date - current_date) / 1000;


	    days = parseInt(seconds_left / 86400);
	    seconds_left = seconds_left % 86400;

	    hours = parseInt(seconds_left / 3600);
	    seconds_left = seconds_left % 3600;

	    minutes = parseInt(seconds_left / 60);
	    seconds = parseInt(seconds_left % 60);

	   
	    if(countdown){
	    countdown.innerHTML = '<span class="days">' + days +  ' <b>Days</b></span> <span class="hours">' + hours + ' <b>Hours</b></span> <span class="minutes">'
	    + minutes + ' <b>Minutes</b></span> <span class="seconds">' + seconds + ' <b>Seconds</b></span>'; 
	    } 

	}, 1000);*/
	//# sourceURL=pen.js
</script>

<!-- Form Send Email script -->
<script type="text/javascript">
	$('.submitBtn').click(function(){
		var email   = $.trim( $('#email').val() );
		var name    = $.trim( $('#name').val() );
		var message = $.trim( $('#message').val() );

		if( !email )
		{
			alert('Email is required');
		}
		else if( !message )
		{
			alert('Message cannot be empty.');
		}
		else
		{
			$.ajax({
				method : 'POST',
				data   : {
					_token:'{{ csrf_token() }}',
					email:email, name:name,
					message:message
				},
				url    : "{{ route('send_contact_us_mail') }}",
				success:function(resp){
					if( resp == 1 )
					{
						$('#email').val("");
						$('#name').val("");
						$('#message').val("");
						$('.sucess_msg').show();
					}
					else
					{
						$('.error_msg').show();
					}
				}
			});
		}
	});



	$('.subscribeFormsub').on('click',function() {
		if($('.sname').val()=='' || $('.semail').val()==''){
			$('.err_div').show();
			return false;
		}
			$('.err_div').hide();
                    $('#overlay').show();
                    $.ajax({

                        type: 'post',
                        url: '{{url('subscribe')}}',
                        data: $('#subscribeForm').serialize(),
                        success: function (data) {
                         if(data.status=='success'){
                            $('#overlay').hide();
                            $('.suc_msg').show();
                            $('.form-control').val('');
                            $('.form-control').text('');
                            $('.err_msg').hide();

                            }else{
                            	$('.err_msg').text(data.msg);
                                $('.err_msg').show();
                                $('#overlay').hide();

                            }
                        }
                 });
               // }
           // });
        });


    </script>
</script>

<!-- Slide animation in map image section -->
<script>
	$(window).scroll(function() {
		var width = $( window ).width();
		if(width > 1200){
			var scroll = $(window).scrollTop();
			var scroll_position = $(".ques_sec")

			if(scroll_position.length) {
				var slide = scroll_position.offset().top-700;
				if (scroll >= slide) {
					$(".ques_left").addClass('left_anim');
					$(".ques_ryt li").addClass('ryt_anim');
				}
				else{
					$(".ques_left").removeClass('left_anim');
					$(".ques_ryt li").removeClass('ryt_anim');
				}
			}
		}
	});

	// Resource section fade animation
	$(window).scroll(function() {
		var width = $( window ).width();
		var res_scroll = $(window).scrollTop();
		var res_height = $('.res-outr').outerHeight();
		var height = $(window).height();
		var res_animate = $(".res-outr");

		if(res_animate.length) {
			var anim_text = res_animate.offset().top;
			if (res_scroll > (anim_text-height)) {
				$(".res-outr").addClass('animate');
			}
			else{
				$(".res-outr").removeClass('animate');
			}
		}
	});

</script>

<!-- floating form script  -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.contact-form .form-control').on('focus blur', function (e) {
			$(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
		}).trigger('blur');
	});
</script>
<script>



 	//var target_date=moment.tz("12/10/2017 11:59 pm", "M/D/YYYY h:mm a", true, "America/Los_Angeles");
 	var target_date=moment.tz("12/25/2017 11:59 pm", "M/D/YYYY h:mm a", true, "America/Los_Angeles");
	// variables for time units
	var days, hours, minutes, seconds;

	// get tag element
	var countdown = document.getElementById('countdown');

	// update the tag with id "countdown" every 1 second
	setInterval(function () {

  

    var current_date= moment.tz('America/Los_Angeles').valueOf();
    //alert(moment.tz('America/Los_Angeles'));
    var seconds_left = (target_date - current_date) / 1000;

    // do some time calculations
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;

    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;

    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    if(parseInt(days)<=0 && parseInt(hours) <= 0 && parseInt(seconds) <=0){
    	target_date=moment.tz("12/25/2017 11:59 pm", "M/D/YYYY h:mm a", true, "America/Los_Angeles");
    	//$('.sale_type').text('Ends');
    	$('.counter_text').text('Public Token Sale Begins');
    	$('.purchase').hide();
    }


    // format countdown string + set tag value
    countdown.innerHTML = '<span class="days">' + days +  ' <b>Days</b></span> <span class="hours">' + hours + ' <b>Hours</b></span> <span class="minutes">'
    + minutes + ' <b>Minutes</b></span> <span class="seconds">' + seconds + ' <b>Seconds</b></span>';  

}, 1000);
//# sourceURL=pen.js
</script>
<!-- <script type="text/javascript">
	
var TxtType = function(el, toRotate, period) {
        this.toRotate = toRotate;
        this.el = el;
        this.loopNum = 0;
        this.period = parseInt(period, 10) || 2000;
        this.txt = '';
        this.tick();
        this.isDeleting = false;
    };

    TxtType.prototype.tick = function() {
        var i = this.loopNum % this.toRotate.length;
        var fullTxt = this.toRotate[i];

        if (this.isDeleting) {
        this.txt = fullTxt.substring(0, this.txt.length - 1);
        } else {
        this.txt = fullTxt.substring(0, this.txt.length + 1);
        }

        this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

        var that = this;
        var delta = 200 - Math.random() * 100;

        if (this.isDeleting) { delta /= 2; }

        if (!this.isDeleting && this.txt === fullTxt) {
        delta = this.period;
        this.isDeleting = true;
        } else if (this.isDeleting && this.txt === '') {
        this.isDeleting = false;
        this.loopNum++;
        delta = 500;
        }

        setTimeout(function() {
        that.tick();
        }, delta);
    };

    window.onload = function() {
        var elements = document.getElementsByClassName('typewrite');
        for (var i=0; i<elements.length; i++) {
            var toRotate = elements[i].getAttribute('data-type');
            var period = elements[i].getAttribute('data-period');
            if (toRotate) {
              new TxtType(elements[i], JSON.parse(toRotate), period);
            }
        }
        // INJECT CSS
        var css = document.createElement("style");
        css.type = "text/css";
        css.innerHTML = ".typewrite > .wrap { border-right: 0.08em solid #fff}";
        document.body.appendChild(css);
    };
</script> -->
@stop