@extends('frontend.layouts.index')

@section('title', 'Home')

@section('content')

<!-- Banner -->
<div class="banner">
	<div class="container">
		<div class="pulse pulse1"></div>
		<div class="pulse pulse2"></div>
		<div class="pulse pulse3"></div>
		<div class="pulse pulse4"></div>
		<div class="banner_text">
			<h2>GexCrypto: Your New Digital Trading Platform</h2>
			<p>A Cryptocurrency trading platform with improved &amp; advanced Blockchain Technology</p>
		</div>
	</div>
	<span class="dot_img">
		<ul>
			<li class="dot_1">
				<span>
					<h3>885</h3>
					<p>Currencies</p>
				</span>
			</li>
			<li class="dot_2">
				<span>
					<h3>125</h3>
					<p>Assets</p>
				</span>
			</li>
			<li class="dot_3">
				<span>
					<h3>7895</h3>
					<p>Markets</p>
				</span>
			</li>
		</ul>
		<img src="{{ asset('public/frontend/images/layer.png') }}">
	</span>
</div>
	
<div class="container-fluid about_crypto">
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-6 col-sm-12 col-xs-12 left_about">
				<h3>The Bridge Between Crypto and Real-World Assets</h3>
				<p>The Distributed Ledger Technology (DLT) or blockchains, are growing at a tremendous rate and are quickly replacing traditional technologies in a vast array of industries such as trading, financial services, smart contracts and identity management. The reason DLTs have grown so rapidly is due to the fact that they are backed by a secure, immutable and decentralized database. The growth of DLTs are limitless and this is because they will continue bridge the gap between ideas and real-world applications.</p>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 ryt_about">
				<div class="year">
					<span class="scale1"></span>
					<span class="scale2"></span>
					<h1>8</h1>
				</div>
				<div class="year_cont">
					<h4>Years of Experience</h4>
					<p>Get started with the most advanced Cryptocurrency trading platform powered by the most trusted, reliable and experienced technical team. The founders of GexC have been a part of the Cryptocurrency community since its inception and are all successful entrepreneurs spanning several different market sectors. The GexC founders pledge to deliver their dynamic experience to each and every user.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid why_global">
	<div class="container">
		<h2>Why Global Exchange Platform?</h2>
		<p>The primary goal of GexC is to become the global leader in the Cryptocurrency trading market. We aim to provide unmatched 24/7/365 user support alongside our superior market analysis tools to ensure our users’ success.</p>
		<img src="{{ asset('public/frontend/images/graph.jpg') }}" class="img-responsive center-block" alt="Graph">
	</div>
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="team_point">
					<img src="{{ asset('public/frontend/images/team.png') }}" class="img-responsive" alt="Team">
					<h4>Team Approach</h4>
					<p>We aim to treat all our users as part of the GexC team. We offer benefits above and beyond any other exchange currently available in hopes of building mutually beneficial opportunities for the entire GexC team. The GexC management team is here to provide guidance and assistance to each and every user.  It is our promise that these services will be available 24/7/365 to help each GexC user maximize their opportunities.</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="team_point center_point">
					<img src="{{ asset('public/frontend/images/delivery.png') }}" class="img-responsive" alt="Delivery">
					<h4>Honest Delivery</h4>
					<p>Here at GexC we pledge that all activities will be 100% transparent. The GexC management team will not keep any inner workings behind closed doors. We believe that there are plenty of growth opportunities for everyone and we want to make sure that all of our users are given every opportunity to succeed. We promise to provide an honest approach to current market conditions which in turn will help our users maximize their profits.</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class="team_point">
					<img src="{{ asset('public/frontend/images/active.png') }}" class="img-responsive" alt="Active">
					<h4>Active Management</h4>
					<p>The GexC management team will take a hands-on approach in running the day to day operations and assures complete involvement, visibility and reach. Our team spans the entire globe which allows us to provide superior communication around the clock regardless of which time zone our users are in.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid res-outr">
	<div class="sec-main-hdng">
		<h2>Under "Global Exchange's Superior Resources"</h2>
		<span>Global Exchange Has Superior Resources and Practices to Deliver<br> Superior Technology and Service</span>
	</div>
	<div class="row second-row">		
		<div class="col-md-6 col-sm-12 col-xs-12 single-res">
			<h4>Multi-Language Customer Support </h4>
			<p>We speak the languages of our customers. We currently offer multi-lingual 24/7/365 user support in six languages: English, Japanese, Mandarin, Korean, Spanish, Russian and Hindi. As our user base grows we will be adding to this list to ensure all of our users’ questions or concerns can be addressed.</p>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 single-res blue-box">
			<h4>Technical Team</h4>
			<p>We have the world’s best developers, coders and mathematicians whom have diverse experiences in all niches related to DLTs and cryptography. The members of the technical team are committed to producing results to ensure your success and ours.</p>
		</div>
	</div>
</div>

<div class="team-outr">
	<div class="container">
		<h1 class="sec-main-blue-heading">We have an Ideal Team that we are sure will lead to a successful venture with GexC.</h1>
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 outr_mmb">
					<div class="team-member">
						<a href="team.php" class="partner-img-cont">
							<span>
								<img src="{{ asset('public/frontend/images/client1.jpg') }}">
							</span>
						</a>				 
						 <div class="member-info">
						 	<h1>Emil Ryn</h1>
						 	<span>CEO & Founder</span>
						 	<div class="linkedin">
						 		<a href="https://www.linkedin.com/in/emil-ryn-4171212a/" target="_blank">
						 			<span class="fa fa-linkedin" aria-hidden="true"></span>
						 		</a>
						 	</div>
						 </div>
					</div>	 
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 outr_mmb">
					<div class="team-member">
						<a href="/team.php" class="partner-img-cont">
							<span>
								<img src="{{ asset('public/frontend/images/isaac.png') }}">
							</span>
						</a>
						 <div class="member-info">
						 	<h1>Isaac Ngalubutu</h1>
						 	<span>Co-Founder</span>
						 	<div class="linkedin">
						 		<a href="https://www.linkedin.com/in/isaac-ngalubutu-8724318/" target="_blank">
						 			<span class="fa fa-linkedin" aria-hidden="true"></span>
						 		</a>
						 	</div>
						 </div>
					</div> 
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 outr_mmb">
					<div class="team-member">
						<a href="/team.php" class="partner-img-cont">
							<span>
								<img src="{{ asset('public/frontend/images/ceo.jpg') }}">
							</span>
						</a>
						 <div class="member-info">
						 	<h1>Navdeep Garg</h1>
						 	<span>Co-Founder & CTO</span>
						 	<div class="linkedin">
						 		<a href="https://www.linkedin.com/in/navdeepgarg/" target="_blank">
						 			<span class="fa fa-linkedin" aria-hidden="true"></span>
						 		</a>
						 	</div>
						 </div>
					</div> 
				</div>
				<div class="col-lg-3 col-md-6 col-sm-6 col-xs-12 outr_mmb">
					<div class="team-member">
						<a href="/team.php" class="partner-img-cont">
							<span>
								<img src="{{ asset('public/frontend/images/client2.jpg') }}">
							</span>
						</a>			 
						 <div class="member-info">
						 	<h1>Lalit Bansal</h1>
						 	<span>Co-Founder & Director of Operation</span>
						 	<div class="linkedin">
						 		<a href="https://www.linkedin.com/in/l-k-bansal-96a130142/" target="_blank">
						 			<span class="fa fa-linkedin" aria-hidden="true"></span>
						 		</a>
						 	</div>
						 </div>
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>

<div class="roadmap homeroadmap">
	<div class="container-fluid">
		<div class="row">
			<div class="projectroadmap">
				<h3> Project Roadmap</h3>
				<p> Have a look at our future milestones</p>
				<div class="roadmapstartend">
					<span class="start">Start</span>
					<ul>						
						<li class="ist_lim">
							<div class="contentroadmap contntrd1">
								<span>Concept</span>
								<label>May 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="scnd_lim">
							<div class="contentroadmap contntrd2">
								<span>Reasearch &amp; Development</span>
								<label>June 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="thrd_lim">
							<div class="contentroadmap contntrd3">
								<span>Corporate Website</span>
								<label>July 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="frth_lim">
							<div class="contentroadmap contntrd4">
								<span>Mission</span>
								<label>Aug 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="ffth_lim">
							<div class="contentroadmap contntrd5">
								<span>Token Development</span>
								<label>Sep 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="sxth_lim">
							<div class="contentroadmap contntrd6">
								<span>ICO Pre-sales</span>
								<label>Oct 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="svth_lim">
							<div class="contentroadmap contntrd7">
								<span>Token Disbursment &amp; ICO Sales</span>
								<label>Nov 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="eight_lim">
							<div class="contentroadmap contntrd8">
								<span>Exchange Listing</span>
								<label>Dec 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="nine_lim">
							<div class="contentroadmap contntrd9">
								<span>Trading Platform Development</span>
								<label>Jan 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="ten_lim">
							<div class="contentroadmap contntrd10">
								<span>Beta Testing </span>
								<label>Mar 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="elev_lim">
							<div class="contentroadmap contntrd11">
								<span>2nd Beta Testing</span>
								<label>Apr 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="twe_lim">
							<div class="contentroadmap contntrd12">
								<span>Launch</span>
								<label>May 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
					</ul>
					<span class="end">We're working on something big after this</span>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="storage_files container-fluid">
	<div class="container container-sm">
		<div class="row">
			<h3>A great amount of storage sits unused in data centers and hard drives around the world.</h3>
			<ul>
				<li>
					<span>
						<img src="{{ asset('public/frontend/images/file.jpg') }}" class="img-responsive center-block" alt="Files">
					</span>
					<h4>Earn Gexc for hosting files</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
				</li>
				<li>
					<span>
						<img src="{{ asset('public/frontend/images/file.jpg') }}" class="img-responsive center-block" alt="Files">
					</span>
					<h4>Exchange Gexc for USD, BTC, ETH and more</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
				</li>
				<li>
					<span>
						<img src="{{ asset('public/frontend/images/file.jpg') }}" class="img-responsive center-block" alt="Files">
					</span>
					<h4>Reliably store files at hyper­competitive prices</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
				</li>
			</ul>
		</div>
	</div>
</div> -->

<div class="ques_sec">
	<img src="{{ asset('public/frontend/images/world_map.jpg') }}" alt="World_Map">
	<div class="container container-sm">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-large-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<img src="{{ asset('public/frontend/images/slider-yellow-small-circle.png') }}" class="blink-circle-one">
		<!-- <h3>Decentralised trust and reputation</h3>
		<p>Commerce and trust need each other, but current systems are complicated. We use smart contracts to simplify the relationship.</p>
		<div class="row">
			<div class="ques_left">
				<img src="{{ asset('public/frontend/images/map.png') }}" class="img-responsive" alt="Map">
			</div>
			<div class="ques_ryt">
				<ul>
					<li>
						<h4>What does it do?</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
					</li>
					<li>
						<h4>How does it work?</h4>
						<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
					</li>
					<li>
						<h4>Creating a decentralised commerce of trust</h4>
						<p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five Aldus PageMaker including versions of Lorem Ipsum.</p>
					</li>
				</ul>
			</div>
		</div> -->
	</div>
</div>

<div class="home-whitepaper">
	<div class="whiteppr_left">
		<h3>Whitepaper</h3>
		<p>We are thrilled to share the GexC whitepaper with you. This whitepaper serves as an introduction into the GexC vision, core competencies and trading platform. The GexC team would like to thank you for taking the time to share in our interest in building the first comprehensive trading platform.</p>
		<a href="{{ asset('public/frontend/files/WhitePaper-GexC-Trading-Platform.pdf') }}" target="_blank" class="btn">Read More</a>
	</div>
	<div class="whiteppr_ryt">
	<img src="{{ asset('public/frontend/images/whitepaper.jpg') }}" class="img-responsive center-block" alt="Whitepaper">
	</div>
</div>

<!-- <div class="partner-outr container-fluid">
	<div class="container container-sm">
		<div class="row">
			<div class="col-lg-6 col-md-7 col-sm-10 col-xs-12">
				<h2>Newsletter</h2>
				<span>Subscribe to our newsletter for daily updates</span>
				<form>
					<div class="form-group col-md-9 col-sm-8 col-xs-12">
						<input type="email" class="form-control" placeholder="Enter your email address...">
					</div>
					<div class="form-group col-md-3 col-sm-4 col-xs-12">
						<button type="submit" class="btn btn-primary">Subscribe</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div> -->

<div class="news-outr">
	<div class="container-fluid">
		<div class="news-left-cnt col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<h1 class="col-md-10 col-sm-10 col-xs-12 sec-main-blue-heading">LATEST FROM OUR BLOG</h1>
			<!-- <a href="#" class="">View all</a> -->
			<p>Putting up with the finest of Pieces, Knowledge Resources, News Archives, and Hand-picked Stories regarding Bitcoin, Cryptocurrency, Blockchain and other tech world to keep you ahead of the technology curve. Making sure you get the latest of industry insights and forum updates from our latest blogs</p>
			<nav>
				<ul class="control-box pager">
					<li><a data-slide="prev" href="#myCarousel" class="prev"><i class="glyphicon glyphicon-chevron-left"></i></a></li>
					<li><a data-slide="next" href="#myCarousel" class="next"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
				</ul>
			</nav>
		</div>
		<div class="news-right-cnt col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="carousel slide" id="myCarousel">
				<div class="carousel-inner">
					<div class="item active">
						<div class="col-md-6 col-sm-6 col-xs-12 test_item">
							<div class="fff">
								<div class="thumbnail">
									<a href="#"><img src="{{ asset('public/frontend/images/news-img.jpg') }}" alt=""></a>
									<div class="thumb-overlay">
										&nbsp;
									</div>
								</div>
								<div class="caption">
									<h4>Present commodo <font style="font-size: 13px; font-weight: normal;">(on 8 Aug,2017)</font></h4>
									<p>Crypto assets are all set to create a new benchmark in the history and breaking the old record of $116 billion. Bitcoin is expected to gain a huge market cap of $10 billion while Ethereum blockchain………</p>
									<!-- <a  href="#">View</a> -->
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-md-6 col-sm-6 col-xs-12 test_item">
							<div class="fff">
								<div class="thumbnail">
									<a href="#"><img src="{{ asset('public/frontend/images/news-img.jpg') }}" alt=""></a>
									<div class="thumb-overlay">
										&nbsp;
									</div>
								</div>
								<div class="caption">
									<h4>Present commodo <font style="font-size: 13px; font-weight: normal;">(on 8 Aug,2017)</font></h4>
									<p>According to sources, Overstock is all set to payments via crypto assets through its all time……</p>
									<!-- <a  href="#">View</a> -->
								</div>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="col-md-6 col-sm-6 col-xs-12 test_item">
							<div class="fff">
								<div class="thumbnail">
									<a href="#"><img src="{{ asset('public/frontend/images/news-img.jpg') }}" alt=""></a>
									<div class="thumb-overlay">
										&nbsp;
									</div>
								</div>
								<div class="caption">
									<h4>Present commodo <font style="font-size: 13px; font-weight: normal;">(on 8 Aug,2017)</font></h4>
									<p>Segregated Witness (SegWit) is all set for its activation and integration into Bitcoin Network on August 8.......</p>
									<!-- <a  href="#">View</a> -->
								</div>
							</div>
						</div>
					</div>
				</div>     
			</div><!-- /#myCarousel -->
		</div>
	</div>
</div>

<div id="contact_form" class="contact-outr">
	<div class="container">
		<div class="form-area">
			<div class="form-contact">
				<img src="{{ asset('public/frontend/images/plane-img.png') }}" class="plane-img">
				<h1 class="sec-main-blue-heading">contact us</h1>
				<span class="sec-main-blue-sub-heading">We’re all ears to your queries and will revert to you as soon as possible.</span>
				<form id="ContForm" class="contact-form" >
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0">
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 name-field">
							<div class="form-group">
							    	<label class="control-label" for="name">Name</label>
							    	<input type="text" class="form-control" id="name" name="name" />
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 email-field">
							<div class="form-group">
							    	<label class="control-label" for="email">Enter your email</label>
							    	<input type="email" class="form-control" id="email" name="email" />
							</div>
						</div>
					</div>
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding: 0"> 
						<div class="form-group txt-area col-lg-12 col-md-12 col-sm-12 col-xs-12 name-field email-field">
							<label class="control-label" for="message">Your message</label>
							<!-- <input class="form-control" rows="4" id="desc"/></textarea> -->
							<textarea id="message" class="form-control" rows="5" name="message"></textarea>
						</div>
					</div>	
					<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12" style="padding: 0">
						<input type="button" name="submitted" class="blue-btn submitBtn" value="Send">	
					</div>
				</form>

				<div class="sucess_msg">
					<h4>Your message has been sent successfully!</h4>
				</div>	
				<div class="error_msg">
					<h4>Sorry, your message could not send.</h4>
				</div>			
			</div>
		</div>
	</div>
</div>
@stop

@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
<script type="text/javascript">
	$('.submitBtn').click(function(){
		var email   = $.trim( $('#email').val() );
		var name    = $.trim( $('#name').val() );
		var message = $.trim( $('#message').val() );

		if( !email )
		{
			alert('Email is required');
		}
		else if( !message )
		{
			alert('Message cannot be empty.');
		}
		else
		{
			$.ajax({
				method : 'POST',
				data   : {
					_token:'{{ csrf_token() }}',
					email:email, name:name,
					message:message
				},
				url    : "{{ route('send_contact_us_mail') }}",
				success:function(resp){
					if( resp == 1 )
					{
						$('#email').val("");
						$('#name').val("");
						$('#message').val("");
						$('.sucess_msg').show();
					}
					else
					{
						$('.error_msg').show();
					}
				}
			});
		}
	});
</script>


<!-- Slide animation in map image section -->
<script>
	$(window).scroll(function() {
		var width = $( window ).width();
		if(width > 1200){
			var scroll = $(window).scrollTop();
			var scroll_position = $(".ques_sec")

			if(scroll_position.length) {
				var slide = scroll_position.offset().top-700;
				if (scroll >= slide) {
					$(".ques_left").addClass('left_anim');
					$(".ques_ryt li").addClass('ryt_anim');
				}
				else{
					$(".ques_left").removeClass('left_anim');
					$(".ques_ryt li").removeClass('ryt_anim');
				}
			}
		}
	});

	// Resource section fade animation 
	$(window).scroll(function() {
		var width = $( window ).width();
		var res_scroll = $(window).scrollTop();
		var res_height = $('.res-outr').outerHeight();
		var height = $(window).height();
		var res_animate = $(".res-outr");

		if(res_animate.length) {
			var anim_text = res_animate.offset().top;
			if (res_scroll > (anim_text-height)) {
				$(".res-outr").addClass('animate');
			}
			else{
				$(".res-outr").removeClass('animate');
			}
		}
	});

</script>

<!-- floating form script  -->
<script type="text/javascript">
	$('.form-control').on('focus blur', function (e) {
		$(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
	}).trigger('blur');
</script>
@stop