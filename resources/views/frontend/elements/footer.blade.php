<div class="footer-outr team_footer">
	<div class="footer-inr">
		<div class="footer-cont">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer-log-text">
						<p>{!! @$footer['content'] !!}</p>
					</div>
					<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
						<h5>Important Links</h5>
						<ul>
						@if(!empty($footer['links']))
						<?php
							$title=[];
							$link=[];
							$order=[]; ?>
							<?php $footer_links=json_decode($footer['links'],true);
							foreach ($footer_links as $key => $row) {
								    $title[$key]  = $row['title'];
								    $link[$key] = $row['link'];
								    $order[$key] = $row['order'];
								}	
												
								array_multisort($order, SORT_ASC,  $footer_links);
								
							?>
							@foreach($footer_links as $lnk)
									<li><a href="{{$lnk['link']}}">{{$lnk['title']}}</a></li>
							@endforeach
						@endif
							<!-- <li><a href="{{ route('roadmap') }}">Roadmap</a></li>
							<li><a href="{{ asset('public/frontend/files/WhitePaper-GexC-Trading-Platform.pdf') }}" target="_blank">Whitepaper</a></li>
							<li><a href="{{ route('team') }}">Team</a></li>
							<li><a href="{{ route('faq') }}">FAQs</a></li> -->
						</ul>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 footer-social-links">
						<h5>We Are On Social Media</h5>
						<a href="{{@$footer['facebook']}}" target="_blank">
							<i class="fa fa-facebook" aria-hidden="true">&nbsp;</i>
						</a>
						<a href="{{@$footer['twitter']}}" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true">&nbsp;</i>
						</a>
						<!--
						 <a href="{{@$footer['linkedin']}}" target="_blank">
							<i class="fa fa-linkedin" aria-hidden="true">&nbsp;</i>
						</a>
						-->
						<a href="{{@$footer['youtube']}}" target="_blank">
							<i class="fa fa-youtube" aria-hidden="true">&nbsp;</i>
						</a>
					<!-- 	<a href="{{@$footer['bitcoin']}}" target="_blank">
						<i class="fa fa-btc" aria-hidden="true">&nbsp;</i>
					</a> -->
						<a href="{{@$footer['bitcointalk']}}" target="_blank">
							<i class="fa fa-btc" aria-hidden="true">&nbsp;</i>
						</a>
						<!-- <a href="{{@$footer['reddit']}}" target="_blank">
							<i class="fa fa-reddit" aria-hidden="true">&nbsp</i>

						</a> -->
						<a href="{{@$footer['linkedin']}}" target="_blank">
							<i class="fa fa-linkedin" aria-hidden="true">&nbsp</i>

						</a>
						<a href="{{@$footer['telegram']}}" target="_blank">
							<i class="fa fa-telegram" aria-hidden="true">&nbsp</i>

						</a>
						
					</div>
					<div class="col-md-12 col-sm-12 col-xs-12 disclosure">
						<h4>{!! @$footer['disclosure'] !!}</h4>
					</div>
				</div>
				<div class="row copyright">
					<span>&copy; GexCrypto 2017. All rights reserved.</span> <a href="{{'terms-conditions'}}" style="color:white;">Terms & Conditions</a>
				</div>
			</div>
		</div>
	</div>
	<div class="apd_notifications">
			@include('frontend.elements.notifications')
	</div>
</div>
