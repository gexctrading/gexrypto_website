	@if(isset($notifications))
		@if(!empty($notifications))
			@foreach($notifications as $noti)
				<div class="inr_notif_blocks">
					<button type="button" class="close close-noti">
				        <span class="fa fa-times" aria-hidden="true"></span>
					</button>
					<div class="notify_text">
						<h4>{{$noti['title']}}</h4>
						<p>{{$noti['description']}}</p>
					</div>
				</div>
			@endforeach
		@endif
	@endif
			