<div class="container-fluid header comn_header">
	<div class="container container-sm">
		<nav class="navbar navbar-default">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-target="#navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="/"><img src="{{ asset('public/frontend/images/white-logo.png') }}"></a>
			</div>
			<div id="navbar" class="navbar-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="close_nav visible-xs">
						<span class="fa fa-times"></span>
					</li>

				 @if(isset($header))
				 	<?php $header_menus=json_decode($header['description'],true);
				 	foreach ($header_menus as $key => $row) {
							    $title[$key]  = $row['title'];
							    $link[$key] = $row['link'];
							    $order[$key] = $row['order'];
							}							
							array_multisort($order, SORT_ASC,  $header_menus);
							
				 	?>
				 	@if(!empty($header_menus))
				 		@foreach($header_menus as $men)
				 			<li @if(!empty($men['submenu'])) class="dropdown" @endif>
				 				<a href="{{$men['link']}}" @if($men['title']=='Whitepaper' || $men['title']=='whitepaper') target="_blank" @endif  @if($men['title']=='Presales' || $men['title']=='Pre-Sales') class="dropdown-toggle" @endif>{{$men['title']}} 
				 					@if(!empty($men['submenu'])) <i class="fa fa-caret-down"></i> @endif 
				 				</a>
				 				@if(!empty($men['submenu']))
				 				<ul class="dropdown-menu">
				 					@foreach($men['submenu'] as $subm)					 					
					 						<li>
					 							<a href="{{$subm['link']}}"> {{$subm['title']}} </a>
					 						</li>
					 				@endforeach		
					 					</ul>
					 				
				 				@endif
				 			
				 			</li>
				 			
				 		@endforeach
				 	@endif

				@endif
				<!-- <li><div class="translate_outr">
                            <div id="google_translate_element"></div>
                        </div></li> -->
					
				</ul>
			</div>
		</nav>
	</div>
</div>


<!-- <div class="comn_header">
	<div class="header-slider-inr">
		<div class="header-slider-cont">
			<div class="container-fluid header-cont">
				<div class="row">
					<div class="col-lg-2 col-md-2 col-sm-12 col-xs-6 left-logo">
						<a href="{{ route('home') }}"><img src="{{ asset('public/frontend/images/white-logo.png') }}"></a>
					</div>
					<div class="col-lg-10 col-md-10 col-sm-12 col-xs-6 right-menu">
						<div class="nav-menu">
							<nav class="navbar navbar-default">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									</button>									 
								</div>
								<div class="collapse navbar-collapse" id="navbar-collapse-1">
                        							<ul class="nav navbar-nav">
										<li><a href="{{ route('roadmap') }}">Roadmap</a></li> 
										<li><a href="{{ route('mission') }}">Mission</a></li>
										<li><a href="{{ asset('public/frontend/files/WhitePaper-GexC-Trading-Platform.pdf') }}" target="_blank">Whitepaper</a></li>
										<li><a href="{{ route('team') }}">Team</a></li>
										<li><a href="{{ route('presale') }}">Pre-Sales</a></li>
										<li><a href="{{ route('faq') }}">FAQs</a></li>
										<li class="contact-btn"><a href="{{ route('home') }}#contact_form">Contact us</a></li>
									</ul>
								</div>
							</nav>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->