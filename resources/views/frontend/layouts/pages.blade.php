@extends('frontend.layouts.master')

@section('layout_content')

	@include('frontend.elements.header')

	@yield('content')

	@include('frontend.elements.footer')

@stop