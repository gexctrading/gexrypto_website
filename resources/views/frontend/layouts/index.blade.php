@extends('frontend.layouts.master')

@section('layout_content')
	<div class="container-fluid header">
		<div class="container container-sm">
			<nav class="navbar navbar-default">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-target="#navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="/"><img src="{{ asset('public/frontend/images/white-logo.png') }}"></a>
				</div>
				<div id="navbar" class="navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li class="close_nav visible-xs">
							<span class="fa fa-times"></span>
						</li>
						@if(isset($header))
						 	<?php $header_menus=json_decode($header['description'],true);
						 		foreach ($header_menus as $key => $row) {
							    $title[$key]  = $row['title'];
							    $link[$key] = $row['link'];
							    $order[$key] = $row['order'];
							}							
							array_multisort($order, SORT_ASC,  $header_menus);

						 	?>
						 	@if(!empty($header_menus))
						 		@foreach($header_menus as $men)
				 			<li @if(!empty($men['submenu'])) class="dropdown" @endif>
				 				<a href="{{$men['link']}}" @if($men['title']=='Whitepaper' || $men['title']=='whitepaper') target="_blank" @endif  @if($men['title']=='Presales' || $men['title']=='Pre-Sales') class="dropdown-toggle" @endif>{{$men['title']}} 
				 					@if(!empty($men['submenu'])) <i class="fa fa-caret-down"></i> @endif 
				 				</a>
				 				@if(!empty($men['submenu']))
				 				<ul class="dropdown-menu">
				 					@foreach($men['submenu'] as $subm)					 					
					 						<li>
					 							<a href="{{$subm['link']}}"> {{$subm['title']}} </a>
					 						</li>
					 				@endforeach		
					 					</ul>
					 				
				 				@endif
				 			
				 			</li>
				 			
				 		@endforeach
						 	@endif
						@endif
						<!-- <li>
							<div class="translate_outr">
								<div id="google_translate_element"></div>
							</div>
						</li> -->
					</ul>
				</div>
			</nav>
		</div>
	</div>


	@yield('content')

	<div class="footer-outr">
		<div class="footer-inr">
			<div class="footer-cont">
				<div class="container-fluid">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 footer-log-text">
							<!-- <img src="img/white-logo.png"> -->
							<p>{!! @$footer['content'] !!}</p>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
							<h5>Important Links</h5>
							<ul>
							@if(!empty($footer['links']))
							<?php
							$title=[];
							$link=[];
							$order=[]; ?>

								<?php $footer_links=json_decode($footer['links'],true);
								foreach ($footer_links as $key => $row) {
								    $title[$key]  = $row['title'];
								    $link[$key] = $row['link'];
								    $order[$key] = $row['order'];
								}	
												
								array_multisort($order, SORT_ASC,  $footer_links);
								?>

								@foreach($footer_links as $lnk)
										<li><a href="{{$lnk['link']}}">{{$lnk['title']}}</a></li>
								@endforeach
							@endif
							</ul>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 footer-social-links">
							<h5>We Are On Social Media</h5>
							<a href="{{@$footer['facebook']}}" target="_blank">
							<i class="fa fa-facebook" aria-hidden="true">&nbsp;</i>
						</a>
						<a href="{{@$footer['twitter']}}" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true">&nbsp;</i>
						</a>
						<!-- <a href="{{@$footer['linkedin']}}" target="_blank">
							<i class="fa fa-linkedin" aria-hidden="true">&nbsp;</i>
						</a> -->
						<a href="{{@$footer['youtube']}}" target="_blank">
							<i class="fa fa-youtube" aria-hidden="true">&nbsp;</i>
						</a>
						<!-- <a href="{{@$footer['bitcoin']}}" target="_blank">
							<i class="fa fa-btc" aria-hidden="true">&nbsp;</i>
						</a> -->
						<a href="{{@$footer['bitcointalk']}}" target="_blank">
							<i class="fa fa-btc" aria-hidden="true">&nbsp;</i>
						</a>
						<!-- <a href="{{@$footer['reddit']}}" target="_blank">
							<i class="fa fa-reddit" aria-hidden="true">&nbsp</i>

						</a> -->
						<a href="{{@$footer['linkedin']}}" target="_blank">
							<i class="fa fa-linkedin" aria-hidden="true">&nbsp</i>
						</a>
						<a href="{{@$footer['telegram']}}" target="_blank">
							<i class="fa fa-telegram" aria-hidden="true">&nbsp</i>

						</a>

							<!-- <div class="counter_sec">
								<h2 class="counter">{{ 1200 + $viewCount }}</h2>
								<p>Users Visited</p>
							</div> -->
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12 disclosure">
							<h4>{!! @$footer['disclosure'] !!}</h4>
						</div>
					</div>
					<div class="row copyright">
						<span>&copy; GexCrypto 2017. All rights reserved.</span> <a href="{{'terms-conditions'}}" style="color:white;">Terms & Conditions</a>
					</div>
				</div>
			</div>
		</div>
		<div class="apd_notifications">
			@include('frontend.elements.notifications')
		</div>
	</div>

@stop
