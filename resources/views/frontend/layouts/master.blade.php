<!DOCTYPE html>
<html>
<head>
 @include('frontend.elements.analyticstracking')

 
	<title>@yield('title')</title>
	<meta charset="utf-8"> 
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('public/frontend/css/font-awesome.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/style.css') }}">
	<link rel="icon" type="image/png" href="{{ asset('public/frontend/images/favicon.png') }}" />
	<script type="text/javascript" src="{{ asset('public/frontend/js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/frontend/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/frontend/js/jquery.validate.js') }}"></script>
	<script src="{{ asset('public/frontend/js/custom.js') }}"></script>
  <script src="{{ asset('public/js/firebase.js') }}"></script>

  <script> 
    $(document).ready(function(){
      var width = $( window ).width();
      if(width > 992){
        $(".dropdown").hover(
          function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).fadeIn("400");
            $(this).toggleClass('open');
          },
          function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).fadeOut("400");
            $(this).toggleClass('open');
          }
        );
      }
      else{
        $('.dropdown').click(
          function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).fadeIn("400");
            $(this).toggleClass('open');
          },
          function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true).fadeOut("400");
            $(this).toggleClass('open');
          }
        );
      }
    });
  </script>

<script>
  // Initialize Firebase
  


  // Initialize Firebase
var config = {
    apiKey: "AIzaSyDCAy5DHeufTkyL7wvnEjTADe1dty8Bkks",
    authDomain: "gexcrypto-180709.firebaseapp.com",
    databaseURL: "https://gexcrypto-180709.firebaseio.com",
    projectId: "gexcrypto-180709",
    storageBucket: "",
    messagingSenderId: "878262428906"
  };
  firebase.initializeApp(config);


  const messaging=firebase.messaging();
  
  messaging.requestPermission()
  .then(function(){
  	console.log('Have Permission');
  	localStorage.setItem('notification','allow');
    var token=messaging.getToken();
    console.log(token);
  	
  	return messaging.getToken();

  })
  .then(function(token){
  	console.log(token);
    $.ajax({
      type: "get",
      url :"{{url('/save_token')}}"+'/'+ token,
      data: '1',
      success: function(data){
        
      }
    });
  })
  .catch(function(err){
  	localStorage.setItem('notification','notallow');
  	console.log(err);
     $.ajax({
      type: "get",
      url :"{{url('/remove_token')}}",
      //data: '1',
      success: function(data){
        
      }
    });
  })
  messaging.onMessage(function(payload){
  	console.log('onMessage: ', payload);
  });

function showNotification(title,content) {
  try {
     var options = {
      body: content
      //icon: theIcon
  }
    var notification = new Notification(title,options);
  } catch (err) {
    alert('Notification API not available: ' + err);
  }
}

function notifyMe(title,content) {
  if (!("Notification" in window)) {
    alert('Notification API not supported.');
    return;
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    // If it's okay let's create a notification
    showNotification(title,content);
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        showNotification(title,content);
      }
    });
  }
}

  function ajaxNotification(){
  $.ajax({
      type: "get",
      url :"{{url('/show_notification')}}",
      data: '1',
      success: function(data){
        //alert(data);
      //  $('.apd_notifications').html(data);
        if(data.status=='found'){
          showNotification(data.title,data.desc);
         }
      }
  });
}

setInterval (function() { 
  ajaxNotification();
},5000);

$(document).on('click','.close-noti',function(){
  
  $(this).parent().remove();
});

</script>

	@yield('page_level_css')

	@yield('page_level_js_top')
  


 @include('frontend.elements.facebooktracking')

</head>
<body>
	<div id="fb-root"></div>
<!-- <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1411524755560484";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> -->
	@yield('layout_content')


	@yield('page_level_js')
	<script>
	 function googleTranslateElementInit(){
          new google.translate.TranslateElement('google_translate_element');
      }
</script>


<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

</body>
</html>