@extends('frontend.layouts.pages')

@section('title', 'Apply Job')

@section('content')
<style type="text/css">
  .error{
    color:#A94442;
  }
  .inputClass{
    color:black !important;
  }
<?php //echo "<pre>";print_r($position);die; ?>
</style>
  <div class="container-fluid blue_bg_top">
    <div class="container">
      <div class="row">
        <h2><span class="white_line">Career with us</span></h2>
        <p>Re-imagine Your Career With Gexcrypto.</p>
      </div>
    </div>
  </div>

  <div class="container apply_job">
    <h2><?= @($position); ?>
      <a href="Javascript:void(0)">View all jobs<span class="fa fa-angle-double-right" aria-hidden="true"></span></a>
    </h2>
    <div class="row">
      <div class="contact_form_left">
        <h4>Apply for this Job</h4>
        <form method="post" id="ProductForm" enctype="multipart/form-data" action="{{ url('/applyJob') }}">
        {{csrf_field()}}
          <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
            <label class="float" for="name">First Name</label>
            <input id="first_name" class="form-control inputClass specialchar" name="first_name" id="first_name" type="text" value="{{ old('first_name') }}"> 
            <span class="text-danger">{{ $errors->first('first_name') }}</span>
            @if (session('valid'))
                <span class="valid">{{ Session::get('valid') }}</span>
            @endif 
          </div>
<!--           <input type = "hidden" value = "<?php echo $jobs_id; ?>" name = "jobs_id"> -->
            <input type="hidden" value="<?= Request::Segment(2); ?>" name="job_id">
          <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
            <label class="float" for="name">Last Name</label>
            <input id="last_name" class="form-control specialchar inputClass" name="last_name"  type="text" value = "{{ old('last_name') }}" >
           <span class="text-danger">{{ $errors->first('last_name') }}</span>
           @if (session('valid'))
              <span class="valid">{{ Session::get('valid') }}</span>
          @endif
          </div>
          <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
            <label class="float" for="email">Email</label>
            <input id="email" class="form-control inputClass" name="email"  type="email" value="{{ old('email') }}">
            <span class="text-danger">{{ $errors->first('email') }}</span>

          </div>
          <div class="form-group {{ $errors->has('contact') ? 'has-error' : '' }}">
            <label class="float" for="contact">Contact Number</label>
            <input id="contact" class="form-control inputClass isNumber" name="contact"   maxlength="10"  type="text" value="{{ old('contact') }}">
            <span class="text-danger">{{ $errors->first('contact') }}</span>
            @if (session('valid'))
                <span class="valid">{{ Session::get('valid') }}</span>
            @endif
          </div>
          <!-- <div class="form-group {{ $errors->has('qualification') ? 'has-error' : '' }}">
            <label class="float" for="qualification">Select Qualification</label>
            <select id="qualification" name="qualification"  class="form-control specialchar selectOpt" >
              <option value=""></option>
              <option value="Graduate">Graduate</option>
              <option value="Post Graduate">Post Graduate</option>
            </select>
            <span class="text-danger">{{ $errors->first('qualification') }}</span>
          </div> -->
           <div class="form-group {{ $errors->has('experience') ? 'has-error' : '' }}">
            <label class="float" for="experience">Select Experience</label>
            <select id="experience" name="experience"  class="form-control specialchar selectOpt">
              <option value=""></option>
              <option value="Fresher">Fresher</option>
              <option value="Experienced">Experienced</option>
            </select>
            <span class="text-danger">{{ $errors->first('experience') }}</span>
          </div>
          <div class="form-group {{ $errors->has('location') ? 'has-error' : '' }}">
            <label class="float" for="location">Select Location</label>
            <select id="location" name="location"  class="form-control specialchar selectOpt" >
              <option value=""></option>
              <option value="California">California</option>
              <option value="Gurugram">Gurugram</option>
              <!-- <option value="Ludhiana">Ludhiana</option> -->
            </select>
            <span class="text-danger">{{ $errors->first('location') }}</span>
          </div>
          <!-- <div class="form-group">
            <label class="float" for="employer">Current Employer</label>
            <input id="current" class="form-control specialchar inputClass" name="current"  type="text">
            @if (session('valid'))
                <span class="valid">{{ Session::get('valid') }}</span>
            @endif
          </div> -->
         
        <div class="form-group upload_file">
            <a class="clickfile" href="javascript:void(0)"><i class="fa fa-file-text-o" aria-hidden="true"></i>Upload your Resume</a>
            <input type="file" name="file"  class="file" >
            <input type="text" name="" readonly="readonly"  class="filename">
        </div> 

         <div class="form-group">
             <div class="g-recaptcha" data-sitekey="6LfiiyIUAAAAAGufM7sy_pAka-iH6KeHNJqd6Fho"></div>
              <input type="hidden" class="hiddenRecaptcha required" name="hiddenRecaptcha" id="hiddenRecaptcha">
        </div> 
        <div id="divCL"></div>


          <div class="form-group">
            <button class="btn blue_btn" type="submit" id="submit">Submit</button>
            <div id="divCLe"></div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <style type="text/css">
    .valid{
      color: #A94442;
      font-size: 15px;
    }
    #divCLe{
      margin-top:20px;
    }
  </style>
@endsection

@section('page_level_js')
<script src="{{ URL('/public/js/google_api.js') }}"></script>
<script type="text/javascript" src="{{ URL('/public/js/jquery.validate.js') }}"></script>
<script type="text/javascript">
  $(function() {
   
    // Setup form validation on the #register-form element
    $("#ProductForm").validate({
    
        // Specify the validation rules
        ignore: ".ignore",
        rules: {
            first_name: "required",
            last_name: "required",
            contact: "required",
            // qualification: "required",
            location: "required",
            experience: "required",
            
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
              hiddenRecaptcha: {
             required: function() {
                 if(grecaptcha.getResponse() == '') {
                     return true;
                 } else {
                     return false;
                 }
             }
         }
        },

      // Specify the validation error messages
      // messages: {
      //     first_name: "<span class='valid'>Field is required</span>",
      //     last_name: "<span class='valid'>Field is required</span>",
      //     contact: "<span class='valid'>Field is required</span>",
      //     qualification: "<span class='valid'>Field is required</span>",
      //     experience: "<span class='valid'>Field is required</span>",
      //     email: "<span class='valid'>Field is required</span>",
      //     file: "<span class='valid'>Field is required</span>",
      //     hiddenRecaptcha: "<span class='valid ss'>Field is required</span>"
      // },
        
      submitHandler: function(form) {
        form.submit();
      }
    });
  });

</script>

<script>
      jQuery(document).ready(function(){
        jQuery('.clickfile').on('click', function(e){
            e.preventDefault();
            jQuery('.file')[0].click();
        });

        jQuery('.file').change(function() {
          
          var file_name = $('input[type=file]').val().split('\\').pop();
          var lastIndex = file_name.lastIndexOf("\\");
          myfile= $(this).val();
         var ext = file_name.split('.').pop();
         if(ext=="pdf" || ext=="docx" || ext=="doc"){
          $('.filename').val(file_name);  
         } else{

            $('.file').val("");
           $('.filename').val(""); 
           alert('Please select correct type of document')
           return false;
         }
         });
      });

      $('.isNumber').keypress(function (event) {
            return isNumber(event, this)
        });

  function isNumber(evt, element)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
            return true;
      }

    </script>

    <script>
      jQuery(document).ready(function() {
        jQuery("form").trigger("reset");
        
        jQuery("body").on("input propertychange", ".form-group", function(e) {
          jQuery(this).toggleClass("float-value", !!jQuery(e.target).val());
        }).on("focus", ".form-group", function() {
          jQuery(this).addClass("float-focus");
        }).on("blur", ".form-group", function() {
          jQuery(this).removeClass("float-focus");
        });
      });

      jQuery('.specialchar').keypress(function (e) {
          var key = e.keyCode || e.which; 
          var regex = new RegExp("^[a-z A-Z-]+$");
          if(key==8){
            return;
          }
          else{
            var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
            if (regex.test(str)) {
                return true;
            }
          }
          e.preventDefault();
          return false;
      });
   </script>
@endsection