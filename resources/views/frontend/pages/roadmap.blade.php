@extends('frontend.layouts.pages')

@section('title', 'Roadmap')

@section('content')

<div class="container-fluid roadmap">

	<div class="container-fluid">

		<div class="row">
			<div class="projectroadmap">
				<h3> {{$content['title']}}</h3>
				<p>  {{$content['heading']}}</p>
				<div class="roadmapstartend">
					<span class="start"> {{$content['start_text']}}</span>
					<ul>	
					<?php $stages=json_decode($content['stage'],true); $i=1;?>	
					@foreach($stages as $key=>$stage)				
						<li class="@if($i%2==0) {{'scnd_lim'}} @else {{'ist_lim'}} @endif">
							<div class="contentroadmap contntrd{{$i}}">
								<span>{{$stage['title']}}</span>
								<label>{{$stage['date']}}</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
					<?php $i++; ?>
					@endforeach
						<!-- <li class="scnd_lim">
							<div class="contentroadmap contntrd2">
								<span>Research & Development</span>
								<label>June 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="thrd_lim">
							<div class="contentroadmap contntrd3">
								<span>Corporate Website</span>
								<label>July 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="frth_lim">
							<div class="contentroadmap contntrd4">
								<span>Mission</span>
								<label>Aug 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="ffth_lim">
							<div class="contentroadmap contntrd5">
								<span>Token Development</span>
								<label>Sep 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="sxth_lim">
							<div class="contentroadmap contntrd6">
								<span>ICO Pre-sales</span>
								<label>Oct 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="svth_lim">
							<div class="contentroadmap contntrd7">
								<span>Token Disbursment & ICO Sales</span>
								<label>Nov 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="eight_lim">
							<div class="contentroadmap contntrd8">
								<span>Exchange Listing</span>
								<label>Dec 2017</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="nine_lim">
							<div class="contentroadmap contntrd9">
								<span>Trading Platform Development</span>
								<label>Jan 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="ten_lim">
							<div class="contentroadmap contntrd10">
								<span>Beta Testing </span>
								<label>Mar 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="elev_lim">
							<div class="contentroadmap contntrd11">
								<span>2nd Beta Testing</span>
								<label>Apr 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li>
						<li class="twe_lim">
							<div class="contentroadmap contntrd12">
								<span>Launch</span>
								<label>May 2018</label>
							</div>
							<div class="bubblebotom"><span></span></div>
						</li> -->
					</ul>
					<span class="end">{{$content['end_text']}}</span>
				</div>
			</div>
		</div>

	</div>

</div>
@stop


@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
@stop