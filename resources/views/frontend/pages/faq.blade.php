@extends('frontend.layouts.pages')

@section('title', 'FAQ')

@section('content')
<div class="container-fluid faq">
	<div class="container">
	{!! $content['description'] !!}

		<div class="panel-group" id="accordion">
		@foreach($list as $key=>$lst)
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$key}}">
						{{$lst['title']}}
						</a>
					</h4>
				</div>
				<div id="collapse{{$key}}" class="panel-collapse collapse ">
					<div class="panel-body">
							{{$lst['description']}}
					</div>
				</div>
			</div>
		@endforeach
			
		</div>
	</div>
</div>
@stop
