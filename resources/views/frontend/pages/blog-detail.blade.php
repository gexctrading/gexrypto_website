@extends('frontend.layouts.pages')

@section('title', 'Blog Detail')
@section('page_level_css')
@parent
  <meta property="og:url"           content="{{ url('blog-detail/'.base64_encode(convert_uuencode($post['id']))) }}" />
  <meta property="og:type"          content="website" />
  <meta property="og:title"         content="{{$post['title']}}" />
  <meta property="og:description"   content="{{$post['description']}}" />
  <meta property="og:image"         content="{{asset('public/admin/blog/'.$post['image'])}}" />

  	<meta name="twitter:card" content="summary" />
	<meta name="twitter:description" content="{{$post['description']}}" />
	<meta name="twitter:title" content="{{$post['title']}}" />
	<meta name="twitter:site" content="@gexcrypto" />
	<meta name="twitter:image" content="{{asset('public/admin/blog/'.$post['image'])}}" />
	<meta name="twitter:creator" content="@revinfotech" />

  <!-- Load Facebook SDK for JavaScript -->
  <div id="fb-root"></div>
 <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=452446421770549";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
  
@stop


@section('content')

<div class="container-fluid blog_sec">
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-12 col-xs-12 blog_head">
				<!-- <h1>Blog Detail</h1>
				<p>Explore Gexcrypto latest blogs</p> -->
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 blog_list_outr">
				<div class="col-lg-9 col-md-8 col-sm-12 col-xs-12 blog_detail_left">
					<div class="blog_detail_inr">
						<h2>{{$post['title']}}</h2>
						<span><i class="fa fa-calendar" aria-hidden="true"></i> <?php  $date=date_create($post['posted_date']); echo $date=date_format($date,'j F, Y') ?> - By {{$post['author']}}</span>
						
						<span class="blogdetail_img">
							<img src="{{ asset('public/admin/blog/'.$post['image']) }}" class="img-responsive">
							<div class="thumb-overlay"></div>
						</span>
						<div class="blog_desc">
							<p>{!! $post['description'] !!} </p>
							@if($post['link']!='')
							<p><b>Reference:</b> <a href="{{$post['link']}}" target="_blank">{{$post['link']}}</a> </p>
							@endif
						</div>
						<div class="share_blog">

						 <?php $desc=substr($post['description'], 0,200);
			                    $strp=strip_tags($desc);
			                    $tit=strip_tags($post['title']);
			                    $title=strtr($tit, ["#" => " "]);
			                    $rep=strtr($strp, ["#" => " "]);
			               ?>

							<p>Share this Post: </p>

							<!-- <div class="fb-share-button" data-href="{{url('blog-detail/'.base64_encode(convert_uuencode($post['id'])))}}" data-layout="button_count" data-size="small" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{url('blog-detail/'.base64_encode(convert_uuencode($post['id'])))}}&amp;src=sdkpreparse">Share</a></div> -->

							<?php
		                    $title=urlencode($post['title']);
		                    $url=url('blog-detail/'.base64_encode(convert_uuencode($post['id'])));
		                    $image=asset('public/admin/blog/'.$post['image']) ;
		                	?>
		                	<!-- <a onClick="window.open('https://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $post['title'];?>&amp;p[url]=<?php echo $url; ?>&amp;p[images][0]=<?php echo $image;?>', 'sharer', 'toolbar=0,status=0,width=548,height=325');" target="_parent" href="javascript: void(0)">
		                			                    <i class="fa fa-facebook" aria-hidden="true"></i>
		                	</a>  -->

							

		                	<a class="w-inline-block social-share-btn tw" href="https://twitter.com/intent/tweet?" target="_blank" title="Tweet" onclick="window.open('https://twitter.com/intent/tweet?text=<?php echo $title;?>'  + ':%20 ' + encodeURIComponent(document.URL)); return false;">
		                		
		                		<i class="fa fa-twitter" aria-hidden="true"></i>
		                	</a>

						</div>
					</div>
					
					<div class="news_comnt">
						<div class="comment_list">
							<h4>{{count($post['comment'])}} Comments</h4>
							@if(!empty($post['comment']))
								<ul>
									@foreach($post['comment'] as $cmt )
										 <li>
											<div class="thmb_cmt">
												<a href="#">
													<img src="{{ asset('public/frontend/images/dummy.png') }}" class="img-responsive">
												</a>
											</div>
											<div class="desc_cmt">
												<h4>{{$cmt['name']}}<span><?php  $date=date_create($cmt['created_at']); echo $date=date_format($date,'j F, Y') ?></span></h4>
												<p>{{$cmt['message']}}</p>
												
											</div 
										</li>
									@endforeach
								</ul>
							@endif
						
						</div>
						<h4 class="rpy_hd"><span class="blue_line">Leave a comment</span></h4>
						<div class="alert alert-success suc_msg" role="alert" >
				                		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				                			<span aria-hidden="true">×</span>
				                		</button>
				                		<span>Submitted successfully!</span>
			            		</div>

						<form class="" method="post" id="commentForm" action="{{url('/blog-comment')}}">
						{{csrf_field()}}
							<div class="form-group col-md-6 col-sm-12 col-xs-12">
								<label class="control-label" for="name">Name</label>
								<input class="form-control" required aria-required="true" type="text" name="name">
								<input class="form-control" required aria-required="true" type="hidden" name="post_id" value="{{$post['id']}}">
							</div>
							<div class="form-group col-md-6 col-sm-12 col-xs-12">
								<label class="control-label" for="email">Email</label>
								<input class="form-control" required  type="email" name="email">
							</div>
							<div class="form-group col-md-12 col-sm-12 col-xs-12">
								<label class="float" for="message">Your Message</label>
								<textarea rows="3" class="form-control" name="message" id="message" required></textarea>
							</div>
							<div class="form-group col-md-12 col-sm-12 col-xs-12">
								<button type="submit" class="btn">Submit</button>
							</div>	
						</form>
					</div>
					
				</div>
				<div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 blog_list_ryt">
					<h4>Search Blog</h4>
					<form action="{{url('blog')}}" method="post">
						{{csrf_field()}}
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search for..." name="search" value="{{@$search}}">
							<span class="input-group-btn">
								<button class="btn" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
						</div>
					</form>
					<h4>Latest Posts</h4>
					<ul>
						@foreach($latest as $lat)
						<li>
							<a href="{{url('blog-detail/'.base64_encode(convert_uuencode($lat['id'])))}}" >
								<h5>{{$lat['title']}}</h5>
								<span><i class="fa fa-calendar" aria-hidden="true"></i> <?php $date=date_create($lat['posted_date']); echo $date=date_format($date,'j F, Y') ?></span>
							</a>
						</li>
						@endforeach						
					</ul>					
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
<script type="text/javascript">
	$('.suc_msg').hide();
	$('#commentForm').validate({  

    submitHandler: function(form) {
        $.ajax({
            url: form.action,
            type: form.method,
            data: $(form).serialize(),
            success: function(data) {
                if(data.status=='success'){
					$('.suc_msg').show();
					$('.form-control').val('');
					$('.form-control').text('');
                }
            }            
        });
    }
});

</script>
@stop
