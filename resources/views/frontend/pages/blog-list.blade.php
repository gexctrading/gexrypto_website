@extends('frontend.layouts.pages')

@section('title', 'Blog List')

@section('content')

<div class="container-fluid blog_sec">
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-12 col-xs-12 blog_head">
				<h1>Blog</h1>
				<p>Explore Gexcrypto latest blogs</p>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12 blog_list_outr">
				<div class="blog_tabs">
					<ul class="nav nav-tabs">
						<li class="@if(isset($_GET['latest-blog']) || isset($_GET['gexcrypto-blog'])) @if(isset($_GET['gexcrypto-blog'])) active @endif @else active @endif">
							<a href="#Gexcrypto" data-toggle="tab">Gexcrypto Blog</a>
						</li>
						<li class="@if(isset($_GET['latest-blog']) || isset($_GET['gexcrypto-blog'])) @if(isset($_GET['latest-blog'])) active @endif @endif">
							<a href="#latest" data-toggle="tab">Latest Blog</a>
						</li>
					</ul>
				</div>
				<div class="tab-content">
					<div class="tab-pane  @if(isset($_GET['latest-blog']) || isset($_GET['gexcrypto-blog'])) @if(isset($_GET['gexcrypto-blog'])) active in @endif @else active in @endif" id="Gexcrypto">
						<div class="col-md-12 col-sm-12 col-xs-12 blog_list_left">
							<ul>
								<?php $j=0; ?>
								@if(!empty($crypto))
								@foreach($crypto as $post)
								<li>
									<div class="blog_blocks">
										<h3>{{$post['title']}}</h3>
										<div class="blog_img">
											<a href="#">
												<img src="{{ asset('public/admin/blog/'.$post['image']) }}">
											</a>
											<div class="thumb-overlay"></div>
										</div>
										<div class="blog_detail">
											<span><i class="fa fa-calendar" aria-hidden="true"></i> <?php $date=date_create($post['posted_date']); echo $date=date_format($date,'j F, Y') ?>- By {{$post['author']}}</span>
											<p>{!! substr($post['description'],0,300) !!}</p>
											<a href="{{url('blog-detail/'.base64_encode(convert_uuencode($post['id'])))}}" class="read_more">Read More</a>
										</div>
									</div>
								</li>
								<?php $j++; ?>
								@endforeach

								@endif

								@if($j==0)
								<center><h3>No Posts Found</h3></center>
								@endif
							</ul>
							<span class="pagination-link">{{ $crypto->links() }}</span>
						</div>
					</div>
					<div class="tab-pane  @if(isset($_GET['latest-blog']) || isset($_GET['gexcrypto-blog'])) @if(isset($_GET['latest-blog'])) active in @endif @endif" id="latest">
						<div class="col-md-12 col-sm-12 col-xs-12 blog_list_left">
							<ul>
								<?php $k=0; ?>
								@if(!empty($lat_posts))
								@foreach($lat_posts as $post)
								<li>
									<div class="blog_blocks">
										<h3>{{$post['title']}}</h3>
										<div class="blog_img">
											<a href="#">
												<img src="{{ asset('public/admin/blog/'.$post['image']) }}">
											</a>
											<div class="thumb-overlay"></div>
										</div>
										<div class="blog_detail">
											<span><i class="fa fa-calendar" aria-hidden="true"></i> <?php $date=date_create($post['posted_date']); echo $date=date_format($date,'j F, Y') ?>- By {{$post['author']}}</span>
											<p>{!! substr($post['description'],0,200) !!}</p>
											<a href="{{url('blog-detail/'.base64_encode(convert_uuencode($post['id'])))}}" class="read_more">Read More</a>
										</div>
									</div>
								</li>
								<?php $k++; ?>
								@endforeach

								@endif

								@if($k==0)
								<center><h3>No Posts Found</h3></center>
								@endif
							</ul>
							<span class="pagination-link">{{ $lat_posts->links() }}</span>
						</div>
					</div>
				</div>
				
				<!-- <div class="col-md-4 col-sm-12 col-xs-12 blog_list_ryt">
					<h4>Search Blog</h4>
					<form action="{{url('blog')}}" method="post">
						{{csrf_field()}}
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Search for..." name="search" value="{{$search}}">
							<span class="input-group-btn">
								<button class="btn" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
							</span>
						</div>
					</form>
					<h4>Latest Posts</h4>
					<ul>
						@foreach($latest as $lat)
						<li>
							<a href="{{url('blog-detail/'.base64_encode(convert_uuencode($lat['id'])))}}" >
								<h5>{{$lat['title']}}</h5>
								<span><i class="fa fa-calendar" aria-hidden="true"></i> <?php $date=date_create($lat['posted_date']); echo $date=date_format($date,'j F, Y') ?></span>

							</a>
						</li>
						@endforeach
					</ul>
				</div> -->
			</div>
		</div>
	</div>
</div>

@stop

@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
@stop
