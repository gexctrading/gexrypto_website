@extends('frontend.layouts.index')

@section('title', 'Team')

@section('content')

<div class="team_group">
	<div class="grp_overlay"></div>
	<img src="{{ asset('public/frontend/images/group_img.jpg') }}" alt="Team" class="img-responsive">
</div>

<div class="container team_text">
	<h4>
		<i class="fa fa-quote-left" aria-hidden="true"></i>{!! $content['description'] !!}<i class="fa fa-quote-right" aria-hidden="true"></i>
	</h4>
</div>

<div class="container-fluid team_sec">
	<div class="container container-sm">
		<ul>
		@foreach($members as $key=>$member)
			<li class="@if($key==0) {{'active'}} @endif">
				<a href="#div-{{$key}}" data-toggle="tab">
					<h3>{{$member['title']}}</h3>
				</a>
			</li>
		@endforeach

		</ul>
		<div class="tab-content">
		@foreach($members as $keys=>$mem)
			<div id="div-{{$keys}}" class="tab-pane @if($keys==0) {{'active in'}} @else {{'fade'}} @endif">
				<ul>
					@foreach($mem['members'] as $mbr)
						<li class="team_list">
							<div class="member_sec">
								<span class="mem_overlay">
									<img src="{{ asset('public/admin/team/'.$mbr['image']) }}" class="center-block">
									@if($mbr['linkedin']!='')
										<div class="linkedin">
									 		<a href="{{$mbr['linkedin']}}" target="_blank">
									 			<span class="fa fa-linkedin" aria-hidden="true"></span>
									 		</a>
									 	</div>
									 @endif
								</span>
								<div class="member_desig">
									<h4>{{ucwords(strtolower($mbr['name']))}}</h4>
									<span>{{$mbr['designation']}}</span>
								</div>
							</div>
						</li>
					@endforeach
					
				</ul>
			</div>
		@endforeach
			
		</div>
	</div>


		
		</div>
	</div>
</div>
@stop


@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
@stop
