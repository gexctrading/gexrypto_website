@extends('frontend.layouts.pages')

@section('title', 'Individual')

@section('content')

<link rel="stylesheet" href="{{asset('public/css/naoTooltips.css')}}">
<style>
.label.error{
	color:red;
	font-size: 12px;
}
.submit_form{
  background-color: #2e3e62;
}
.cust {
  margin-top: 10%;
}
 #overlay{
     position: fixed;
     z-index: 99999;
     top: 0;
     left: 0;
     bottom: 0;
     right: 0;
     text-align: center;
     display: none;
     background: rgba(0,0,0,0.8);
     transition: 1s 0.4s;
     -webkit-transition: 1s 0.4s;
     -moz-transition: 1s 0.4s;
     -ms-transition: 1s 0.4s;
    }
    .load_img{
     position: absolute;
     top: 50%;
     transform: translate(-50%, -50%);
     left: 50%;
     width: 6%;
     z-index: 999;
    }
    .clip{
    	cursor: grab;
    }
    .send_address{
    	display: none;
    }
    .send_address{
    	position: relative;
    }
    .send_address .naoTooltip-wrap{
    	position: absolute;
    	top: 18px;
    	right: 0;
    }
    .send_address .naoTooltip-wrap span.fa{
    	font-size: 16px;
    }
    .send_address .naoTooltip.nt-small{
    	max-width: 180px!important;
    	top: 35px!important;
    }
    /*.send_address .naoTooltip.nt-bottom::before{
    	left: 60px!important;
    }*/
</style>

<div id="overlay">
 <img class="load_img" src="{{asset('public/frontend/images/loading.gif')}}" >
</div>
<!-- center section -->

<div class="container-fluid token_steps">
	<div class="container">
		<div class="row">
			<div class="step_heading">
				<h1 >Buy Gex Tokens</h1>
				<p >Join the most revolutionary crowdsale, investing is as simple as following these three steps:</p>
			</div>
		</div>
	</div>
	<div class="container container-sm steps">
		<div class="row">
			<div class="col-md-5 col-sm-12 col-xs-12 left_tokensteps">
				<div id="token_step1" class="check_field token_tabs active">
					<h3><span>Step 1</span> Agree to the Terms and Conditions</h3>
					<form id="stepform">
					<ul>
						<li>
							<input type="checkbox" class="hideme" id="check1" required />
							<label for="check1">I have read and agree with the <a href="{{asset('public/frontend/files/WhitePaper-GexC-Trading-Platform.pdf')}}" target="_blank">GexCrypto Whitepaper</a></label>
						</li>
						<li>
							<input type="checkbox" class="hideme" id="check2"  required />
							<label for="check2">By continuing through to the GexCrypto , I acknowledge that I have read and agree to the Terms & conditions and agree to be bound by them. </label>
						</li>
						<li>
							<input type="checkbox" class="hideme" id="check3" required />
							<label for="check3">I confirm that i am not the citizen of The United States/Resident of America, Canada, China, and Taiwan due to existing regulations in their respective states, or associated with any jurisdiction where distribution or dissemination of these Terms, or participation in the Event, is prohibited or restricted</label>
						</li>
						<li>
							<input type="checkbox" class="hideme" id="check4" required />
							<label for="check4">I acknowledge that purchasing GexCrypto Tokens is entirely at my own risk and accepts  <a href="{{asset('terms-sales')}}" target="_blank">Terms of Sales</a>.</label>
						</li>
					</ul>
					</form>
					<div class="botm_buttons">
						<button type="button" name="continue" class="continue">Continue</button>
					</div>
				</div>
				<div id="token_step2" class="token_tabs">
					<h3><span>Step 2</span> Enter Your Details</h3>
					<form id="ContForm" class="contact-form form-contact">
					{{csrf_field()}}
					<input type="hidden" name="page" value="join-now" required />

					<input type="hidden" name="whitepaper_agreement" value="" required id="checkbox1" />
					<input type="hidden" name="terms_and_conditions" value="" required id="checkbox2"/>
					<input type="hidden" name="citizen_confirmation" value="" required id="checkbox3" />
					<input type="hidden" name="acknowledge_risk" value="" required id="checkbox4" />

						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 email-field">
							<div class="form-group">
							    	<label class="control-label" for="name">Name</label>
							    	<input type="text" class="form-control namee" id="name" name="name" required />
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 email-field">
							<div class="form-group">
							    	<label class="control-label" for="email">Enter your email</label>
							    	<input type="email" class="form-control emaill" id="email" name="email" required/>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 email-field">
							<div class="form-group">
							    	<label class="control-label" for="email cemaill">Confirm your email</label>
							    	<input type="email" class="form-control" id="emails" name="cemail" required/>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 email-field">
							<div class="form-group">
							    	<label class="control-label" for="address">Ethereum wallet/account address</label>
							    	<input type="text" class="form-control eth_address" id="address" name="address" required   rangelength="[42,42]"/>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 email-field select-field">
							<div class="form-group">
							<label class="control-label" for="address">Select Currency</label>
							    	
							    	<select class="form-control currency_type" name="type" required>				
							    		<option value="Ethereum">Ethereum </option>
							    		<option value="Bitcoin">Bitcoin</option>
							    		<option value="Litecoin">Litecoin </option>
							    		<option value="Dogecoin">Dogecoin </option>
							    	</select>
							</div>
						</div>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 email-field send_address">
							<div class="form-group">
							    	<label class="control-label" for="fromAddress">Send Address</label>
							    	<input type="text" class="form-control fromAddress" id="fromAddress" name="fromAddress" required  value="Ethereum"/>
							</div>
							<div class="naoTooltip-wrap">
								<span class="fa fa-question" aria-hidden="true"></span>
								<div class="naoTooltip nt-bottom nt-small">
									Add address from where you will send us the <span class="tooltip_type">Bitcoin</span>
								</div>
							</div>

						</div>
						<div class="botm_buttons">
							<button type="submit" name="continue" class="submit_form">Continue</button>
							<button type="button" name="back" class="back">Back</button>
						</div>
					</form>
				</div>
				<div id="token_step3" class="token_step3 token_tabs">
					<h3><span>Step 3</span> Thanks and Check Email</h3>
					<p>Please send <span class="title">ether</span> at this address : <b><span class="address" id="p1"> </span> <span onclick="copyToClipboard('#p1')" class="clip"><i class="fa fa-clipboard" aria-hidden="true"></i>
				</span></b>
					 </p>
					<p class="ethereum">
					<b style="color:red;">Note :</b> Send atleast <b> 80000 to 100000  </b> gas during the transfer of ether from your account
					Any excessive gas pending will be refunded to your wallet/account.</p>

					<h4 style="margin-top:25px !important;"><center>Scan this QR code for faster access to the Address. </center></h4>
					<center><span class="coin-image"></span></center>
				</div>
			</div>

			<div class="col-md-2 hidden-sm hidden-xs center_bar">
				<ul id="progressbar">
					<li class="active"><span>1</span></li>
					<li><span>2</span></li>
					<li><span>3</span></li>
				</ul>
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12 ryt_tokensteps">
				<ul>
					<li class="token_txt active">
						<h3>
							<span>01</span> Agree to the Terms and Conditions
						</h3>
						<p>You must read and agree to the entire Terms and Conditions, before you can you complete registration process. Once you have read them, you must check the boxes to accept the Terms and Conditions.</p>
					</li>
					<li class="token_txt">
						<h3>
							<span>02</span> Enter Your Details
						</h3>
						<p>You must provide an Email Address to be used to communicate important notices to you.</p>
					</li>
					<li class="token_txt">
						<h3>
							<span>03</span> Thanks & Check Email
						</h3>
						<p>One of our representative will get in touch with you.</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog cust">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <h3 class="apd_msg"></h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

@stop


@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
<script type="text/javascript" src="{{asset('public/js/jquery.validate.min.js') }}"></script>
<script src="{{asset('public/js/naoTooltips.js') }}"></script>
<!-- Token-Steps script -->
<script type="text/javascript">
	$(document).ready(function(){
		$('.continue').click(function(){
			//var check1=$('.check1').val();
			if ($('#check1').prop('checked')==false || $('#check2').prop('checked')==false || $('#check3').prop('checked')==false || $('#check4').prop('checked')==false){

				$('.apd_msg').text('You have to check all "checkboxes" to continue');
				$('#myModal').modal('show');
				return false;
			}

			if ($('.active').next('.token_tabs').length) {
				$('.active').removeClass('active').next('.token_tabs').addClass('active');
			}
		});
		$('.back').click(function(){
			if ($('.active').prev('.token_tabs').length) {
				$('.active').removeClass('active').prev('.token_tabs').addClass('active');
			}
		});
	});

</script>
<script>
$(document).ready(function () {
	    $("#ContForm").validate({
	    	ignore: [],
	    	messages:{
	    		address:"Please enter 42 characters long address."
	    	},
	        submitHandler: function(form) {
	        	$('#overlay').show();
	        	$.ajax({

			        type: 'post',
			        url: '{{url('step_new_form')}}',
			        data: $('#ContForm').serialize(),
			        success: function (data) {
			         if(data.status=='success'){
			         	$('#overlay').hide();
			               $('.token_tabs').hide();
			               $('.address').text(data.address);
			               $('.coin-image').html(data.img);
			               if(data.type=='Ethereum'){
			               	$('.ethereum').show();
			               }else{
			               	 	$('.ethereum').hide();
			               }
			               $('.title').text(data.type);
			               $('#token_step3').show();
			            }else if(data.status=='ban'){
			            	
			            }else{
			            	$('#overlay').hide();
			            	$('.apd_msg').text(data.msg);
			            	$('#myModal').modal('show');
			            }
	       			 }
 				});
	        }
	    });
	});

 	$('#check1').change(function() {
        if ($(this).prop('checked')) {
            $('#checkbox1').val('1');
        }
        else {
            $('#checkbox1').val('0');
        }
    });

    $('#check2').change(function() {
        if ($(this).prop('checked')) {
            $('#checkbox2').val('1');
        }
        else {
            $('#checkbox2').val('0');
        }
    });

    $('#check3').change(function() {
        if ($(this).prop('checked')) {
            $('#checkbox3').val('1');
        }
        else {
            $('#checkbox3').val('0');
        }
    });

    $('#check4').change(function() {
        if ($(this).prop('checked')) {
            $('#checkbox4').val('1');
        }
        else {
            $('#checkbox4').val('0');
        }
    });

function copyToClipboard(element) {
  var $temp = $("<input>");
  $("body").append($temp);
  $temp.val($(element).text()).select();
  document.execCommand("copy");
  $temp.remove();
}

$('.currency_type').change(function(){
var cur = $(this).val();
$('.tooltip_type').text(cur);
//alert(cur);
	if(cur=='Ethereum'){
		$('.send_address').hide();
		$('.fromAddress').val($('.eth_address').val());
	}else{
		$('.send_address').show();
		 $("#ContForm").validate();
		 $('.fromAddress').val('');
	
	}
});





</script>
<script>
	(function() {
		$('.naoTooltip-wrap').naoTooltip();
	})();
</script>
@stop
   