@extends('frontend.layouts.pages')

@section('title', 'Mission')

@section('content')
<div class="container-fluid whitepaper">

	<div class="container">

		<div class="row">
			<div class="mission">
			 	{!! $content['description'] !!}
			 </div> 
		</div>

	</div>

</div>
@stop


@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
@stop

@section('page_level_js_top')
<script type="text/javascript">
	$(window).scroll(function(){
		var sticky = $('.header-cont'),
		scroll = $(window).scrollTop();
		if($(window).width() > 767)
		{
			if (scroll >= 10)
			{
				sticky.addClass('fixed');
				$('.header-cont .row').addClass('fix-header-row');
			}
			else
			{
				sticky.removeClass('fixed');
			}
		}		  
	});
</script>
@stop