@extends('frontend.layouts.pages')

@section('title', 'Terms & Conditions')

@section('content')
<div class="container-fluid term_cond">
	<div class="container terms_page">
		<div class="row">
			<div class="step_heading">
			 	<h1>Terms of Sale</h1>			 	
			 </div> 
		</div>
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12 terms_cont">
				{!! $content['description'] !!}
			</div>
		</div>
	</div>

</div>
@stop


@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
@stop

@section('page_level_js_top')
<script type="text/javascript">
	$(window).scroll(function(){
		var sticky = $('.header-cont'),
		scroll = $(window).scrollTop();
		if($(window).width() > 767)
		{
			if (scroll >= 10)
			{
				sticky.addClass('fixed');
				$('.header-cont .row').addClass('fix-header-row');
			}
			else
			{
				sticky.removeClass('fixed');
			}
		}		  
	});
</script>
@stop