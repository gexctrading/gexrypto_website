@extends('frontend.layouts.pages')

@section('title', 'Contact')

@section('content')
<style>
#overlay{
     position: fixed;
     z-index: 99999;
     top: 0;
     left: 0;
     bottom: 0;
     right: 0;
     text-align: center;
     display: none;
     background: rgba(0,0,0,0.8);
     transition: 1s 0.4s;
     -webkit-transition: 1s 0.4s;
     -moz-transition: 1s 0.4s;
     -ms-transition: 1s 0.4s;
    }
    .load_img{
     position: absolute;
     top: 50%; 
     transform: translate(-50%, -50%); 
     left: 50%;
     width: 6%; 
     z-index: 999;
    }
    .suc_msg{
        display: none;
    }
    .col-md-12.text-center.top-tags-ct > p {
      font-size: 16px;
    }
</style>
<div id="overlay">
 <img class="load_img" src="{{asset('public/frontend/images/loading.gif')}}" >
</div>
<div class="container-fluid contact-ct">
    <div class="container">
    <div class="row">
        <div class="col-md-12 text-center top-tags-ct">
            <h1>{{$content['title']}}</h1>
            <p>{{$content['heading']}}</p>
        </div>
    </div>
    <div class="row ct-block">
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="ct-left-about">
                <h3>About Us</h3>
                <p>{{$content['about_us']}}</p>
                <h3>Contact Details</h3>
               {!! $content['contact_detail'] !!}
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12 ct-right-bg">
            <div class="ct-right-contact">
                <h3>Send Us a Message</h3>
               
                <div class="alert alert-success suc_msg" role="alert" >
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <span>Message sent successfully!</span>
                </div>
               
                <form method="post" action="{{url('/contact')}}" id="contact-form">
                 {{ csrf_field() }}
                    <div class="form-group">
                        <input type="text" class="form-control" id="fname" placeholder = "Enter your name *" name = "fname">
                    </div>
                    <div class="form-group">
                         <input type="email" class="form-control" id="email" placeholder = "Enter your email *" name = "email">
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" id="pwd" placeholder = "Phone Number (Optional)" name = "contact" >
                    </div>
                      <div class="form-group">
                       <textarea class="form-control" placeholder = "Your message *" name = "messages" rows = "1" required></textarea>
                    </div>
                    <button type="submit" class="btn btn-default" id = "sve-chngs">Send</button>
                </form>
            </div>
        </div>
    </div>
        
    </div>
</div>
    <script>
        $('#sve-chngs').on('click',function() {
            $("#contact-form").validate({
                rules: {
                    fname: "required",
                    email: "required",
                    message : "required"
                },
                messages: {
                  //  fname: "Required field!",
                 //   email: "Required field!",
                  //  message: "Required field"
                },
                 
                submitHandler: function(form){
                    $('#overlay').show();
                    $.ajax({

                        type: 'post',
                        url: '{{url('contact')}}',
                        data: $('#contact-form').serialize(),
                        success: function (data) {
                         if(data.status=='success'){
                            $('#overlay').hide();
                            $('.suc_msg').show();
                            $('.form-control').val('');
                            $('.form-control').text('');
                             
                            }else{
                               
                                $('#overlay').hide();
                                
                            }
                        }
                 });
                }
            });
        });


    </script> 
@stop