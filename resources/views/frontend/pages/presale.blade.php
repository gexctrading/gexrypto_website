@extends('frontend.layouts.pages')

@section('title', 'Pre-Sales')

@section('content')
<div class="container-fluid presale">
	<div class="container container-sm">
		<ul class="register">
			<li>
				<a href="{{url('/private-investors')}}" class="blue-btn"> Private Contributors </a>
			</li>
			<li>
				<a href="{{url('/individual')}}" class="blue-btn"> Individual </a>
			</li>
			<li>
				<a href="{{url('/airdrop')}}" class="blue-btn"> Airdrop </a>
			</li>
		</ul>
	</div>
	<div class="container">
		<div class="row">
			<div class="gexclogo">
				<h1> 
					<a href="javascript:void(0);"> <img src="{{ asset('public/frontend/images/lg1.png') }}"></a>
				</h1>
				<ul class="ulinfos">
					<li>
						<a href="mailto:gexc@gexcrypto.io">  gexc@gexcrypto.io </a>	
					</li>					
				</ul>
			 
				<h3> Thank you all for the support! </h3>
				<div class="faviconcenter">
					<img src="{{ asset('public/frontend/images/ic_n1.png') }}">
				</div>
				<div class="counterstart">					
 					<div id="countdown">
 						<span class="days"> <label>0</label> <b>Days</b></span> 
 						<span class="hours"><label>12</label> <b>Hours</b></span>
 						 <span class="minutes"><label>10</label> <b>Minutes</b></span> 
 						 <span class="seconds"><label>19<b>Seconds</b></span>
 					</div><!-- /#Countdown Div -->	
				</div>
			</div>
		</div>
	</div>
</div>
@stop


@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
<script src="{{ asset('public/js/moment.js') }}"></script>
<script src="https://momentjs.com/downloads/moment-timezone-with-data-2012-2022.js"></script>

<script>

/*var target_date = new Date('Nov, 12, 2017 00:31:00').getTime();


var days, hours, minutes, seconds;


var countdown = document.getElementById('countdown');

setInterval(function () {


    var current_date = new Date().getTime();
    var seconds_left = (target_date - current_date) / 1000;

   
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;

    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;

    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

 
    countdown.innerHTML = '<span class="days">' + days +  ' <b>Days</b></span> <span class="hours">' + hours + ' <b>Hours</b></span> <span class="minutes">'
    + minutes + ' <b>Minutes</b></span> <span class="seconds">' + seconds + ' <b>Seconds</b></span>';  

}, 1000);*/
//# sourceURL=pen.js
</script>

<script>

// set the date we're counting down to
// /var target_date = new Date('Nov, 11, 2017 11:00:00').getTime();

 var target_date=moment.tz("12/10/2017 11:59 pm", "M/D/YYYY h:mm a", true, "America/Los_Angeles");

//alert(target_date);
//alert(target_date.toUTCString());

// variables for time units
var days, hours, minutes, seconds;

// get tag element
var countdown = document.getElementById('countdown');

// update the tag with id "countdown" every 1 second
setInterval(function () {

    // find the amount of "seconds" between now and target
   // var current_date = new Date().getTime();

    var current_date= moment.tz('America/Los_Angeles').valueOf();
    //alert(moment.tz('America/Los_Angeles'));
    var seconds_left = (target_date - current_date) / 1000;

    // do some time calculations
    days = parseInt(seconds_left / 86400);
    seconds_left = seconds_left % 86400;

    hours = parseInt(seconds_left / 3600);
    seconds_left = seconds_left % 3600;

    minutes = parseInt(seconds_left / 60);
    seconds = parseInt(seconds_left % 60);

    if(parseInt(days)<=0 && parseInt(hours) <= 0 && parseInt(seconds) <=0){
    	target_date=moment.tz("12/25/2017 11:59 pm", "M/D/YYYY h:mm a", true, "America/Los_Angeles");
    	$('.counter_text').text('Public Token Sale Begins');
    	$('.purchase').hide();
    }


    // format countdown string + set tag value
    countdown.innerHTML = '<span class="days">' + days +  ' <b>Days</b></span> <span class="hours">' + hours + ' <b>Hours</b></span> <span class="minutes">'
    + minutes + ' <b>Minutes</b></span> <span class="seconds">' + seconds + ' <b>Seconds</b></span>';  

}, 1000);
//# sourceURL=pen.js
</script>
@stop