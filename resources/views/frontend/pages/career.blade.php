@extends('frontend.layouts.pages')

@section('title', 'Career')

@section('content')
    <div class="container-fluid blue_bg_top">
      <div class="container">
        <div class="row">
        @if (session('custom_insert_success'))
            <div class="alert alert-success" role="alert" id="msg">
              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
              <strong>Success ! </strong>
              <span>{{ Session::get('custom_insert_success') }}</span>
            </div>
        @endif
          @foreach($dynamicSection1 as $value)
            <h2><span class="white_line">{{$value->title}}</span></h2>
            <p>{{$value->description}}</p>
          @endforeach
        </div>
      </div>
    </div>
    <div class="container head_content">
      <div class="row">
         @foreach($dynamicSection2 as $value)
          <h2>{{$value->title}}</h2>
          <p>{{$value->description}}</p>
        @endforeach
      </div>
    </div>
    <div class="container-fluid career_bg">

      <div class="container">

        <ul class="nav-tabs nav developer" role="tablist">
              <?php 
                $i=1; 
              ?>
              @foreach($dynamicSection3 as $key => $value)
                <li role="presentation" class="col-md-4 col-sm-4 col-xs-12  <?php if($i == 1 || $i == 3 || $i == 4 || $i == 6 || $i == 7 || $i == 9){ echo "nav_list"; } ?>">
                <a href="#tab{{$value->position}}" data-toggle="tab" role="tab" >
                  <div id="<?= $value->description ?>"  class="career_tabs">
                    <div class="tabs_text">
                      <span></span>
                      <p>{{$value->title}}</p>
                    </div>
                    <div id="<?= $value->description ?>" class="tabs_black_bg">
                      <p>{{$value->description}}</p>
                    </div>
                  </div>
                </a>
              </li>
              <?php $i++; ?>
              @endforeach
        </ul>
      <!--   <div class="glassdoor">
          <h3>Over current and former employees have written reviews of their experience at RevInfotech.</h3>
          <h3>Learn what it's like to work here on <strong>Glassdoor</strong>.</h3>
          <a target="_blank" href="https://www.glassdoor.co.in/Overview/Working-at-Rev-infotech-EI_IE525789.11,23.htm"><img src="/public/img/glassdoor.png" alt=""></a>
        </div> -->
      </div>
      <div class="container-fluid career_info">
        <div id="cont" class="tab-content container-sm container">
        	
        </div>
      </div>
    </div>

    <div class="container-fluid dark_overlay">
      <h3>Excited to work with us...</h3>
      <h3>But don't see your positions listed</h3>
      <a href="/send-resume" class="btn blue_btn">Send Your Resume</a>
    </div>
  <input type="hidden" value="{{csrf_token()}}" id="token">
@endsection


@section('page_level_js')
  <script>
    $(document).ready(function(){

      $('#msg').fadeOut(10000);

      $(".nav-tabs a").click(function() {
        $('html, body').animate({scrollTop: $(".career_info").offset().top - 150}, 2500);
      });

    var token= $('#token').val();
        $.ajax({
            method:'Post',
            url:"{{ url('/getCarrerByAjax') }}",
            data:{name:'Designer',_token:token},
            success:function(resp)
            {
              $('#cont').html(resp);
            }

        });

        /*$('.tabs_black_bg').click(function(){
          var name=$(this).attr('id');
          var token= $('#token').val();
          $.ajax({
              method:'Post',
              url:"{{ url('/getCarrerByAjax') }}",
              data:{name:name,_token:token},
              success:function(resp)
              {
                $('#cont').html(resp);
              }
          });
        });*/

         $('.career_tabs').click(function(){
          var name=$(this).attr('id');
          var token= $('#token').val();
          $.ajax({
              method:'Post',
              url:"{{ url('/getCarrerByAjax') }}",
              data:{name:name,_token:token},
              success:function(resp)
              {
                $('#cont').html(resp);
              }
          });
        });

    });

    jQuery('.developer li:first').addClass('active');

  </script>
@endsection