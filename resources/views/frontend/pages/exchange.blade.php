@extends('frontend.layouts.index')

@section('title', 'Exchange')
@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('public/frontend/css/sky-carousel.css') }}">
<div class="exchange_banner">
	<img src="{{asset('public/frontend/images/coin_banner.jpg')}}">
	<div class="exch_banr_text">
		<div class="container">
			<div class="row">
				<div class="exch_left_banner">
					<h1>Cryptocurrency Exchange</h1>
					<h2>Start the 2.0 Age of Cryptocurrency Exchange</h2>
					<!-- <div class="login_btn">
						<a href="#" class="btn blue_btn">Login</a>
						<a href="#" class="btn yel_btn">Register</a>
					</div> -->
				</div>
				<div class="exch_ryt_banner">
					<img class="img-responsive center-block" src="{{asset('public/frontend/images/coins.png')}}">
				</div>
			</div>
		</div>
	</div>
</div>

<div class="three_pair_sec">
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-4 col-sm-4 col-xs-12 three_pair_blk">
				<img class="img-responsive" src="{{asset('public/frontend/images/blu_icon.png')}}">
				<h4>First Winner of Gexcrypto API Competition</h4>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 three_pair_blk">
				<img class="img-responsive" src="{{asset('public/frontend/images/blu_icon.png')}}">
				<h4>Gexcrypto Adds DNT/BTC Trading Pair</h4>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-12 three_pair_blk">
				<img class="img-responsive" src="{{asset('public/frontend/images/blu_icon.png')}}">
				<h4>Gexcrypto Coin Burn in 2017 Fall</h4>
			</div>
		</div>
	</div>
</div>

<div class="exchange_two_tabs">
	<div class="container container-sm">
		<div class="row">
			<div class="exchange_tab_cont">
				<h4>We're working of our exchange. Catch a glimpse of our GexCrypto Exchange</h4>
			</div>
			<ul class="nav nav-tabs">
				<li class="active">
					<a href="#desktop" data-toggle="tab">Desktop</a>
				</li>
				<!-- <li>
					<a href="#tablet" data-toggle="tab">Tablet</a>
				</li> -->
				<li>
					<a href="#mobile" data-toggle="tab">Mobile</a>
				</li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active in" id="desktop">

					<!-- Sky carousel -->

					<!-- <div class="sky-carousel sc-no-select">
						<div class="sky-carousel-wrapper">
							<ul class="sky-carousel-container">
								<li>
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li>
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="sc-selected">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
								<li class="">
									<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" alt="" class="sc-image">
								</li>
							</ul>
						</div>
						<div class="sc-preloader"></div>

						<canvas class="sc-overlay sc-overlay-left" width="190" height="1"></canvas>
						<canvas class="sc-overlay sc-overlay-right" width="190" height="1"></canvas>
						<a href="#" class="sc-nav-button sc-prev sc-no-select"></a>
						<a href="#" class="sc-nav-button sc-next sc-no-select"></a>
					</div> -->

					<!-- Sky carousel ends -->

					<ul>
						<li>
							<a href="#">
								<img src="{{asset('public/frontend/images/desktop_slide.jpg')}}" class="img-responsive center-block">
							</a>
						</li>
					</ul>
				</div>

				<div class="tab-pane" id="tablet">
					<ul>
						<li>
							<a href="#">
								<img src="{{asset('public/frontend/images/tab_slide.jpg')}}" class="img-responsive center-block">
							</a>
						</li>
					</ul>
				</div>

				<div class="tab-pane" id="mobile">
					<ul>
						<li>
							<a href="#">
								<img src="{{asset('public/frontend/images/mobile_slide.jpg')}}" class="img-responsive center-block">
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- <div class="exchange_two_blocks">
	<div class="container container-sm">
		<div class="row">
			<div class="col-md-7 col-sm-12 col-xs-12 exchange_blue_sec">
				<div class="exch_blue_head">
					<h2>Trade over 700 instruments</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<h3 class="yellow_hd">Balance</h3>
				</div>
				
				<div class="table-responsive">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>
									<h4>Deposit</h4>
								</th>
								<th>
									<h4>Withdraw</h4>
								</th>
								<th>
									<h4>Transfer</h4>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Lorem Ipsum</td>
								<td>22.33</td>
								<td>63.63</td>
							</tr>
							<tr>
								<td>Lorem Ipsum</td>
								<td>22.33</td>
								<td>63.63</td>
							</tr>
							<tr>
								<td>Lorem Ipsum</td>
								<td>22.33</td>
								<td>63.63</td>
							</tr>
							<tr>
								<td>Lorem Ipsum</td>
								<td>22.33</td>
								<td>63.63</td>
							</tr>
							<tr>
								<td>Lorem Ipsum</td>
								<td>22.33</td>
								<td>63.63</td>
							</tr>
							<tr>
								<td>Lorem Ipsum</td>
								<td>22.33</td>
								<td>63.63</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12 exchange_ryt_sec">
				<h2>New Order</h2>
				<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text.</p>
				<div class="exch_outr_shadow">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#buy" data-toggle="tab">Buy</a>
						</li>
						<li>
							<a href="#sell" data-toggle="tab">Sell</a>
						</li>
					</ul>
					<div class="tab-content">
      					<div class="tab-pane active in" id="buy">
      						<form>
      							<div class="form-group">
      								<label>Kick</label>
      								<input type="text" placeholder="Amount to buy" class="form-control">
      							</div>
      							<div class="form-group">
      								<label>Kick/Eth</label>
      								<input type="text" placeholder="Price" class="form-control">
      							</div>
      							<div class="form-group">
      								<label>Eth</label>
      								<input type="text" placeholder="Total" class="form-control">
      							</div>
      							<div class="form-group">
      								<label>Expires</label>
      								<input type="text" placeholder="1000" class="form-control">
      							</div>
      							<div class="form-group">
      								<button class="btn">Buy</button>
      							</div>
      						</form>
      					</div>
      					<div class="tab-pane" id="sell">
      						<form>
      							<div class="form-group">
      								<label>Kick</label>
      								<input type="text" placeholder="Amount to sell" class="form-control">
      							</div>
      							<div class="form-group">
      								<label>Kick/Eth</label>
      								<input type="text" placeholder="Price" class="form-control">
      							</div>
      							<div class="form-group">
      								<label>Eth</label>
      								<input type="text" placeholder="Total" class="form-control">
      							</div>
      							<div class="form-group">
      								<label>Expires</label>
      								<input type="text" placeholder="1000" class="form-control">
      							</div>
      							<div class="form-group">
      								<button class="btn">Sell</button>
      							</div>
      						</form>
      					</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7 col-sm-12 col-xs-12 prc_chart_sec">
				<h3 class="yellow_hd">Price Chart</h3>
				<div class="chartContainer" style="height: 330px; width: 100%;"></div>
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12 exchange_order">
				<div class="exch_outr_shadow">
					<h2>Order Book</h2>
					<div class="table-responsive">
						<table class="table">
							<tbody>
								<tr>
									<td>0.020023051010</td>
									<td>450.00</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>0.020023051010</td>
									<td>450.00</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>0.020023051010</td>
									<td>450.00</td>
									<td>3.121</td>
								</tr>

								<tr>
									<td>0.020023051010</td>
									<td>450.00</td>
									<td>3.121</td>
								</tr><tr>
									<td>0.020023051010</td>
									<td>450.00</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>0.020023051010</td>
									<td>450.00</td>
									<td>3.121</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 botm_three_tables">
				<div class="exch_outr_shadow">
					<h2>BTC Markets</h2>
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Pair</th>
									<th>Last Price</th>
									<th>Change</th>
									<th>Volume</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 botm_three_tables">
				<div class="exch_outr_shadow">
					<h2>ETH Markets</h2>
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Pair</th>
									<th>Last Price</th>
									<th>Change</th>
									<th>Volume</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 botm_three_tables">
				<div class="exch_outr_shadow">
					<h2>USDT Markets</h2>
					<div class="table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th>Pair</th>
									<th>Last Price</th>
									<th>Change</th>
									<th>Volume</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
								<tr>
									<td>WTC/BTC</td>
									<td>0.00108510 / <span>$6.04</span></td>
									<td>-5.20%</td>
									<td>3.121</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->
@stop


@section('page_level_js')
<script src="{{ asset('resources/assets/frontend/js/home/index.js') }}"></script>
<script src="{{ asset('public/frontend/js/jquery.canvasjs.min.js') }}"></script>

@stop

@section('page_level_js_top')
<!-- <script src="{{ asset('resources/frontend/js/jquery_002.js') }}"></script>
<script src="{{ asset('public/frontend/js/sky-carousel-jquery.js') }}"></script>
<script src="{{ asset('public/frontend/js/SkyPlugins.json') }}"></script>

<script type="text/javascript">
	$(function() {
		$('.sky-carousel').carousel({
			itemWidth: 170,
			itemHeight: 240,
			distance: 15,
			selectedItemDistance: 50,
			selectedItemZoomFactor: 1,
			unselectedItemZoomFactor: 0.67,
			unselectedItemAlpha: 0.6,
			motionStartDistance: 170,
			topMargin: 119,
			gradientStartPoint: 0.35,
			gradientOverlayColor: "#f5f5f5",
			gradientOverlaySize: 190,
			reflectionDistance: 1,
			reflectionAlpha: 0.35,
			reflectionVisible: true,
			reflectionSize: 70,
			selectByClick: true
		});
	});
</script> -->
<script>
	$(function() {
		$(".chartContainer").CanvasJSChart({
			title: false,
			axisY: {
				title: false,
				includeZero: false,
			},
			axisX: {
				interval: 1
			},
			data: [
			{
				type: "line",
				toolTipContent: "{label}: {y} mm",
				dataPoints: [
					{ label: "", y: 5.28 },
					{ label: "", y: 3.83 },
					{ label: "", y: 6.55 },
					{ label: "", y: 4.81 },
					{ label: "", y: 2.37 },
					{ label: "", y: 2.33 },
					{ label: "", y: 3.06 },
					{ label: "", y: 2.94 },
					{ label: "", y: 5.41 },
					{ label: "", y: 2.17 },
					{ label: "", y: 2.17 },
					{ label: "", y: 2.80 }
				]
			}
			]
		});
	});
</script>
<script>
	$(document).ready(function(){
		var width = $( window ).width();
		var height = $(window).height();
		if (width < 1024) {
			$(".exchange_banner > img").css("height", height);
		}
	});
</script>
<script type="text/javascript">
	$(window).scroll(function(){
		var sticky = $('.header-cont'),
		scroll = $(window).scrollTop();
		if($(window).width() > 767)
		{
			if (scroll >= 10)
			{
				sticky.addClass('fixed');
				$('.header-cont .row').addClass('fix-header-row');
			}
			else
			{
				sticky.removeClass('fixed');
			}
		}		  
	});
</script>
@stop