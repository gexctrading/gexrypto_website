$('.carousel').carousel({
	interval: false
})

$('.carousel .item').each(function(){
	var itemToClone = $(this);
	for (var i=1;i<2;i++) {
		itemToClone = itemToClone.next();
      		// wrap around if at end of item collection
      		if (!itemToClone.length) {
      			itemToClone = $(this).siblings(':first');
      		}
     		 // grab item, clone, add marker class, add to collection
      		itemToClone.children(':first-child').clone()
      		.addClass("cloneditem-"+(i))
      		.appendTo($(this));
  	}
});

$(".yellow_arrow img").click(function() {
	$('html, body').animate({
		scrollTop: $(".about-gexc").offset().top-150
	}, 700);
});

$(".contact-btn").click(function(){
	$('html, body').animate({
		scrollTop: $(".contact-outr").offset().top-100
	}, 900);
});

jQuery(function(){
	var Service = window.location.hash;
	if(Service)
	{
		jQuery('html,body').animate({
			scrollTop: jQuery("#contact_form").offset().top-200
		}, 900);
	}
});

