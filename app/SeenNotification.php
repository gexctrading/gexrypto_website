<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeenNotification extends Model
{
    protected $table ='seen_notifications';
    protected $guarded=[];
}
