<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogPost extends Model
{
    protected $table ='blog_posts';
    protected $fillable=[];
    protected $guarded=[];

    public function comment(){
    	return $this->hasMany('App\BlogComment','post_id','id')->where('status',1);
    }

}
