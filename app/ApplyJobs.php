<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplyJobs extends Model
{
   	protected $table = 'apply_jobs';
   //	protected $fillable = ['title','category','description','image'];

   	public function Careers()
    {
        return $this->hasOne('App\Careers','id','job_id');
    }
}
