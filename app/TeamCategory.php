<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamCategory extends Model
{
    protected $table ='team_categories';
    protected $fillable=[];
    protected $guarded=[];

    public function members(){
    	return $this->hasMany('App\TeamMember','category','id');
    }

}
