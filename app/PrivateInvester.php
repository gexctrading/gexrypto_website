<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrivateInvester extends Model
{
    protected $table ='private_investers';
     protected $guarded=[];
}
