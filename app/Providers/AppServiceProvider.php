<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DB;
use Redirect;
use View;
use App\Cms;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $header=Cms::where('slug','header_menu')->first();
        $header=json_decode(json_encode($header),true);
       $footr=Cms::where('slug','footer')->get();
       $footr=json_decode(json_encode($footr),true);
       $footer=array();
       
       foreach($footr as $key=>$foot){
        $footer[$foot['title']]=$foot['description'];
       }
        $contents=CMS::where('slug','roadmap')->get()->toArray();  
        $roadmap=array();
        foreach($contents as $key=>$cont){
            $roadmap[$cont['title']]=$cont['description'];
        } 
       View::share(['footer'=>$footer,'header'=>$header,'roadmap'=>$roadmap]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
