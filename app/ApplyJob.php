<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApplyJob extends Model
{
    protected $table ='apply_jobs';

 	public function Career()
    {
        return $this->hasOne('App\Career','id','job_id');
    }
}
