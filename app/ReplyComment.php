<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReplyComment extends Model
{
    protected $table ='reply_comments';
    protected $fillable=[];
    protected $guarded=[];
}
