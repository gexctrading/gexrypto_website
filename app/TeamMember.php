<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TeamMember extends Model
{
    protected $table ='team_members';
    protected $fillable=[];
    protected $guarded=[];

    public function team_category(){
    	return $this->hasOne('App\TeamCategory','id','category');
    }
}
