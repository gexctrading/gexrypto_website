<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\frontend\BaseController;
use App\Cms;
use App\TeamMember;
use App\BlogPost;
use App\Career;
use App\ApplyJob;
use Illuminate\Support\Facades\Input;
use Session;
use Mail;

class HomeController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
	}

		public function index()
		{
			$content=CMS::where('slug','home')->get()->toArray();
			$data=array();
			foreach($content as $key=>$cont){
					$data[$cont['title']]=$cont['description'];
			}

			$members = TeamMember::where('show_on_home_page',1)->get()->toArray();  
			$posts = BlogPost::where('status',1)->orderBy('posted_date','desc')->get();
			$posts = json_decode(json_encode($posts),true);

			$crypto = BlogPost::where('status',1)->where('category',1)->orderBy('posted_date','desc')->get();
			$crypto = json_decode(json_encode($crypto),true);
			$lat_posts = BlogPost::where('status',1)->where('category',2)->orderBy('posted_date','desc')->get();
			$lat_posts = json_decode(json_encode($lat_posts),true);


				//echo '<pre>'; print_r($posts); die;
			return view('frontend.home.index')->with(['data'=>$data,'members'=>$members,'posts'=>$posts,'lat_posts'=>$lat_posts,'crypto'=>$crypto]);
				
		}

		public function sendContactUsMail(Request $request)
		{
			$to      = 'gexc@gexcrypto.com';
			$name    = $request->name;
			$email   = $request->email;
			$subject = 'Confirmation Mail';
			$message = $request->message;
			$header  = "From: $name <$email>";

			// Send it
			$submitted = mail($to, $subject, $message, $header);
			if($submitted)
			{
				echo 1;
			} 
			else
			{
				echo 0;
			}
		}

		public function getCarrerByAjax(Request $request)
		{
				$all=Career::where('category',$request->name)->get();
				$vac=0;
				$div="";
				$opn="";
				foreach($all as $openings)
				{
					$vac+=$openings->vacancy;
					$opn.=
					   '<div class="panel panel-default ">
							<div id="heading1" class="panel-heading" role="tab">
								<h4 class="panel-title">
									<a href="#panel1" role="button" data-toggle="collapse" data-parent="#accordion0" aria-expanded="true" aria-controls="panel1">Position: '.$openings->position.'</a>
								</h4>
							</div>
							<div id="panel1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
								<div class="panel-body">
									<p>'.@($openings->description).'</p>
									<p><strong>Experience :</strong> '.$openings->experience.' years</p>
									<p><strong>Location :</strong> '. $openings->location .'</p>
									<strong>Required Skills :</strong>
									'.@($openings->skills).'
									<strong>Responsibilities :</strong>
									'.@($openings->responsibility).'
									
									<a href="/applyJob/'.base64_encode(convert_uuencode($openings->id)).'"><button class="btn blue_btn">Click Here To Apply</button></a> 
									<b>&nbsp;&nbsp;OR&nbsp;&nbsp;</b>
									<a href="javascript::void(0)">
										<img src="/public/frontend/images/Apply-with-linkedin.png">
								 	</a>
								</div>
							</div>
						</div>';
				}
				$div.=
				'<div class="tab-pane active" role="tabpanel">
					<p>We have '. $vac .' openings for this category</p>
					<div id="accordion0" class="panel-group career_accordion" role="tablist" aria-multiselectable="true">

						'.$opn.'

					</div>
				</div>';
					// <a href="/linkedin/auth.php?login=linkedin&job_id='.base64_encode(convert_uuencode($openings->id)).'"> 
					echo $div;die;

		}

		public function applyJob(Request $request,$id=null)
		{
			//echo "<pre>";print_r($request->input());die;
				if(!empty($request->all())){
				 
					$this->validate($request,[
						'first_name' => 'required',
						'last_name' => 'required',
						'email' => 'required',
						// 'qualification' => 'required',
						'location' => 'required',
						'contact' =>'required',
						'experience' => 'required',
					 
						]);
						
						$id=convert_uudecode(base64_decode($request->job_id));
						$request['job_id']=$id;
						$request['date']=date('Y-m-d');
					 // echo "<pre>"; print_r($request->all());die;
						if(Input::file()){
							$file = Input::file('file');
							$filename  = time() . '.' . $file->getClientOriginalExtension();
							$path = public_path('assets/resume/' . $filename);
							move_uploaded_file($_FILES["file"]["tmp_name"], $path);
							$request['resume']=$filename;
							$request['file']="";
							ApplyJob::insert($request->all());
							return redirect('/career')->with('custom_insert_success','You have successfully applied for this job');
						}
						else
						{
							if((!preg_match('/^[a-z A-Z]+$/', $request->first_name)) || (!preg_match('/^[a-z A-Z]+$/', $request->last_name)) || (!preg_match('/^[0-9]+$/', $request->contact))){
								return redirect::back()->with('valid', 'Enter Valid Data.');
							}
							else{
							 	$data = $request->input();

								Mail::send('emails.job_apply', $data, function($message) use ($data,$request) {
				                    $message->to('parul@revinfotech.com');
				                    $message->subject('New Job Application');
				                    // $message->from('info@revinfotech.com','Revinfotech');
				                    //$message->message($data['message']);
				                });

				                Mail::send('emails.applicant_job_apply', $data, function($message) use ($data,$request) {
				                    $message->to($data['email']);
				                    $message->subject('Job Application');
				                    // $message->from('info@revinfotech.com','Revinfotech');
				                    //$message->message($data['message']);
				                });

								ApplyJob::insert($request->all());
								return redirect('/career')->with('custom_insert_success','You have successfully applied for this job');
							}
							
						}
						return redirect('/career');
				}

				else{
					 // $jobId=$request->id;
						$id=convert_uudecode(base64_decode($request->id));
						$get=Career::where('id',$id)->get()->first();
						$position=$get->position; 
						return view('frontend.pages.apply_job')->with('position',$position)->with('jobs_id',$id);
				}
		}

		public function sendResume(Request $request)
		{
			$all=Career::select('description')->where('type','section3')->get();
			return view('frontend.pages.sendResume')->with('allPositions',$all);
		}

		public function applylinkedin()
		{
				return view('frontend.pages.apply_linkedin');
				
		}
		public function saveLinkedInData(Request $request)
		{
		//	 $job_id=$_SESSION['Jid'];
			$job_id =session('Jid');
			$array['email']=$request->email;
			$array['job_id']=$job_id;
			$array['first_name']=$request->first_name;
			$array['last_name']=$request->last_name;
			$array['date']=date('Y-m-d');
			$array['position']='Linkedin';
			ApplyJob::insert(array($array));
		//	 session_destroy();
			 
		 	$array=ApplyJob::where('email',$request->email)->limit(1)->get();
			 
		 	return view('frontend.pages.apply_linkedin')->with('array',$array);
		}

		public function editlinkedin (Request $request) 
		{

			$id=$request->session()->get('linkedin_id');
			
			$data=ApplyJob::where('email', $request->input('email'))->update(['first_name' => $request->input('first_name'),'last_name' => $request->input('last_name'),'email' => $request->input('email'),'contact' => $request->input('contact'),'location' => $request->input('location'),'experience' => $request->input('experience') ]);
					
			return redirect('/career')->with('custom_insert_success','You have successfully applied for this job');
		}
}
