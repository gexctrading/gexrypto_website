<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use View;

use App\SiteView;

class BaseController extends Controller
{
    public function __construct()
    {
    	parent::__construct();

    	// Updating Site View Count
    	$ip = $_SERVER['REMOTE_ADDR'];
    	$count = SiteView::where('ip', $ip)->count();
    	if( !$count )
    	{
    		$siteView = new SiteView;
    		$siteView->ip = $ip;
    		$siteView->save();
    	}

    	$viewCount = SiteView::count();
    	View::share('viewCount', $viewCount);
    }
}
