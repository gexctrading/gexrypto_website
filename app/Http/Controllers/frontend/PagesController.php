<?php

namespace App\Http\Controllers\frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\frontend\BaseController;
use App\Faq;
use App\TeamMember;
use App\TeamCategory;
use App\Cms;
use DB;
use Mail;
use App\Subscriber;
use App\User;
use App\Contact;
use App\PowerToken;
use App\PrivateInvester;
use Session;
use App\SeenNotification;
use View;
use App\SubscribeToken;
use App\Newsletter;
use App\BlogPost;
use App\BlogComment;
use Unirest;
class PagesController extends BaseController
{
	function __construct()
	{
		parent::__construct();
	}

	public function roadmap()
	{
		$contents=CMS::where('slug','roadmap')->get()->toArray();
        $content=array();
        foreach($contents as $key=>$cont){
            $content[$cont['title']]=$cont['description'];
        }
        //echo '<pre>'; print_r($content); die;
		return view('frontend.pages.roadmap')->with('content',$content);
	}

	public function mission()
	{
		$content = Cms::where('slug','mission')->first()->toArray();
		return view('frontend.pages.mission')->with('content',$content);
	}

	public function team()
	{
		$members = TeamMember::with(['team_category'])->where('status',1)->get()->toArray();
		//echo '<pre>'; print_r($members); die;

		$members = TeamCategory::with(['members'=>function($query){ $query->where('status',1);}])->where('status',1)->get()->toArray();

		$content = Cms::where('slug','team')->first()->toArray();
		return view('frontend.pages.team')->with('members',$members)->with('content',$content);
	}

	public function teams()
	{
		$members = TeamCategory::with(['members'=>function($query){ $query->where('status',1);}])->where('status',1)->get()->toArray();
		//echo '<pre>'; print_r($members); die;
		$content = Cms::where('slug','team')->first()->toArray();
		return view('frontend.pages.teams')->with('members',$members)->with('content',$content);
	}

	public function presale()
	{
		return view('frontend.pages.presale');
	}

	public function faq()
	{
		$list	 = FAQ::where('status',1)->get()->toArray();
		$content = Cms::where('slug','faq')->first()->toArray();
		return view('frontend.pages.faq')->with('list',$list)->with('content',$content);
	}
	public function steps()
	{
		return view('frontend.pages.steps');
	}
	public function private_invester(){

		return view('frontend.pages.private_invester');
	}
	public function airdrop(){

		return view('frontend.pages.airdrop');
	}
	public function stepsSubmit(Request $request){
		$data=$request->input();
		if(!empty($data)){
			if($data['name']=='' || $data['email']=='' || $data['cemail']==''){

				return $resposne=array('status'=>'fail','msg'=>'Please fill all fields');
			}
			if($data['email']!=$data['cemail']){
				return $resposne=array('status'=>'fail','msg'=>'Confirmation email should be same');

			}else{

				$exist = PowerToken::where('email',$data['email'])->first();
				if(!empty($exist)){
					return $resposne=array('status'=>'fail','msg'=>'Email already exist');
					}

				PowerToken::create(['name'=>$data['name'],'email'=>$data['email']]);
				 Mail::send('emails.email', $data, function($message) use ($data) {
			        $message->to($data['email']);
			        $message->subject('Join');
			    });
				$admin=User::first();
				$admin=json_decode(json_encode($admin),true);
				$sendArray=array(
					'admin'=>$admin['name'],
					'user_name'=>$data['name'],
					'email'=>$data['email']
					);
				Mail::send('emails.admin_email', $sendArray, function($message) use ($sendArray,$admin) {
			        $message->to('GexC@gexcrypto.io');
			        $message->subject('Join');
			    });

				return $resposne=array('status'=>'success');
			}

		}else{

			return $resposne=array('status'=>'fail','msg'=>'Please fill all fields');
		}
	}
	public function investerStepsSubmit(Request $request){
		$data=$request->input();
		if(!empty($data)){
			if($data['name']=='' || $data['email']=='' || $data['cemail']==''){

				return $resposne=array('status'=>'fail','msg'=>'Please fill all fields');
			}
			if($data['email']!=$data['cemail']){
				return $resposne=array('status'=>'fail','msg'=>'Confirmation email should be same');

			}else{
				$exist = PrivateInvester::where('email',$data['email'])->first();
				if(!empty($exist)){
					return $resposne=array('status'=>'fail','msg'=>'Email already exist');
					}

				PrivateInvester::create(['name'=>$data['name'],'email'=>$data['email']]);
				 Mail::send('emails.email', $data, function($message) use ($data) {
			        $message->to($data['email']);
			        $message->subject('Join');
			    });
				$admin=User::first();
				$admin=json_decode(json_encode($admin),true);
				$sendArray=array(
					'admin'=>$admin['name'],
					'user_name'=>$data['name'],
					'email'=>$data['email']
					);
				Mail::send('emails.admin_email', $sendArray, function($message) use ($sendArray,$admin) {
			        $message->to('GexC@gexcrypto.io');
			        $message->subject('Join');
			    });

				return $resposne=array('status'=>'success');
			}

		}else{

			return $resposne=array('status'=>'fail','msg'=>'Please fill all fields');
		}
	}
	public function terms(){

		$content = Cms::where('slug','terms')->first()->toArray();
		return view('frontend.pages.terms')->with('content',$content);

	}
	public function contact(Request $request)
	{
		$data=$request->input();
		//echo '<pre>'; print_r($data); die;

		$contents=CMS::where('slug','contact')->get()->toArray();
        $content=array();
        foreach($contents as $key=>$cont){
            $content[$cont['title']]=$cont['description'];
        }

		if(!empty($data)) {
        	Contact::create(['name' => $request->fname, 'email' => $request->email,
        		'phone' => $request->contact, 'message' => $request->messages]);
        	$data['name']=$request->fname;
        	$data['email']=$request->email;
        	$data['phone']=$request->phone;
        	$data['messages']=$request->messages;
        	/*mail to contact person */
        	Mail::send('emails.contact', $data, function($message) use ($data,$request) {
		        $message->to($request->email);
		        $message->subject('Contact Us Reply');
			});

			Mail::send('emails.admin_contact', $data, function($message) use ($data,$request) {
		 		$message->to('gurugram@revinfotech.com');
		        $message->subject('Contact Us');
			});

			Mail::send('emails.admin_contact', $data, function($message) use ($data,$request) {
		        $message->to('GexC@gexcrypto.io');
		        $message->subject('Contact Us');
			});



			\Session::flash('custom_message','Message sent successfully!');
			 return $resposne=array('status'=>'success');
			 die;

		} else {
			return view('frontend.pages.contact')->with('content',$content);
		}
	}
	public function subscribe(Request $request){

		$data=$request->input();
		if(!empty($data)) {
			if(filter_var($request->email, FILTER_VALIDATE_EMAIL)) {

					$exist=Subscriber::where('email',$data['email'])->first();
					if(empty($exist)){
						Subscriber::create(['name' => $request->name, 'email' => $request->email,
		       				]);

						/*mail to contact person */
			        	 Mail::send('emails.subscribe', $data, function($message) use ($data,$request) {
						        $message->to($request->email);
						        $message->subject('Thank you for subscribe with us.');
						 });

			 			return $resposne=array('status'=>'success');
			 		}else{
			 			return $resposne=array('status'=>'failure','msg'=>'Already Subscribed !');
			 		}

			}else{
        		return $resposne=array('status'=>'failure','msg'=>'Invalid Email !');
			 	die;

			 die;
			}

		}
	}
	public function saveToken($token){
		if($token){
			$exist=SubscribeToken::where('token',$token)->first();
			if(empty($exist)){
				SubscribeToken::create(['token'=>$token]);
			}
			Session::put('subscribe_token', $token);
			return $resposne=array('status'=>'success');
		}
	}
	public function show_notificationold(){

		if(Session::has('subscribe_token')){
			$token=Session::get('subscribe_token');
			$exist=SubscribeToken::where(['token'=>$token])->first();
			if(!empty($exist)){
				$created_date=$exist->created_at;
				$notifications=Newsletter::wheredate('created_at','>=',"'$created_date'")->get();
				$notifications=json_decode(json_encode($notifications),true);

				foreach($notifications as $key=>$noti){

					$seen=SeenNotification::where(['subscriber_token'=>$token,'newsletter_id'=>$noti['id']])->first();
					if(empty($seen)){
						SeenNotification::create(['subscriber_token'=>$token,'newsletter_id'=>$noti['id']]);
					}else{
						unset($notifications[$key]);
					}
				}
				return View::make('frontend.elements.notifications',array("notifications"=>$notifications));
			}
		}
	}
	public function show_notification(){

		if(Session::has('subscribe_token')){
			$token=Session::get('subscribe_token');
			$exist=SubscribeToken::where(['token'=>$token])->first();
			if(!empty($exist)){
				 $created_date=$exist->created_at;
				$notifications=Newsletter::orderBy('id','desc')->first();
				$notifications=json_decode(json_encode($notifications),true);


					$seen=SeenNotification::where(['subscriber_token'=>$token,'newsletter_id'=>$notifications['id']])->first();
					if(empty($seen)){
						SeenNotification::create(['subscriber_token'=>$token,'newsletter_id'=>$notifications['id']]);
					}else{
						$notifications=[];
					}
					if(empty($notifications)){
						return $resposne=array('status'=>'notfound');
					}else{
						return $resposne=array('title'=>$notifications['title'],'desc'=>$notifications['description'],'status'=>'found');
					}


			}
		}
	}
	public function remove_token(){

		Session::put('subscribe_token','');
		return $resposne=array('status'=>'success');
	}
		public function blog(Request $request){
		$data=$request->input();
		$search='';
		if(!empty($data['search'])){
		$search=$data['search'];

		$crypto = BlogPost::where('status',1)->where('category',1)->where('title', 'like', '%' . $data['search'] . '%')->orWhere('description', 'like', '%' . $data['search'] . '%')->orderBy('posted_date','desc')->paginate(9,['*'], 'gexcrypto-blog');
		$lat_posts = BlogPost::where('status',1)->where('category',2)->where('title', 'like', '%' . $data['search'] . '%')->orWhere('description', 'like', '%' . $data['search'] . '%')->orderBy('posted_date','desc')->paginate(9,['*'], 'latest-blog');

		}else{
		$crypto = BlogPost::where('status',1)->where('category',1)->orderBy('posted_date','desc')->paginate(9,['*'], 'gexcrypto-blog');
		$lat_posts = BlogPost::where('status',1)->where('category',2)->orderBy('posted_date','desc')->paginate(9,['*'], 'latest-blog');

		}
		$latest=BlogPost::where('status',1)->orderBy('posted_date','desc')->take(5)->get();
		$latest = json_decode(json_encode($latest),true);


		return View('frontend.pages.blog-list')->with('latest',$latest)->with('search',$search)->with('crypto',$crypto)->with('lat_posts',$lat_posts);

	}
	public function blog_detail($id){
		if($id)
		{
			$id   = convert_uudecode(base64_decode($id));
			$post = BlogPost::with('comment.reply')->where('id',$id)->first();
			$post = json_decode(json_encode($post),true);


			$latest=BlogPost::where('status',1)->orderBy('posted_date','desc')->take(5)->get();
			$latest = json_decode(json_encode($latest),true);
			return view('frontend.pages.blog-detail')->with(['post'=>$post,'latest'=>$latest]);
		}

		Redirect::to('blog');
	}
	public function blog_comment(Request $request){
		$data=$request->input();
		if(!empty($data)){

			BlogComment::create(['post_id'=>$data['post_id'],
								'name'=>$data['name'],
								'email'=>$data['email'],
								'message'=>$data['message']
				]);
			return $resposne=array('status'=>'success');
		}
	}

	public function exchange(){

		//$content = Cms::where('slug','terms')->first()->toArray();
		return view('frontend.pages.exchange');
	}

	public function about(){

		//$content = Cms::where('slug','terms')->first()->toArray();
		return view('frontend.pages.about');
	}

	public function test() {


		 return view('frontend.pages.test');
	}

	public function test3(Request $request) {
		$sendArray=array();
		Mail::send('emails.airdrop', $sendArray, function($message) use ($sendArray) {
					        $message->to('revin.testing@gmail.com');			        
					        $message->subject('Join');
					    });

		die;
 
 		$headers = array(
			
			'Access-Control-Allow-Origin' => '*',
			'Accept' =>'application/json',
			'Content-Type' => 'application/json'

			
		);
		$ip = \Request::ip();
		$body = array(
			'name' => 'Anurag Makol',
			'email' => 'anurag@revinfotech.com',
			'address' => 'asdasdsasadasdsadasdsadasdasd',
			'type' => 'ethereum',
			'ip' =>$ip,
			'page'=>'join-now',
			'whitepaper_agreement'=>1,
			'terms_and_conditions' =>1,
			'citizen_confirmation'=>1,
			'acknowledge_risk'=>1,
			'contract_address'=>'aaaa5555aaaa5555aaaa5555aaaa5555aaaa5555aa'
		);
		$body = json_encode($body);

		$response = Unirest\Request::post('https://dashboard.gexcrypto.io:3000/pullUser', $headers, $body);
		echo '<pre>';print_r($response); die;
		/*$ip = \Request::ip();	
		$country = $this->getCountry($ip);	
	    if($country=='US' ||  $country=='CN'){
	   		echo 'hm';
	   	}
	   die;*/


	}

	public function stepsnewSubmit(Request $request){
		$data=$request->input();
		if(!empty($data)){
			if($data['name']=='' || $data['email']=='' || $data['cemail']=='' || $data['address']==''){

				return $resposne=array('status'=>'fail','msg'=>'Please fill all fields');
			}
			if($data['email']!=$data['cemail']){
				return $resposne=array('status'=>'fail','msg'=>'Confirmation email should be same');

			}else{

				/*$exist = PowerToken::where('email',$data['email'])->first();
				if(!empty($exist)){
					return $resposne=array('status'=>'fail','msg'=>'Record already exist.');
				}
*/

			    /* send data to server */

			   if($data['type']=='Bitcoin'){
			   	$address='32vnfb5voNDSD86n9c8aNRxBsQeMJv4bFP';
			  	 //$img=url('public/frontend/images/bitcoin.png');
			   	$img = 'https://s3.us-east-2.amazonaws.com/gexcrypto/bitcoin.png';

			   }
			    if($data['type']=='Ethereum'){
			   		$address='0x9ef49eAd3cEf0d122DB97E9592ecbe34eeff21b1';
			   		//$img=url('public/frontend/images/ethereum.gif');
			   		$data['fromAddress']=$data['address'];
			   		$img = 'https://s3.us-east-2.amazonaws.com/gexcrypto/ethereum.gif';
			   }

			   if($data['type']=='Litecoin'){
			   	$address='3KSNkRdD9c2jqa4iXAuVF62TEckJ8KELpq';
			   	//$img=url('public/frontend/images/Litecoin.png');
			   	$img = 'https://s3.us-east-2.amazonaws.com/gexcrypto/litecoin.png';
			   }

			   if($data['type']=='Dogecoin'){
			   	$address='A23Zq3gr3AEjtJKtdKhrZaTZ3N5bdSpQkZ';
			   	// $img=url('public/frontend/images/dogecoin.png');
			    $img = 'https://s3.us-east-2.amazonaws.com/gexcrypto/dogecoin.png';
			   }
			   $data['contract_address']=$address;
			   $image='<img src="'.$img.'" class="img-responsive"/>';
			   unset($data['continue']);
			   unset($data['cemail']);
			   $data['ip'] = \Request::ip();

			 

				// Send post data Json format
			   
			   $store= PowerToken::create(['name'=>$data['name'],
					'email'=>$data['email'],
					'address'=>$data['address'],
					'type'=>$data['type'],
					'fromAddress'=>$data['fromAddress'],
					'page'=>$data['page']

			   	]);

			    $res =  $this->CurlSendPostJson($data);
			   
			   if($res->code=='200'){
			   		if(isset($res->body->error)){
			   //if(empty($store)){

			   			return $resposne=array('status'=>'fail','msg'=>@$res->body->message);

			   		}else{
					   	/*email*/
					   	// Mail::send('emails.email', $data, function($message) use ($data) {
					    //     $message->to($data['email']);
					    //     $message->subject('Join');
					    // });
					    if($data['page']=='airdrop'){

					    	Mail::send('emails.airdrop', $data, function($message) use ($data) {
						        $message->to($data['email']);			        
						        $message->subject('Welcome to GexCrypto');
						    });

					    }else{

					    	Mail::send('emails.investor', $data, function($message) use ($data) {
						        $message->to($data['email']);			        
						        $message->subject('Welcome to GexCrypto');
						    });
					    }
						$admin=User::first();
						$admin=json_decode(json_encode($admin),true);
						$sendArray=array(
							'admin'=>$admin['name'],
							'user_name'=>$data['name'],
							'email'=>$data['email'],
							'type'=>$data['type'],
							'address'=>$data['address']
							);
						Mail::send('emails.admin_email', $sendArray, function($message) use ($sendArray,$admin) {
					        $message->to('GexC@gexcrypto.io');			        
					        $message->subject('Join');
					    });
						$ip = \Request::ip();	
						$country = $this->getCountry($ip);	
			    		if($country=='US' ||  $country=='CN'){
			    			return $resposne=array('status'=>'success','address'=>$address,'img'=>$image,'type'=>$data['type']);
			    		}else{
							return $resposne=array('status'=>'success','address'=>$address,'img'=>$image,'type'=>$data['type']);
						}
					}
				}

				else{
					return $resposne=array('status'=>'fail','msg'=>$res->body->message,'res'=>$res);
				}
			}

		}else{

			return $resposne=array('status'=>'fail','msg'=>'Please fill all fields');
		}
	}

	public function CurlSendPostJson($body){
		$body = json_encode($body);

	    $headers = array(
			'Access-Control-Allow-Origin' => '*',
			'Accept' =>'application/json',
			'Content-Type' => 'application/json'
		);


		$response = Unirest\Request::post('https://dashboard.gexcrypto.io:3000/pullUser', $headers, $body);
		return $response;
	}
	public function getCountry($ip){			
    	$json = file_get_contents("http://ipinfo.io/".$ip);
    	$details = json_decode($json);  
    	return $details->country;  
	}

	public function career(){
		$dynamicSection1 = DB::table('career')->where('type', 'section1')->get();
		$dynamicSection2 = DB::table('career')->where('type', 'section2')->get();
		$dynamicSection3 = DB::table('career')->where('type', 'section3')->get();
		return view('frontend.pages.career')->with(['dynamicSection1' => $dynamicSection1, 'dynamicSection2' => $dynamicSection2, 'dynamicSection3' => $dynamicSection3]);
    }



}