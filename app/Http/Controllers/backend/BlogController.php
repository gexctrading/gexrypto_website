<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Redirect;
use App\Http\Controllers\Controller;
use App\BlogPost;
use App\BlogComment;
use App\ReplyComment;
use View;

class BlogController extends Controller
{
       
    public function __construct()
    {
       return $this->middleware('admin');
    }

    public function posts(Request $request){
        $data=$request->input();
        if(!empty($data)){
            $fileName='';

             if ($request->hasFile('image')) {
                $file = array('image' => Input::file('image'));
                $destinationPath = public_path().'/admin/blog/'; // upload path
                $ext= Input::file('image')->getClientOriginalExtension(); 
                if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext!='gif'){

                    \Session::flash('custom_success','File extension not allowed.');
                    return Redirect::Back();
                }
                $fileName = rand(11111,99999).'.'.$ext; // renaming image
               
                Input::file('image')->move($destinationPath, $fileName);

            }
            
            $posted_date=date_format(date_create($data['posted_date']),'Y-m-d');

            if($data['id']!=''){
                if($fileName==''){
                    $fileName=BlogPost::where('id',$data['id'])->value('image');
                }

          
                BlogPost::where('id',$data['id'])->update(['title'=>$data['title'],'description'=>$data['description'],'image'=>$fileName,'link'=>$data['link'],'author'=>$data['author'],'posted_date'=>$posted_date,'category'=>$data['category']]);
                \Session::flash('custom_success','Blog Post updated successfully.');
            }else{
                BlogPost::create(['title'=>$data['title'],'description'=>$data['description'],'image'=>$fileName,'link'=>$data['link'],'author'=>$data['author'],'posted_date'=>$posted_date,'category'=>$data['category']]);
                 \Session::flash('custom_success','Blog Post created successfully.');
            }
        }

        $posts=BlogPost::All();
        $posts=json_decode(json_encode($posts),true);
        return View('backend.blog.posts')->with('posts',$posts);
    }
    public function editPost($id){
        if($id){
            $id=convert_uudecode(base64_decode($id));
            $data=BlogPost::where('id',$id)->first();
            $data=json_decode(json_encode($data),true);
            return View('backend.blog.post_form')->with('data',$data);
        }
        return Redirect::to('/administrator/blog/posts');
    }
    public function change_status_blog_post($id=null,$status=null){
        if($id){
            if($status=='false'){
                BlogPost::where('id',$id)->update(['status'=>0]);
            }else{
                BlogPost::where('id',$id)->update(['status'=>1]);
            }
            return $resonse=array('status'=>'success');
        }
    }
    public function view_blog($id){
        if($id){
            $id       = convert_uudecode(base64_decode($id));
            $comments = BlogComment::where('post_id',$id)->get();
            $comments = json_decode(json_encode($comments),true);
           
            $blog   = BlogPost::where('id',$id)->first();
            $blog   =json_decode(json_encode($blog),true);
            return view('backend.blog.comments')->with('comments',$comments)->with('blog',$blog);
        }
        return Redirect::to('/administrator/blog/posts');
    }
    public function view_comment($id){
        if($id){
            $id       = convert_uudecode(base64_decode($id));
            $comment = BlogComment::with('reply')->where('id',$id)->first();
            $comment = json_decode(json_encode($comment),true);

            return View::make('backend.blog.render-comments',compact('comment'));
            //return $resonse=array('status'=>'success','comment'=>$comment['message']);
        }
    }
    public function comment_status($id,$status){
        if($id){
            if($status=='false'){
                BlogComment::where('id',$id)->update(['status'=>0]);
                $sts=0;
            }else{
                BlogComment::where('id',$id)->update(['status'=>1]);
                $sts=1;
            }
            return $resonse=array('status'=>'success','sts'=>$sts);
        }
    }
    public function reply(Request $request){
        $data = $request->input();
       
        if(!empty($data)){
            ReplyComment::create(['comment_id'=>$data['comment_id'],'comment'=>$data['message']]);
            return Redirect::back();
        }
    }
   
    
}
