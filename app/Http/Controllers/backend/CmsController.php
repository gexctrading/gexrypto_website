<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Redirect;
use App\Cms;
use App\Faq;
use App\Http\Controllers\Controller;

class CmsController extends Controller
{
       
    public function __construct()
    {
       return $this->middleware('admin');
    }

    public function homePage(Request $request){
        $data = $request->input();       
        if(!empty($data)){

            foreach($data as $key=>$dta){
                Cms::where(['slug'=>'home','title'=>$key])->update(['description'=>$dta]);
            }
         
          \Session::flash('custom_success','Home page updated successfully.');
        }
        $contents=CMS::where('slug','home')->get()->toArray();  
        $content=array();
        foreach($contents as $key=>$cont){
            $content[$cont['title']]=$cont['description'];
        }   
        //echo '<pre>'; print_r($content); die;   
        return View('backend.cms.home_page')->with(['content'=>$content]);
    }

    public function faq(Request $request){
        $data = $request->input();   
          
        if(!empty($data)){
            if($data['id']!=''){
            
                FAQ::where('id',$data['id'])->update(['title'=>$data['title'],'description'=>$data['description']]);
                \Session::flash('custom_success','Faq content Updated successfully.');
            }else{
                FAQ::create(['title'=>$data['title'],'description'=>$data['description']]);
                \Session::flash('custom_success','Faq content added successfully.');
            }
            
        }
       
        $list=FAQ::all()->toArray();   
       
        return View('backend.cms.faq')->with(['list'=>$list]);
    }
    public function edit_faq($id=null){
       
        if($id){
            $id=convert_uudecode(base64_decode($id));          
            $data=FAQ::where('id',$id)->first();
            return View('backend.cms.faq_form')->with(['data'=>$data]);

        }
        $list=FAQ::all()->toArray();
        return View('backend.cms.faq')->with(['list'=>$list]);
    }
    public function change_status($id=null,$status=null){
        if($id){
            if($status=='false'){
                Faq::where('id',$id)->update(['status'=>0]);
            }else{
                Faq::where('id',$id)->update(['status'=>1]);
            }
            return $resonse=array('status'=>'success');

        }
    }
    public function teamPage(Request $request){
        $data = $request->input();       
        if(!empty($data)){
           
           
                CMS::where(['slug'=>'team'])->update(['description'=>$data['description']]);
            
         
          \Session::flash('custom_success','Team page updated successfully.');
        }
        $content=CMS::where('slug','team')->first()->toArray();        
        return View('backend.cms.team_page')->with(['content'=>$content]);
    }
     public function faqPage(Request $request){
        $data = $request->input();       
        if(!empty($data)){
           
           
                CMS::where(['slug'=>'faq'])->update(['description'=>$data['description']]);
            
         
          \Session::flash('custom_success','Faq page updated successfully.');
        }
        $content=CMS::where('slug','faq')->first()->toArray();        
        return View('backend.cms.faq_page')->with(['content'=>$content]);
    }
    public function missionPage(Request $request){
        $data = $request->input();       
        if(!empty($data)){
           
           
                CMS::where(['slug'=>'mission'])->update(['description'=>$data['description']]);
            
         
          \Session::flash('custom_success','Mission page updated successfully.');
        }
        $content=CMS::where('slug','mission')->first()->toArray();        
        return View('backend.cms.mission_page')->with(['content'=>$content]);
    }
    public function whitepaperPage(Request $request){

        $data = $request->input();       
        if(!empty($data)){
           // print_r($data);
           //die;
            $fileName='';

             if ($request->hasFile('file')) {
                $file = array('file' => Input::file('file'));
                $destinationPath = public_path().'/frontend/files/'; // upload path
                $ext= Input::file('file')->getClientOriginalExtension(); 
                if($ext!='pdf'){

                    \Session::flash('custom_success','File extension not allowed.');
                    return Redirect::Back();
                }
                $fileName ='WhitePaper-GexC-Trading-Platform.'.$ext; // renaming image
               
                Input::file('file')->move($destinationPath, $fileName);
                \Session::flash('custom_success','Whitepaper pdf  updated successfully.');
            }

               // CMS::where(['slug'=>'whitepaper'])->update(['description'=>$data['description']]);
            
         
          
        }
        $content=CMS::where('slug','whitepaper')->first()->toArray();        
        return View('backend.cms.whitepaper_page')->with(['content'=>$content]);
    }
    public function roadmapPage(Request $request){

        $data = $request->input();

        if(!empty($data)){
             
            $data['stage']=json_encode(array_values($data['stage'])); 
            foreach($data as $key=>$dta){
                CMS::where(['slug'=>'roadmap','title'=>$key])->update(['description'=>$dta]);
            }
         
          \Session::flash('custom_success','RoadMap page updated successfully.');
        }

        $contents=CMS::where('slug','roadmap')->get()->toArray();  
        $content=array();
        foreach($contents as $key=>$cont){
            $content[$cont['title']]=$cont['description'];
        }   
          
        return View('backend.cms.roadmap_page')->with(['content'=>$content]);
    }
    public function header(Request $request){

        $data = $request->input();
        if(!empty($data)){
          $count = count($data['menu']);
          $order=[];
            foreach($data['menu'] as $menu){
                $order[]=$menu['order'];
            }

           
            $err=0;
            for($i=1;$i<=$count;$i++){
                if(in_array($i,$order)){
                    $err=$err;
                }else{
                    $err=1;
                }
            }
            
            $data['menu']=json_encode(array_values($data['menu']));
            CMS::where(['slug'=>'header_menu'])->update(['description'=>$data['menu']]);
            if($err==0){           
                \Session::flash('custom_success','Header Menus updated successfully.');
            }else{
                \Session::flash('custom_error','Please select the correct order of all menus.');
            }
        }

        $content=CMS::where('slug','header_menu')->first()->toArray(); 
        return View('backend.cms.header')->with(['content'=>$content]);

    }
    public function footer(Request $request){

        $data=$request->input();
        if(!empty($data)){

            
            $count = count($data['link']);
            $order=[];
            if(!empty($data['link'])){
                foreach($data['link'] as $link){
                    $order[]=$link['order'];
                }

               
                $err=0;
                for($i=1;$i<=$count;$i++){
                    if(in_array($i,$order)){
                        $err=$err;
                    }else{
                        $err=1;
                    }
                }
                $data['links']=json_encode(array_values($data['link']));
            }
            unset($data['link']);
            //echo '<pre>'; print_r($data);die;
            foreach($data as $key=>$dta){
              
               CMS::where(['slug'=>'footer','title'=>$key])->update(['description'=>$dta]);
            }
            //die;
            
            if($err==1){

                \Session::flash('custom_error','Please check order of links.');
            }else{
                \Session::flash('custom_success','Footer Page updated successfully.');
            }

        }
            $contents=CMS::where('slug','footer')->get()->toArray();
            $content=array();
            foreach($contents as $key=>$cont){
                $content[$cont['title']]=$cont['description'];
            }
            
        
        //echo '<pre>'; print_r($content); die; 
        return View('backend.cms.footer')->with(['content'=>$content]);

    }
    public function contact(Request $request){
        $data = $request->input();       
        if(!empty($data)){

            foreach($data as $key=>$dta){
               CMS::where(['slug'=>'contact','title'=>$key])->update(['description'=>$dta]);
            }
         
          \Session::flash('custom_success','Contact page updated successfully.');
        }
        $contents=CMS::where('slug','contact')->get()->toArray();  
        $content=array();
        foreach($contents as $key=>$cont){
            $content[$cont['title']]=$cont['description'];
        }   
        //echo '<pre>'; print_r($content); die;   
        return View('backend.cms.contact_page')->with(['content'=>$content]);

    }
    public function termsPage(Request $request){

        $data = $request->input();       
        if(!empty($data)){
           
           
                CMS::where(['slug'=>'terms'])->update(['description'=>$data['description']]);
            
         
          \Session::flash('custom_success','Terma and condition page updated successfully.');
        }
        $content=CMS::where('slug','terms')->first();
        $content = json_decode(json_encode($content),true);    
        return View('backend.cms.terms_page')->with(['content'=>$content]);
    }
}
