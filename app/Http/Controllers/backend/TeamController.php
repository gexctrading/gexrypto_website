<?php

namespace App\Http\Controllers\backend;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Redirect;
use App\TeamCategory;
use App\TeamMember;
use App\Http\Controllers\Controller;


class TeamController extends Controller
{
       
    public function __construct()
    {
       return $this->middleware('admin');
    }

    public function categories(Request $request){
        $data = $request->input();       
        if(!empty($data)){
            
            if($data['id']!=''){

                 TeamCategory::where('id',$data['id'])->update(['title'=>$data['title']]);      
                \Session::flash('custom_success','Category updated successfully.');

            } else{          
                TeamCategory::create(['title'=>$data['title']]);      
                \Session::flash('custom_success','Category added successfully.');
            }
        }

        $list=TeamCategory::get()->toArray();        
        return View('backend.team.category_list')->with(['list'=>$list]);
    }
    public function edit_categories($id=null){
        if($id){
            $id=convert_uudecode(base64_decode($id));          
            $data=TeamCategory::where('id',$id)->first();
            return View('backend.team.category_form')->with(['data'=>$data]);
        }
        return Redirect::to('administrator/team/categories');
    }
    public function del_categories($id=null){
        if($id){
             $id=convert_uudecode(base64_decode($id)); 
             $data=TeamCategory::where('id',$id)->delete();
             \Session::flash('custom_success','Category deleted successfully.');
        }
        
        return Redirect::to('administrator/team/categories');
    }
    public function addMember(){

        $category=TeamCategory::get()->toArray();        
        return View('backend.team.member_form')->with(['category'=>$category]);
    }
    public function members(Request $request)
    {
        $data = $request->input();       
        if(!empty($data)){

            $fileName='';

             if ($request->hasFile('image')) {
                $file = array('image' => Input::file('image'));
                $destinationPath = public_path().'/admin/team/'; // upload path
                $ext= Input::file('image')->getClientOriginalExtension(); 
                if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext!='gif'){

                    \Session::flash('custom_success','File extension not allowed.');
                    return Redirect::Back();
                }
                $fileName = rand(11111,99999).'.'.$ext; // renaming image
               
                Input::file('image')->move($destinationPath, $fileName);

            }
            
            if($data['id']!=''){
                if($fileName==''){
                    $fileName=TeamMember::where('id',$data['id'])->value('image');
                }

                 TeamMember::where('id',$data['id'])->update(['name'=>$data['name'],'designation'=>$data['designation'],'linkedin'=>$data['linkedin'],'image'=>$fileName,'category'=>$data['category'],'show_on_home_page'=>$data['show_on_home_page']]);      
                \Session::flash('custom_success','Team member updated successfully.');

            } else{          
                TeamMember::create(['name'=>$data['name'],'designation'=>$data['designation'],'linkedin'=>$data['linkedin'],'image'=>$fileName,'category'=>$data['category'],'show_on_home_page'=>$data['show_on_home_page']]);      
                \Session::flash('custom_success','Team Member added successfully.');
            }
        }

        $list=TeamMember::get()->toArray();        
        return View('backend.team.member_list')->with(['list'=>$list]);
    }
    public function edit_member($id=null){
        if($id){
            $id=convert_uudecode(base64_decode($id));          
            $data=TeamMember::where('id',$id)->first()->toArray();
            $category=TeamCategory::get()->toArray();
            return View('backend.team.member_form')->with(['data'=>$data,'category'=>$category]);
        }
        return Redirect::to('administrator/team/categories');
    }
    public function del_member($id=null){
        if($id){
             $id=convert_uudecode(base64_decode($id)); 
             $data=TeamMember::where('id',$id)->delete();
             \Session::flash('custom_success','Category deleted successfully.');
        }
        
        return Redirect::to('administrator/team/members');
    }

    public function change_status_team_category($id=null,$status=null){
        if($id){
            if($status=='false'){
                TeamCategory::where('id',$id)->update(['status'=>0]);
            }else{
                TeamCategory::where('id',$id)->update(['status'=>1]);
            }
            return $resonse=array('status'=>'success');

        }
    }

    public function change_status_team_member($id=null,$status=null){
        if($id){
            if($status=='false'){
                TeamMember::where('id',$id)->update(['status'=>0]);
            }else{
                TeamMember::where('id',$id)->update(['status'=>1]);
            }
            return $resonse=array('status'=>'success');

        }
    }

}
