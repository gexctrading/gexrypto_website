<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Facades\Input;
use Intervention\Image\ImageManagerStatic as Image;
use DB;
use Redirect;
use App\Newsletter;
use Mail;
use App\TeamMember;
use App\Subscriber;
use App\SiteView;
use App\BlogPost;
use App\Contact;
use App\ApplyJob;
use App\Career;
use App\PowerToken;
use App\PrivateInvester;


class AdminController extends Controller
{
       
    public function __construct()
    {
       $this->middleware('admin')->except('login');
    }


    public function login()
    {   
         if(Auth::check()){
            return Redirect::to('/administrator/dashboard');
        }else{
    	   return view('backend.login');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/administrator');
    }

    public function dashboard()
    {
            $team_members = TeamMember::count();
            $subscribers  = Subscriber::count();
            $site_views   = SiteView::count();
            $posts        = BlogPost::count();
    	    return view('backend.dashboard')->with(['team_members'=>$team_members,'subscribers'=>$subscribers,'site_views'=>$site_views,'posts'=>$posts]);
        
    }

    public function profile()
    {
    	return view('backend.profile');
    }

    public function profile_information(Request $request)
    {
    	User::where('id',Auth::user()->id)->update(array('name'=>$request->name,'phone'=>$request->phone,'email'=>$request->email));
    	return redirect('/administrator/profile')->with('custom_success','Update Successfully.');
    }

    public function admin_image(Request $request)
    {
    	if(Input::file())
    	{
    		

                $file = array('image' => Input::file('image'));
                $destinationPath = public_path().'/admin/'; // upload path
                $ext= Input::file('image')->getClientOriginalExtension(); 
                if($ext!='jpg' && $ext!='png' && $ext!='jpeg' && $ext!='gif'){

                    \Session::flash('custom_danger','File extension not allowed.');
                    return Redirect::Back();
                }
                $fileName = rand(11111,99999).'.'.$ext; // renaming image
               
                Input::file('image')->move($destinationPath, $fileName);

                User::where('id',Auth::user()->id)->update(array('image'=>$fileName));

    		return redirect('/administrator/profile')->with('custom_success','Profile Image Update Successfully');
    	}
    	else
    	{
    		return redirect('/administrator/profile')->with('custom_success','Profile Image Update Successfully');
    	}    
    } 

    public function checkAdminPassword(Request $request)
    {
    	$password=md5($request->old_password);
    	$admin_id=Auth::user()->id;
    	$record=User::where('id',$admin_id)->where('md5_password',$password)->get()->first();
    	if(!empty($record))
    	{
    		echo 'true';die;
    	}
    	else
    	{
    		echo 'false';die;
    	}
    }

    public function change_pass(Request $request)
    {

    	$admin_id=Auth::user()->id;
    	$new_pass=bcrypt($request->password);
    	$record=User::where('id',$admin_id)->update(array('password'=>$new_pass,'md5_password'=>md5($request->password)));
    	return redirect('/administrator/profile')->with('custom_success','Password Change Successfully');
    }
    public function contactList(){

        $list=Contact::all();
        $list=json_decode(json_encode($list),true);

        return View('backend.cms.contact-list')->with('list',$list);

    }
    public function viewContact($id){
        if($id){
            $message=Contact::where('id',$id)->value('message');
            return $response=array('status'=>'success','message'=>$message);
        }
    }
    public function subscribers(){

        $list=Subscriber::get();
        $list=json_decode(json_encode($list),true);

        return View('backend.subscribers')->with('list',$list);
    }
    public function deleteSubscriber($id){
        if($id){
             $id=convert_uudecode(base64_decode($id));
            Subscriber::where('id',$id)->delete();
            return Redirect::to('administrator/subscribers');
        }

    }
    public function newsletters(Request $request){
        $data=$request->input();
        if(!empty($data)){

        }
        $list=Newsletter::all();
        $list=json_decode(json_encode($list),true);
        return View('backend.newsletters')->with('list',$list);

    }
    public function delNewsletter($id){
        if($id){

             $id=convert_uudecode(base64_decode($id));
             Newsletter::where('id',$id)->delete();
             return Redirect::to('/administrator/newsletters');
        }
    }
    public function sendNewsletter(Request $request){
        
        $data=$request->input();
        if(!empty($data)){
            if($data['id']==''){
                Newsletter::create(['title'=>$data['title'],'description'=>$data['description']]);

            }else{
                Newsletter::where('id',$data['id'])->update(['title'=>$data['title'],'description'=>$data['description']]);
            }

            $subscribers=Subscriber::all();
            $subscribers=json_decode(json_encode($subscribers),true);
            foreach($subscribers as $subscribe){
                Mail::send('emails.newsletter', $data, function($message) use ($data,$request,$subscribe) {
                            $message->to($subscribe['email']);
                            $message->subject('Newsletter');
                });
            }
            \Session::flash('custom_message','Newsletter sent successfully!'); 

        }

        return Redirect::to('/administrator/newsletters');

    }
    public function send_newsletter($id){

        if($id){
            
            $id=convert_uudecode(base64_decode($id));          
            $data=Newsletter::where('id',$id)->first();
            $data=json_decode(json_encode($data),true);
            
            return view('backend.newsletter_form')->with('data',$data);
        }
        return Redirect::to('/administrator/newsletters');
    }
    public function private_investers(){

        $list = PrivateInvester::all();
        $list = json_decode(json_encode($list),true);

         return view('backend.investers')->with('list',$list);
    }
    public function join_list(){

        $list = PowerToken::all();
        $list = json_decode(json_encode($list),true);

         return view('backend.power_token')->with('list',$list);
    }
    public function deleteInvester($id){
        if($id){
            $id=convert_uudecode(base64_decode($id));
            PrivateInvester::where('id',$id)->delete();
             \Session::flash('custom_success','Record deleted successfully!');
            return Redirect::to('administrator/private-investers');
        }
    }
    public function deleteJoin($id){
        if($id){
            $id=convert_uudecode(base64_decode($id));
            PowerToken::where('id',$id)->delete();
             \Session::flash('custom_success','Record deleted successfully!'); 
            return Redirect::to('administrator/join-list');
        }
    }

    public function empCareer(){
        $section1 = Career::where('type','section1')->get();
        $section2 = Career::where('type','section2')->get();
        $section3 = Career::where('type','section3')->get();
        $section4 = Career::where('type','section4')->get();
        return view('backend.career')->with(['section1' => $section1, 'section2' => $section2, 'section3' => $section3, 'section4' => $section4]);
    }

    public function careerSection1(Request $request){
        $countSec1 = Career::where('type','section1')->count();
        if($countSec1==1)
        {
           return redirect::back()->with('custom_info','You can Add only one record.'); 
        }
        else{
            Career::insert(['title' => $request->input('title'), 'description' => $request->input('description'), 'vacancy' => '0', 'position' => '0', 'responsibility' => '0','experience' => '0', 'skills' => '0', 'location' => '0', 'type' => 'section1']);    
        }
        
        return redirect::back()->with('custom_insert_success', 'Insert Record Successfully.');
    }
    public function editCareerSec1(Request $request){
        Career::where('id',$request->input('edit_secid1'))->update(['title' => $request->input('edit_title'), 'description' => $request->input('edit_description')]);
        return redirect::back()->with('custom_edit_trust', 'Edit Record Successfully.');
    }
    public function deleteCareerSec1(Request $request){
        Career::where('id', $request->input('delete_secid'))->delete();
        return redirect::back()->with('custom_trust_error','Delete Record Successfully.');
    }


    public function careerSection2(Request $request){
        $countSec1 = Career::where('type','section2')->count();
        if($countSec1==1)
        {
           return redirect::back()->with('custom_info','You can Add only one record.'); 
        }
        else{
            Career::insert(['title' => $request->input('title'), 'description' => $request->input('description'), 'vacancy' => '0', 'position' => '0', 'experience' => '0', 'responsibility' => '0','skills' => '0', 'location' => '0', 'type' => 'section2']);    
        }
        
        return redirect::back()->with('custom_insert_success', 'Insert Record Successfully.');
    }
    public function editCareerSec2(Request $request){
        Career::where('id',$request->input('edit_secid1'))->update(['title' => $request->input('edit_title'), 'description' => $request->input('edit_description')]);
        return redirect::back()->with('custom_edit_trust', 'Edit Record Successfully.');
    }
    public function deleteCareerSec2(Request $request){
        Career::where('id', $request->input('delete_secid'))->delete();
        return redirect::back()->with('custom_trust_error','Delete Record Successfully.');
    }


    public function careerSection3(Request $request){
        Career::insert(['title' => $request->input('title'), 'description' => $request->input('description'), 'vacancy' => '0', 'position' => '0', 'experience' => '0', 'responsibility' => '0', 'skills' => '0', 'location' => '0', 'type' => 'section3']);    
        return redirect::back()->with('custom_insert_success', 'Insert Record Successfully.');
    }
    public function editCareerSec3(Request $request){
        Career::where('id',$request->input('edit_secid1'))->update(['title' => $request->input('edit_title'), 'description' => $request->input('edit_description')]);
        return redirect::back()->with('custom_edit_trust', 'Edit Record Successfully.');
    }
    public function deleteCareerSec3(Request $request){
        Career::where('id', $request->input('delete_secid'))->delete();
        return redirect::back()->with('custom_trust_error','Delete Record Successfully.');
    }

    public function careerSection4(Request $request){
        if($request->input('description')==''){
            Career::insert(['title' => '0', 'description' => '', 'vacancy' => $request->input('vacancy'), 'position' => $request->input('position'), 'experience' => $request->input('experience'), 'category' => $request->input('category'), 'responsibility' => $request->input('responsibility'), 'skills' => $request->input('skills'), 'location' => $request->input('location'), 'type' => 'section4']);
        return redirect::back()->with('custom_insert_success', 'Insert Record Successfully.');
        }
        else{
           Career::insert(['title' => '0', 'description' => $request->input('description'), 'vacancy' => $request->input('vacancy'), 'position' => $request->input('position'), 'experience' => $request->input('experience'),  'category' => $request->input('category'),'responsibility' => $request->input('responsibility'), 'skills' => $request->input('skills'), 'location' => $request->input('location'), 'type' => 'section4']);
        return redirect::back()->with('custom_insert_success', 'Insert Record Successfully.'); 
        }
        
    }

    public function editCareerSec4(Request $request){
        if($request->input('edit_description4')==''){
           Career::where('id', $request->input('edit_secid4'))->update(['description' => '', 'vacancy' => $request->input('edit_vacancy'), 'position' => $request->input('edit_position'), 'experience' => $request->input('edit_experience'), 'responsibility' => $request->input('edit_responsibility'), 'skills' => '', 'location' => $request->input('edit_location')]);
        return redirect::back()->with('custom_edit_trust', 'Edit Record Successfully.'); 
        }
        else{
            Career::where('id', $request->input('edit_secid4'))->update(['description' => $request->input('edit_description4'), 'vacancy' => $request->input('edit_vacancy'), 'position' => $request->input('edit_position'), 'experience' => $request->input('edit_experience'), 'responsibility' => $request->input('edit_responsibility'), 'skills' => $request->input('edit_skills'), 'location' => $request->input('edit_location')]);
        return redirect::back()->with('custom_edit_trust', 'Edit Record Successfully.');
        }
        
    }
    public function deleteCareerSec4(Request $request){
        Career::where('id', $request->input('delete_secid'))->delete();
        return redirect::back()->with('custom_trust_error','Delete Record Successfully.');
    }

    public function applicantDetail(){
       /* $record = DB::select('SELECT a.id, a.first_name, a.last_name, a.email,a.contact,a.qualification,a.current,a.experience,a.resume,a.date,b.category FROM apply_jobs a, career b WHERE a.job_id = b.id');*/

        $records=ApplyJob::with('Career')->latest('date')->get();
        // echo "<pre>"; print_r($records);die;
        
        return view('backend.applicant')->with(['record' => $records]);
    }

    public function deleteRecord(Request $request){
        ApplyJob::where('id',$request->input('delete_content_id'))->delete();
        return redirect::back()->with('custom_trust_error', 'Delete Record Successfully.');
    }

}
