<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubscribeToken extends Model
{
    protected $table ='subscribe_tokens';
    protected $guarded=[];
}
