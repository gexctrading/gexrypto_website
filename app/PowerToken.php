<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PowerToken extends Model
{
    protected $table ='power_tokens';
     protected $guarded=[];
}
