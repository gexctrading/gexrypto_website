<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogComment extends Model
{
    protected $table ='blog_comments';
    protected $fillable=[];
    protected $guarded=[];

     public function reply(){
    	return $this->hasMany('App\ReplyComment','comment_id','id');
    }
}
