
$(document).ready(function(){
	$('.navbar-toggle').click(function(){
		$('.navbar-nav.navbar-right').addClass('open');
	});
	$('.close_nav').click(function(){
		$('.navbar-nav.navbar-right').removeClass('open');
	});
});

$(window).scroll(function(){
	var sticky = $('.header'),
	scroll = $(window).scrollTop();
	if($(window).width() > 767)
	{
		if (scroll >= 4)
		{
			sticky.addClass('fixed');
			// $('.header .row').addClass('fix-header-row');
			// $('.slider-text').addClass('slider-text-pos-after-scroll');					  
		}
		else
		{
			sticky.removeClass('fixed');
			// $('.header .row').removeClass('fix-header-row');
			// $('.slider-text').removeClass('slider-text-pos-after-scroll');		
		}
	}
});

$(document).ready(function(){
	$('.contact-form .form-control').on('focus blur', function (e) {
		$(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
	}).trigger('blur');
});

$('.counter').each(function () {
	$(this).prop('Counter',0).animate({
		Counter: $(this).text()
	}, {
		duration: 7000,
		easing: 'swing',
		step: function (now) {
			$(this).text(Math.ceil(now));
		}
	});
});

window.intercomSettings = {
	app_id: "tvhstisv"
};

(function(){
	var w=window;
	var ic=w.Intercom;
	if(typeof ic==="function"){
		ic('reattach_activator');
		ic('update',intercomSettings);
	}
	else{
		var d=document;
		var i=function(){
			i.c(arguments)
		};
		i.q=[];
		i.c=function(args){
			i.q.push(args)
		};
		w.Intercom=i;
		function l(){
			var s=d.createElement('script');
			s.type='text/javascript';
			s.async=true;
			s.src='https://widget.intercom.io/widget/tvhstisv';
			var x=d.getElementsByTagName('script')[0];
			x.parentNode.insertBefore(s,x);
		}
		if(w.attachEvent){
			w.attachEvent('onload',l);
		}
		else{
			w.addEventListener('load',l,false);
		}
	}
})()